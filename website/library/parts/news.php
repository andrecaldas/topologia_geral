<?php
  @include_once ( dirname(__FILE__) . '/news_file.php' );

  function render_news ( $opening, $closing )
  {
    $news_date = news_date();
    $news_title = news_title();
    $news_content = news_content();

    $news_deadline_iso = news_deadline_iso();

    if ( ! isset($news_title) && ! isset($news_content) )
    {
      return;
    }

    if ( isset($news_deadline_iso) )
    {
      $deadline  = strtotime( $news_deadline_iso );
      if ( time() > $deadline )
      {
        return;
      }
    }


    ?>
      <?= $opening ?>
        <dl>
          <?php
            if ( isset($news_title) )
            {
              $news_date = isset($news_date)?$news_date:'';

              ?>
                <dt><?= htmlspecialchars($news_title) ?> (<?= htmlspecialchars($news_date) ?>)</dt>
              <?php
            }
    
            if ( isset($news_content) )
            {
              ?>
                <dd><?= $news_content ?></dd>
<?php /*
                <dd><?= htmlspecialchars($news_content) ?></dd>
*/?>
              <?php
            }
          ?>
        </dl>
      <?= $closing ?>
    <?php
  }
?>