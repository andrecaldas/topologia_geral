<?php
  class page_info
  {
    private $codename;
    private $include_file;
    private $menu_name;
    private $sub_items = array();
    private $parent_page = NULL;

    static private $root_pages = array();
    static private $all_pages = array();


    public function __construct ( $codename, $include_file, $menu_name = NULL )
    {
      assert( 'false === strpos($codename, page_info::codename_separator())' );

      $this->codename = $codename;
      $this->include_file = $include_file;
      $this->menu_name = $menu_name;
    }


    public function full_codename ()
    {
      if ( NULL == $this->parent_page )
      {
        return $this->codename;
      }

      return $this->parent_page->full_codename . page_info::codename_separator() . $this->codename;
    }


    public function codename ()
    {
      return $this->codename;
    }


    static public function codename_separator ()
    {
      return '/';
    }


    public function include_file ()
    {
      return $this->include_file;
    }


    public function menu_name ()
    {
      return $this->menu_name;
    }


    public function page_link ()
    {
      $url_to_prepend = '?page_codename=';

      $item_full_codename = $this->full_codename();
      if ( '' === $item_full_codename )
      {
        return preg_replace( '#/+[^/]*$#', '/', $_SERVER['PHP_SELF'] );
      }

      $item_full_codename_url = urlencode( $item_full_codename );
      return $url_to_prepend . $item_full_codename_url;
    }


    public function language ()
    {
      return 'pt-br';
    }


    static public function &get_page ( $full_codename )
    {
      if ( ! isset(self::$all_pages[$full_codename]) )
      {
        die ( "<br />Not found page: $full_codename" );
      }

      return self::$all_pages[$full_codename];
    }


    // Can I make it "const" as in C++?
    public function sub_items ()
    {
      return $this->sub_items;
    }


    public function append_page ( &$page_info_obj )
    {
      assert ( 'NULL == $page_info_obj->parent_page' );
      $this->sub_items[$page_info_obj->codename()] =& $page_info_obj;

      if ( NULL == $page_info_obj->parent_page )
      {
        $page_info_obj->parent_page = $this;
      }
    }


    static public function &new_page ( $codename, $include_file, $menu_name = NULL, &$parent_page = NULL )
    {
      $page = new page_info( $codename, $include_file, $menu_name );
      if ( NULL != $parent_page )
      {
        $parent_page->append_page( $page );
      }
      else
      {
        page_info::$root_pages[$page->codename()] =& $page;
      }

      assert( '!isset(page_info::$all_pages[$page->full_codename()])' );
      page_info::$all_pages[$page->full_codename()] =& $page;

      return page_info::$all_pages[$page->full_codename()];
    }


    public function parent_page ()
    {
      return $this->parent_page;
    }


    // Can I make it "const" as I do in C++?
    static public function root_pages ()
    {
      return page_info::$root_pages;
    }
  }
?>