<?php
  require_once( 'pdf_ids.php' );


  $urls = array();
  $urls['normal']          = "http://ourproject.org/frs/download.php/$id_normal/topologia_geral.pdf";
  $urls['frente_e_verso']  = "http://ourproject.org/frs/download.php/$id_frente_e_verso/frente_e_verso_topologia_geral.pdf";
  $urls['letras_grandes']  = "http://ourproject.org/frs/download.php/$id_letras_grandes/letras_grandes_topologia_geral.pdf";
  $urls['economico']       = "http://ourproject.org/frs/download.php/$id_economico/economico_topologia_geral.pdf";
  $urls['muito_economico'] = "http://ourproject.org/frs/download.php/$id_muito_economico/muito_economico_topologia_geral.pdf";


  $versao = isset($_REQUEST['versao'])?$_REQUEST['versao']:'normal';
  if ( isset($_REQUEST['no_redirect']) )
  {
    die ( "<p>$urls[$versao]</p>\n" );
  }

  switch ( $versao )
  {
    case 'normal':
    case 'frente_e_verso':
    case 'letras_grandes':
    case 'economico':
    case 'muito_economico':
      header( 'Location: ' . $urls[$versao] );
      break;

    default:
      // PHP is so lame!
      // Why do I have to write this by hand!?
      header("HTTP/1.0 404 Not Found");
      header("Status: 404 Not Found");
  }
?>
