<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction( )
    {
      $request = $this->getRequest();
      $page = $request->getParam( 'page' );

      $this->render( $page );
    }
}

