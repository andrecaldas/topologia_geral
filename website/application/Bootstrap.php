<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
  protected function _initViewHelpers ()
  {
    $this->bootstrap( 'layout' );
    $layout = $this->getResource( 'layout' );
    $view = $layout->getView();

    $view->doctype( 'XHTML1_STRICT' );
    $view->headMeta()->appendHttpEquiv( 'ContentType', 'text/html; charset=utf-8' );

    $view->headTitle()->setSeparator( ' :: ' );
  }

  protected function _initNavigation ()
  {
    $this->bootstrap( 'layout' );
    $layout = $this->getResource( 'layout' );
    $view = $layout->getView();

    $config = new Zend_Config_Xml( APPLICATION_PATH.'/configs/navigation.xml', 'nav' );

    $navigation = new Zend_Navigation( $config );
    $view->navigation( $navigation );
  }

  protected function _initRewriteRouter ()
  {
    $ctrl = Zend_Controller_Front::getInstance();
    $router = $ctrl->getRouter();

    $router->addRoute
    (
      'simplify',
      new Zend_Controller_Router_Route
      (
        ':page',
        array
        (
          'controller' => 'index',
          'action'     => 'index',
          'page'       => 'index'
        )
      )
    );
  }
}

