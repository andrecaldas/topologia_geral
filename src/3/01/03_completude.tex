\section{Completude}
  \label{section:completude:metrica}

  Em um espaço métrico,
  uma sequência convergente $a_n$,
  é uma sequência que ``acumula'' em torno de um ponto $a$.
  Para $N$ suficientemente grande,
  todos os elementos de
  $\setsuchthat{a_n}{n \geq N}$
  estão próximos de $a$,
  e próximos \emph{uns dos outros}.

  Se removessemos o ponto $a$ do espaço em questão,
  a sequência $a_n$ não seria mais convergente.
  Mas se \emph{``acumularia''} em torno de um \emph{``buraco''}.
  A seguir,
  formalizaremos essas noções de sequências que se \emph{``acumulam''},
  e espaços com ou sem \emph{``buracos''}.

  \begin{definition}[Sequências de Cauchy]
    Em um espaço métrico,
    uma sequência $a_n$ é dita
    \emph{sequência de Cauchy},
    ou
    \emph{sequência fundamental},
    quando a sequência de conjuntos
    \begin{equation*}
      A_n
      =
      \setsuchthat{a_j}{j \geq n}
    \end{equation*}
    é tal que
    \begin{equation*}
      \diameter{A_n}
      \rightarrow
      0.
    \end{equation*}
    Onde
    \begin{equation*}
      \diameter{A_n}
      =
      \sup \setsuchthat{d(x,y)}{x,y \in A_n}.
    \end{equation*}
  \end{definition}
  Em outras palavras,
  para todo $\varepsilon > 0$,
  existe $N$ tal que
  \begin{equation*}
    m,n \geq N
    \Rightarrow
    d(a_n, a_m) < \varepsilon.
  \end{equation*}
  As sequências de Cauchy são aquelas que
  \emph{``deveriam''} convergir,
  não fosse o espaço em questão \emph{``esburacado''}.

  \begin{definition}[Completude]
    Um espaço métrico é \emph{completo}
    quando toda sequência de Cauchy é convergente.
  \end{definition}

  \begin{lemma}
    \label{lemma:sequencia_convergente_eh_cauchy}
    Em um espaço métrico,
    toda sequência convergente é de Cauchy.
  \end{lemma}

  \begin{proof}
    Seja $a_n \rightarrow a$.
    Então,
    para todo $\varepsilon > 0$,
    existe $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N
      \Rightarrow
      d(a_n, a) < \frac{\varepsilon}{2}.
    \end{equation*}
    Sendo assim,
    \begin{equation*}
      m,n \geq N
      \Rightarrow
      d(a_m, a_n)
      \leq
      d(a_m, a) + d(a, a_n)
      <
      \varepsilon.
    \end{equation*}
  \end{proof}

  \begin{lemma}
    \label{lemma:uniformemente_continua:cauchy_em_cauchy}
    Uma função
    \begin{equation*}
      \function{f}{X}{Y}
    \end{equation*}
    entre espaços métricos que é uniformemente contínua,
    leva sequências de Cauchy em sequências de Cauchy.
  \end{lemma}

  \begin{proof}
    Seja
    $a_n \in X$ uma sequência de Cauchy.
    Pela continuidade uniforme,
    para todo $\varepsilon > 0$,
    existe $\delta > 0$ tal que
    \begin{equation*}
      d(x,y) < \delta
      \Rightarrow
      d(f(x), f(y)) < \varepsilon.
    \end{equation*}
    Como $a_n$ é sequência de Cauchy,
    existe $n \in \naturals$
    tal que
    \begin{align*}
      m,n \geq N
      &\Rightarrow
      d(a_m, a_n) < \delta
      \\
      &\Rightarrow
      d(f(a_m), f(a_n)) < \varepsilon.
    \end{align*}
    Ou seja,
    para todo $\varepsilon > 0$,
    existe $\delta > 0$ tal que
    \begin{equation*}
      m,n \geq N
      \Rightarrow
      d(f(a_m), f(a_n)) < \varepsilon.
    \end{equation*}
  \end{proof}

  Agora,
  podemos finalmente estender
  $\function{f}{Z \subset X}{Y}$.

  \begin{proposition}
    Seja
    \begin{equation*}
      \function{f}{Z \subset X}{Y}
    \end{equation*}
    uma aplicação uniformemente contínua,
    onde $X$ e $Y$ são espaços métricos,
    e $Y$ é completo.
    Então,
    existe
    \begin{equation*}
      \function{g}{\closure{Z}}{Y}
    \end{equation*}
    uniformemente contínua,
    tal que $f = g|_Z$.
  \end{proposition}

  \begin{proof}
    Para $z \in \closure{Z}$,
    tome $z_n \in Z$ tal que
    $z_n \rightarrow z$.
    Faça
    \begin{equation*}
      g(z)
      =
      \lim f(z_n).
    \end{equation*}
    Note que $\lim f(z_n)$ existe,
    pois $z_n$ é,
    pelo Lema \ref{lemma:sequencia_convergente_eh_cauchy},
    sequência de Cauchy.
    E pelo Lema \ref{lemma:uniformemente_continua:cauchy_em_cauchy},
    $f(z_n)$ também é de Cauchy.
    E portanto,
    como $Y$ é completo,
    $f(z_n)$ é convergente.

    Para garantir que $g(z)$ é um valor bem definido,
    precisamos mostrar que se $x_n \in Z$
    também é tal que $x_n \rightarrow z$,
    então
    \begin{equation*}
      \lim f(x_n)
      =
      \lim f(z_n).
    \end{equation*}
    Basta prosseguir como no
    Exemplo \ref{example:continuous_extension:x2},
    escolhendo épsilons e deltas adequados.
    Uma outra forma,
    é notando que
    \begin{equation*}
      a_n
      =
      \begin{cases}
        x_{\frac{n}{2}},
        &\text{$n$ é par}
        \\
        z_{\frac{n+1}{2}},
        &\text{$n$ é ímpar}.
      \end{cases}
    \end{equation*}
    é uma sequência de Cauchy que tem $x_n$ e $z_n$ como subsequências.
    Assim,
    pelo Lema \ref{lemma:uniformemente_continua:cauchy_em_cauchy},
    $f(a_n)$ é Cauchy,
    e como $Y$ é completo,
    $\lim f(a_n)$ existe.
    Mas como $f(z_n)$ e $f(x_n)$ são subsequências,
    \begin{equation*}
      \lim f(z_n)
      =
      \lim f(a_n)
      =
      \lim f(x_n).
    \end{equation*}
    E portanto,
    os limites são iguais.
    Note que ainda não sabemos se $g$ é ou não contínua em $z$!
    Isso porque provamos que para $z_n \in Z$,
    \begin{equation*}
      z_n \rightarrow z
      \Rightarrow
      g(z_n) \rightarrow g(z).
    \end{equation*}
    Mas precisamos provar para $z_n \in \closure{Z}$.

    E de fato,
    dado $\varepsilon > 0$,
    escolha $\delta > 0$
    tal que
    \begin{equation*}
      d(a,b) < \delta
      \Rightarrow
      d(f(a), f(b)) < \frac{\varepsilon}{3}.
    \end{equation*}
    Para cada $x \in \closure{Z}$,
    escolha $z_x \in Z$ tal que
    \begin{equation*}
      d(z_x, x) < \delta
      \quad\text{e}\quad
      d(g(z_x), g(x)) < \frac{\varepsilon}{3}.
    \end{equation*}
    Desse modo,
    se $x,y \in \closure{Z}$,
    \begin{multline*}
      d(x,y) < \delta
      \Rightarrow
      d(g(x), g(y))
      \\
      \leq
      d(g(z_x), g(x))
      +
      d(g(x), g(y))
      +
      d(g(y), g(z_y))
      <
      \frac{\varepsilon}{3}
      +
      \frac{\varepsilon}{3}
      +
      \frac{\varepsilon}{3}
      =
      \varepsilon.
    \end{multline*}
    Ou seja,
    $g$ é uniformemente contínua.

    É evidente que $f = g|_Z$,
    pois se $z \in Z$,
    então a sequência constante $z_n = z$ converge para $z$,
    e portanto,
    \begin{equation*}
      g(z)
      =
      \lim f(z_n)
      =
      \lim f(z)
      =
      f(z).
    \end{equation*}
  \end{proof}

  A essência da proposição anterior
  é o fato de funções uniformemente contínuas
  levarem sequências convergentes em sequências convergentes
  quando o contradomínio é completo.
