\section{Continuidade Uniforme}

  Uma métrica $d$ em um conjunto $X$
  induz em $X$ uma topologia.
  Nem todas as topologias são induzidas por uma métrica.
  Por exemplo,
  uma topologia que não é \emph{Hausdorff}
  (Definição \ref{def:espaco_de_Hausdorff})
  não pode ser induzida por uma métrica
  (Proposição \ref{prop:espaco_metrico_eh_hausdorff}).

  Em espaços métricos,
  podemos formular conceitos que não são puramente topológicos.
  Um exemplo é a \emph{continuidade uniforme}.
  Uma função
  $\function{f}{\metricspace{X}}{\metricspace{Y}}$
  entre espaços métricos é contínua em $a \in X$ quando
  para todo $\varepsilon > 0$ existe $\delta > 0$ tal que
  \begin{equation*}
    x \in \ball{\delta}{a}
    \Rightarrow
    f(x) \in \ball{\varepsilon}{f(a)}.
  \end{equation*}
  E a função é contínua quando é contínua em todo ponto.
  Ou seja,
  para todo $a \in X$
  e todo $\varepsilon > 0$ existe $\delta_a > 0$ tal que
  \begin{equation*}
    x \in \ball{\delta_a}{a}
    \Rightarrow
    f(x) \in \ball{\varepsilon}{f(a)}.
  \end{equation*}
  Usamos o índice $a$ em $\delta_a$,
  para enfatizar o fato de que $\delta_a$
  depende da escolha de $a$.
  Pode ser que mesmo $f$ sendo contínua,
  seja necessário um $\delta_a$ diferente para cada $a \in X$.

  \begin{example}
    \label{example:continuidade_uniforme:1_x}
    Considere
    \begin{equation*}
      \functionarray{f}{(0,\infty)}{\reals}{x}{\frac{1}{x}}.
    \end{equation*}
    A função $f$ é contínua.
    Mas repare que fazendo $\varepsilon = 1$
    e fixando $a \in (0,\infty)$,
    $\delta_a$ deve ser no mínimo igual a
    $\frac{a^2}{\abs{1-a}}$,
    para que
    \begin{equation*}
      \abs{x - a} < \delta_a
      \Rightarrow
      \abs{\frac{1}{x} - \frac{1}{a}} < 1.
    \end{equation*}
    Em particular,
    quando $a$ se aproxima de $0$,
    a bola $\ball{\delta_a}{a}$ tem que ter o raio cada vez menor
    para garantir que
    $\abs{\frac{1}{x} - \frac{1}{a}} < 1$.
    Veja a Figura \ref{fig:grafico_1_x}.

% TODO: Figura.
  \end{example}

  \begin{definition}[Continuidade Uniforme em Espaços Métricos]
    \label{def:continuidade_uniforme:metrica}
    Uma função
    $\function{f}{\metricspace{X}}{\metricspace{Y}}$
    entre espaços métricos é \emph{uniformemente contínua} quando
    para todo $\varepsilon > 0$ existe $\delta > 0$ tal que
    para todo $a \in X$,
    \begin{equation*}
      x \in \ball{\delta}{a}
      \Rightarrow
      f(x) \in \ball{\varepsilon}{f(a)}.
    \end{equation*}
  \end{definition}

  A Definição \ref{def:continuidade_uniforme:metrica} pode ser reescrita de modo mais simétrico:
  \begin{quote}
    Uma função
    $\function{f}{\metricspace{X}}{\metricspace{Y}}$
    entre espaços métricos é \emph{uniformemente contínua} quando
    para todo $\varepsilon > 0$ existe $\delta > 0$ tal que
    \begin{equation*}
      \metricop[X](x,y) \leq \delta
      \Rightarrow
      \metricop[Y](f(x),f(y)) \leq \varepsilon.
    \end{equation*}
  \end{quote}

  A função $f$
  do Exemplo \ref{example:continuidade_uniforme:1_x}
  não pode ser estendida continuamente para o domínio
  $[0,\infty)$.
