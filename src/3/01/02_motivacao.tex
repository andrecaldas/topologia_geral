\section{Motivação}

  Considere o conjunto
  \begin{equation*}
    Q
    =
    \rationals \cap [-N, N]
  \end{equation*}
  e a função ``produto''
  \begin{equation*}
    \functionarray{p}{Q \times Q}{\reals}
                  {\left(\frac{a}{b}, \frac{x}{y}\right)}
                  {\frac{ax}{by}}.
  \end{equation*}
  Será possível estender $p$ para todo o conjunto
  $[-N,N] \times [-N,N]$?
  Estender,
  significa especificar
  \begin{equation*}
    \function{f}{[-N,N] \times [-N,N]}{\reals}
  \end{equation*}
  tal que
  $p = f|_{Q \times Q}$.
  Ora,
  estender é sempre possível!
  Por exemplo,
  \begin{equation*}
    f(x)
    =
    \begin{cases}
      p(x),
      &
      x \in Q
      \\
      0,
      &
      x \not \in Q.
    \end{cases}
  \end{equation*}
  Talvez não queiramos apenas estender,
  mas estender continuamente.
  Ou seja,
  encontrar uma tal $f$ que seja também contínua!
  Note que $p$ é \emph{uniformemente contínua},
  pois para todo $\varepsilon > 0$,
  tomando $N$ tal que $\frac{3}{N} < \varepsilon$,
  temos que quando
  $\frac{\alpha}{\beta}, \frac{\gamma}{\delta} < \frac{1}{N^2}$,
  \begin{align*}
    \abs
    {
      p
      \left(
        \frac{a}{b} + \frac{\alpha}{\beta},
        \frac{x}{y} + \frac{\gamma}{\delta}
      \right)
      -
      p
      \left(
        \frac{a}{b},
        \frac{x}{y}
      \right)
    }
    &=
    \abs
    {
      \frac{\alpha}{\beta}
      \frac{\gamma}{\delta}
      +
      \frac{a}{b}
      \frac{\gamma}{\delta}
      +
      \frac{\alpha}{\beta}
      \frac{x}{y}
    }
    \\
    &\leq
    N
    \abs
    {
      \frac{1}{N^2}
      +
      \frac{1}{N^2}
      +
      \frac{1}{N^2}
    }
    \\
    &=
    \frac{3}{N}
    <
    \varepsilon.
  \end{align*}

  Se for possível estender $p$ continuamente a
  $[-N,N] \times [-N,N]$,
  só haverá uma forma de fazê-lo.
  Se
  $(q_n, r_n) \in Q \times Q$
  é tal que
  $(q_n, r_n) \rightarrow (x, y) \in [-N,N] \times [-N,N]$,
  pela continuidade que esperamos de $f$,
  a única alternativa seria definir
  Mas será que $f(q_n, r_n)$ de fato converge?
  E será que todas as outras sequências
  $(s_n, t_n) \in Q \times Q$
  que também convegem para $(x,y)$ são tais que
  \begin{equation*}
    \lim_{n \rightarrow \infty} f(q_n, r_n)
    =
    \lim_{n \rightarrow \infty} f(s_n, t_n)?
  \end{equation*}
  Essas duas perguntas podem ser traduzidas em
  \begin{quote}
    ``Está bem definida a função
    \begin{equation*}
      \functionarray{f}{[-N,N] \times [-N,N]}{\reals}
                    {\lim (q_n, r_n)}{\lim p(q_n, r_n)},
    \end{equation*}
    onde $(q_n, r_n) \in Q \times Q$?''
  \end{quote}

  \begin{example}
    \label{example:continuous_extension:x2}
    A função
    \begin{equation*}
      \functionarray{g}{\reals \setminus \set{0}}{\reals}{x}{\frac{x}{\abs{x}}}
    \end{equation*}
    é contínua.
    É evidente que não podemos estender $g$ a todo o conjunto $\reals$.
    Note que,
    escolhido $\varepsilon = 1$,
    para cada $x \in \reals \setminus \set{0}$,
    $\delta_x > 0$
    tem que satisfazer
    $\delta_x < \abs{x}$ para que
    \begin{equation*}
      \abs{y - x} < \delta_x
      \Rightarrow
      \abs{f(y) - f(x)} < 1.
    \end{equation*}
    Por outro lado,
    observe o gráfico da função
    \begin{equation*}
      \functionarray{g}{[-1,1] \setminus \set{0}}{\reals}{x}{x^2},
    \end{equation*}
    que evidentemente pode ser estendida continuamente a todo o intervalo $[-1,1]$.
  \end{example}

  FIGURA

  Dado $\varepsilon > 0$,
  basta fazer
  $\delta = \frac{\varepsilon}{3}$
  para que
  $\abs{y - x} < \delta$
  implique que
  \begin{align*}
    \abs{y^2 - x^2}
    &\leq
    (x + \delta)^2 - x^2
    \\
    &=
    2y\delta + \delta^2
    \\
    &<
    3\delta
    =
    \varepsilon.
  \end{align*}
  Para $x$ próximo de $0$,
  o intervalo
  $(x - \delta, x + \delta)$
  é tal que
  \begin{equation*}
    0
    \in
    (x - \delta, x + \delta).
  \end{equation*}
  Estendemos $g$,
  fazendo $g(0) = 0$.

  Vamos mostrar que $g$ continua contínua,
  sem utilizarmos a fórmula $g(x)$,
  com um argumento que possa ser aplicado em casos mais gerais.
  Vamos usar apenas o fato de
  $g|_{[-1,1] \setminus \set{0}}$
  ser uniformemente contínua,
  e o fato de
  \begin{equation*}
    g\left(\frac{1}{n}\right)
    =
    \frac{1}{n^2}
    \rightarrow
    0.
  \end{equation*}
  Precisamos apenas mostrar que $g$ é contínua em $0$.
  Pela continuidade uniforme,
  para todo $\varepsilon > 0$,
  existe $\delta > 0$ tal que
  \begin{equation*}
    \abs{y - x} < \delta
    \Rightarrow
    \abs{g(y) - g(x)} < \varepsilon.
  \end{equation*}
  Sendo assim,
  se $a_n \in [-1,1] \setminus \set{0}$
  converge para $0$,
  então existe $N$ tal que
  \begin{align*}
    n, m \geq N
    &\Rightarrow
    \abs{\frac{1}{m}}, \abs{a_n} < \frac{\delta}{2}
    \\
    &\Rightarrow
    \abs{g(a_n) - g\left(\frac{1}{m}\right)} < \varepsilon.
  \end{align*}
  Tomando o limite em $m$,
  \begin{equation*}
    \abs{g(a_n) - 0} \leq \varepsilon.
  \end{equation*}
  Portanto,
  $g(a_n) \rightarrow 0$.

  Usamos apenas o fato de
  $g(a_n) \rightarrow 0$
  para uma sequência fixada
  $a_n = \frac{1}{n}$,
  e mais o fato de $g$ ser uniformemente contínua,
  para conluir que podemos definir $g(0)$ de modo que $g$ continue contínua.

  Para podermos utilizar a técnica
  do Exemplo \ref{example:continuous_extension:x2},
  precisamos de uma forma de garantir que a sequência
  $g(a_n)$ vai convergir.
