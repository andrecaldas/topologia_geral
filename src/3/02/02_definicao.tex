\section{Definição Formal}

  Um \emph{espaço vetorial topológico}
  é um espaço vetorial munido de uma topologia
  \emph{``compatível''} com a estrutura algébrica de espaço vetorial.

  \begin{definition}[Espaço Vetorial Topológico]
    Um \emph{espaço vetorial topológico} $(E, \tau)$ é um espaço vetorial $E$
    munido de uma topologia $\tau$ tal que as operações de soma e produto por escalar são contínuas.
    Para ser específico,
    munindo os espaços $E \times E$ e $\reals \times E$
    com a topologia produto,
    as aplicações
    \begin{align*}
      &\functionarray{p}{\reals \times E}{E}{(\alpha, a)}{\alpha a}
      \\
      &\functionarray{s}{E \times E}{E}{(a, b)}{a + b}
    \end{align*}
    são contínuas.
  \end{definition}

  \begin{obs}
    Evidentemente,
    poderíamos falar de espaços vetoriais complexos.
  \end{obs}

  \begin{obs}
    Alguns autores exigem que a topologia seja Hausdorff
    para que o espaço seja considerado um \emph{espaço vetorial topológico}.
  \end{obs}

  Vejamos alguns exemplos além dos espaços normados.

  \begin{example}[Análise Funcional: topologias fracas]
    \label{example:topologia_fraca}
    Seja $(E, \norm{\cdot})$ um espaço vetorial normado.
    Seja $\family{F}$ a família de todos os funcionais lineares contínuos.
    Ou seja,
    \begin{equation*}
      \function{f}{E}{\reals}
    \end{equation*}
    pertence a $\family{F}$ se, e somente se, $f$ é linear e contínuo.

    A topologia da norma é usualmente chamada de \emph{topologia forte}.
    Algumas vezes vamos dizer que determinada função é \emph{fortemente contínua}
    ao invés de dizermos que é contínua na topologia da norma.
    A topologia fraca induzida por $\family{F}$ em $E$
    também faz de $E$ um espaço vetorial topológico.
    De fato,
    para ver que a soma é contínua,
    pela Proposição \ref{prop:topologia_inicial_com_diagramas:generalizacao},
    basta mostrar que $(a,b) \mapsto f(a+b)$ é contínua para toda $f \in \family{F}$.
    E de fato,
    \begin{equation*}
      f(a + b)
      =
      f(a) + f(b)
    \end{equation*}
    é a composição de duas aplicações contínuas.
    Primeiro,
    $(a,b) \mapsto (f(a), f(b))$,
    que é contínua já que é contínua em cada coordenada
    (Proposição \ref{prop:topologia_produto:continuidade}).
    Depois,
    a soma de números reais,
    que também é contínua.
    Da mesma forma,
    o produto por escalar também é contínuo.

    É evidente que a topologia induzida por $\family{F}$ é mais fraca que a topologia forte.
    Isso porque,
    a topologia induzida é,
    por definição,
    a mais fraca onde todos os elementos de $\family{F}$ são contínuos,
    e a continuidade forte faz parte das hipóteses sobre a família $\family{F}$.

    Na disciplina de \emph{análise funcional},
    quando $\family{F} = E^*$,
    é comum chamar a topologia induzida por $\family{F}$ de \emph{topologia fraca}.
  \end{example}

  \begin{example}[Análise Funcional: topologia fraca-$*$]
    Continuando o Exemplo \ref{example:topologia_fraca},
    dado o espaço normado $E$,
    considere o espaço dual $E^*$ formado pelos funcionais lineares fortemente contínuos.
    A topologia fraca de $E^*$ é induzida pelos funcionais $E^{**}$.

    Agora,
    podemos enxergar $E$ como subespaço de $E^{**}$ através da seguinte identificação.
    \begin{equation*}
      \functionarray{J}{E}{E^{**}}{x}{\functionarray{J(x) = J_x}{E^*}{\reals}{f}{f(x)}}.
    \end{equation*}
    A topologia induzida em $E^*$ por $E$ é comumente chamada de \emph{topologia fraca-$*$}.
    E é mais fraca que a topologia fraca.
  \end{example}
