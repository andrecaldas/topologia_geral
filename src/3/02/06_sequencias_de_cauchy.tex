\section{Sequências de Cauchy e Completude}

  Na Seção \ref{section:completude:metrica},
  em busca de uma ideia do que seriam sequências que \emph{``deveriam''} convergir,
  chegamos ao conceito de \emph{sequência de Cauchy}.
  Em um espaço topológico,
  de um modo geral,
  para mostrarmos que $a_n \rightarrow a$,
  precisamos mostrar que dado $A \in \neighbourhoods{a}$,
  existe $N \in \naturals$ tal que
  \begin{equation*}
    n \geq N
    \Rightarrow
    a_n \in A.
  \end{equation*}
  Para espaços vetoriais topológicos,
  poderíamos dizer que
  dado $V \in \neighbourhoods{0}$,
  existe $N \in \naturals$ tal que
  \begin{equation*}
    n \geq N
    \Rightarrow
    a_n \in a + V.
  \end{equation*}
  Considerando os conjuntos $a_n - V$,
  para $n,m \geq N$,
  temos que
  \begin{equation*}
    a
    \in
    (a_n - V)
    \cap
    (a_m - V).
  \end{equation*}
  Podemos,
  saindo de $a_n$,
  dar um \emph{``salto''} de \emph{``tamanho''} $-V$ para chegarmos em $a$,
  e depois um \emph{``salto''} de \emph{``tamanho''} $-V$ para chegarmos em $a_m$.
  Desse modo,
  a \emph{``distância''} de $a_n$ até $a_m$ não é \emph{``maior''} que $(-V) + (-V)$.

  \begin{obs}
    Em um espaço vetorial,
    o conjunto
    \begin{equation*}
      V + V
      =
      \setsuchthat{v + w}{v,w \in V}
    \end{equation*}
    é diferente de
    \begin{equation*}
      2V
      =
      \setsuchthat{2v}{v \in V}.
    \end{equation*}
  \end{obs}

  Uma forma meio complicada de definir \emph{sequências de Cauchy}
  como sequências que \emph{``deveriam convergir''},
  seria dizer que para todo $V \in \neighbourhoods{0}$,
  existe $N \in \naturals$
  tal que
  \begin{equation*}
    m,n \geq N
    \Rightarrow
    a_n - a_m
    \in
    (-V) + (-V).
  \end{equation*}
  Mas note,
  no entanto,
  que se isso acontecer,
  então,
  para todo $U \in \neighbourhoods{0}$,
  escolhendo $W \in \neighbourhoods{0}$ simétrico e com $W + W \subset U$,
  existe $N \in \naturals$ tal que
  \begin{equation*}
    m,n \geq N
    \Rightarrow
    a_n - a_m
    \in
    (-W) + (-W)
    =
    W + W
    \subset U.
  \end{equation*}

  \begin{definition}[Sequência de Cauchy em Espaços Vetoriais Topológicos]
    \label{definition:cauchy:evt}
    Uma sequência $a_n \in E$ em um espaço vetorial topológico $E$
    é chamada de \emph{sequência de Cauchy},
    ou \emph{sequência fundamental},
    quando para todo $V \in \neighbourhoods{0}$,
    existir $N$ tal que
    \begin{equation*}
      m,n \geq N
      \Rightarrow
      a_n - a_m
      \in
      V.
    \end{equation*}
  \end{definition}

  \begin{obs}
    A Definição \ref{definition:cauchy:evt} poderia ter sido feita
    com a notação da Seção \ref{section:diagonal:evt}.
    Ou seja,
    para todo $V \in \neighbourhoods{0}$,
    existe $N \in \naturals$ tal que
    \begin{equation*}
      m,n \geq N
      \Rightarrow
      (a_m, a_n) \in \uniform{V}.
    \end{equation*}
  \end{obs}

  \begin{definition}[Completude]
    Um espaço vetorial topológico é \emph{sequêncialmente completo}
    quando toda sequência de Cauchy é convergente.
  \end{definition}

  \begin{obs}
    Quando introduzirmos o conceito de \emph{rede},
    poderemos falar de \emph{rede de Cauchy}.
    Nesse caso,
    diremos que o espaço vetorial topológico é \emph{completo}
    quando toda \emph{rede de Cauchy} for convergente.
    Veja a Seção \ref{section:cauchy:nets}.
  \end{obs}
