\section{Abstraindo o Conceito de Uniformidade}

  Uma propriedade comum das estruturas discutidas
  nos Capítulos
  \ref{chapter:uniformidade:metrica},
  \ref{chapter:uniformidade:evt}
  e
  \ref{chapter:uniformidade:grupos}
  é a existência de,
  para cada ponto do espaço,
  uma base de vizinhanças
  $\family{B}_x$
  satisfazendo algumas propriedades comuns.
  Nos espaços metricos,
  são as bolas centradas em $x$.
  Nos epaços vetoriais,
  são os conjuntos da forma $x + V$,
  onde $V$ é uma vizinhança simétrica da origem.
  E nos grupos topológicos,
  sao os conjuntos da forma $gU$,
  onde $U$ é uma vizinhança simétrica da identidade.
  \begin{description}
    \item[Simetria: ]
      \begin{align*}
        x \in \ball{\varepsilon}{y}
        &\Leftrightarrow
        y \in \ball{\varepsilon}{x}
        \\
        \vector{v} \in \vector{w} + V
        &\Leftrightarrow
        \vector{w} \in \vector{v} + V
        \\
        g \in hU
        &\Leftrightarrow
        h \in gU.
      \end{align*}

    \item[Desigualdade Triangular: ]
      Para espaços métricos,
      \begin{equation*}
        y \in \ball{\frac{\varepsilon}{2}}{x},\,
        z \in \ball{\frac{\varepsilon}{2}}{y}
        \Rightarrow
        z \in \ball{\varepsilon}{x}.
      \end{equation*}
      Em outras palavras,
      \begin{equation*}
        \ball{\frac{\varepsilon}{2}}{\ball{\frac{\varepsilon}{2}}{x}}
        =
        \ball{\varepsilon}{x}.
      \end{equation*}
      Para espaços vetoriais topológicos,
      para toda vizinhança da origem $V$
      existe uma vizinhança $W$ tal que
      \begin{equation*}
        \vector{u} \in \vector{v} + W,\,
        \vector{v} \in \vector{w} + W
        \Rightarrow
        \vector{u} \in \vector{w} + V.
      \end{equation*}
      Em outras palavras,
      \begin{equation*}
        W + W
        \subset
        V.
      \end{equation*}
      E da mesma forma,
      para grupos topológicos,
      dada uma vizinhança da identidade $V$,
      existe outra vizinhança $U$ tal que
      \begin{equation*}
        U^2
        \subset
        V.
      \end{equation*}
      E portanto,
      \begin{equation*}
        k \in hU,\,
        h \in gU
        \Rightarrow
        k \in gV.
      \end{equation*}
  \end{description}

  Essas duas propriedades é que são utilizadas a todo momento
  nos Capítulos
  \ref{chapter:uniformidade:metrica},
  \ref{chapter:uniformidade:evt}
  e
  \ref{chapter:uniformidade:grupos}.

  Podemos pensar no símbolo (em negrito)
  $\uniform{\varepsilon}$
  (ou $\uniform{\delta}$),
  não como um número real,
  mas como a família de todas as bolas de raio $\varepsilon$.
  Uma bola para cada $x \in X$.
  \begin{equation*}
    \uniform{\varepsilon}
    =
    \bigcup_{x \in X}
    \set{x}
    \times
    \ball{\varepsilon}{x}.
  \end{equation*}
  Podemos pensar na vizinhança
  $V \in \neighbourhoods{\vector{0}}$
  como representando a família das vizinhanças
  $\vector{a} + V \in \neighbourhoods{\vector{a}}$,
  indexadas por $\vector{a}$:
  \begin{equation*}
    \uniform{V}
    =
    \bigcup_{\vector{a} \in E}
    \set{\vector{a}}
    \times
    (\vector{a} + V).
  \end{equation*}
  Ou,
  em um grupo topológico $G$,
  cada vizinhança $U \in \neighbourhoods{1_G}$
  como sendo correspondente à família das vizinhanças
  $gU \in \neighbourhoods{g}$, indexadas por $g$:
  \begin{equation*}
    \uniform{U}
    =
    \bigcup_{g \in G}
    \set{g}
    \times
    gU.
  \end{equation*}
  Ao invés de dizer
  \begin{equation*}
    d(x,y) < \varepsilon,
  \end{equation*}
  vamos dizer
  \begin{equation*}
    (x,y) \in \uniform{\varepsilon}.
  \end{equation*}
  Analogamente,
  ao invés de dizer
  \begin{equation*}
    \vector{a}
    \in
    \vector{b} + V
    \quad\text{ou}\quad
    \vector{a} - \vector{b} \in V,
  \end{equation*}
  diremos
  \begin{equation*}
    (\vector{a}, \vector{b}) \in \uniform{V}.
  \end{equation*}

  No caso dos espaços métricos,
  a métrica é que determina toda a ``estrutura''.
  Por isso,
  vamos usar o símbolo $\uniform{d}$ para representar
  a \emph{estrutura uniforme} abstrata
  que vamos definir mais a frente.
  Os elementos de $\uniform{d}$,
  em analogia com os espaços métricos,
  serão representados por símbolos como
  $\uniform{\varepsilon}$
  e
  $\uniform{\delta}$.
  A \emph{continuidade uniforme},
  por exemplo,
  assume a seguinte forma:
  \begin{quote}
    ``Para todo $\uniform{\varepsilon} \in \uniform{d}$,
    existe $\uniform{\delta} \in \uniform{d}$ tal que
    \begin{equation*}
      (x,y) \in \uniform{\delta}
      \Rightarrow
      (f(x), f(y)) \in \uniform{\varepsilon}.
    \end{equation*}''
  \end{quote}
  Ou então,
  usando a notação
  \begin{equation*}
    f^{-1}(\uniform{\varepsilon})
    =
    \setsuchthat{(x,y) \in X^2}{(f(x), f(y)) \in \uniform{\varepsilon}},
  \end{equation*}
  assume a forma:
  Para todo $\uniform{\varepsilon} \in \uniform{d}$,
  existe $\uniform{\delta} \in \uniform{d}$ tal que
  \begin{equation*}
    \uniform{\delta}
    \subset
    f^{-1}(\uniform{\varepsilon}).
  \end{equation*}
  Por causa desta última inclusão,
  assim como fizemos com o conceito de \emph{vizinhança},
  percebemos que seria mais interessante incluir em $\uniform{d}$
  todo subconjunto de $X^2$ que contenha algum outro elemento de $\uniform{d}$.
  Ou seja,
  assim como com as vizinhanças de um ponto
  (Definição \ref{def:vizinhanca}),
  vamos assumir que $\uniform{d}$ satisfaz a implicação
  \begin{equation*}
    \uniform{\alpha} \in \uniform{d},
    \uniform{\alpha} \subset \uniform{\beta}
    \Rightarrow
    \uniform{\beta} \in \uniform{d}.
  \end{equation*}
  Antes disso,
  tinhamos apenas uma \emph{base da estrutura uniforme}.
  E com essa hipótese adicional em $\uniform{d}$,
  a continuidade uniforme de $f$ pode ser descrita assim:
  \begin{equation*}
    \uniform{\varepsilon} \in \uniform{d}_Y
    \Rightarrow
    f^{-1}(\uniform{\varepsilon}) \in \uniform{d}_X.
  \end{equation*}
  Ou,
  de modo mais sucinto,
  \begin{equation*}
    f^{-1}(\uniform{d_Y}) \subset \uniform{d}_X.
  \end{equation*}
