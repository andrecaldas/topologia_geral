\section{Definição}

  Devidamente motivados,
  estamos prontos para definir o que vem a ser uma
  \emph{estrutura uniforme}.

  \begin{notation}
    Dado um conjunto $X$,
    vamos denotar por
    \begin{equation*}
      \diagonal
      =
      \setsuchthat{(x,x) \in X^2}{x \in X}
    \end{equation*}
    a \emph{diagonal} de $X^2$.
    E dados $A, B \subset X^2$,
    vamos denotar por
    \begin{equation*}
      A \bullet B
      =
      \setsuchthat{(x,z) \in X^2}{\exists y \in X,\, (x,y) \in A,\, (y,z) \in B}
    \end{equation*}
    os pares que podem ser construídos concatenando um ``salto de $A$''
    e um de $B$.
    E também,
    \begin{equation*}
      A^{-1}
      =
      \setsuchthat{(x,y) \in X^2}{(y,x) \in A}.
    \end{equation*}
  \end{notation}

  \begin{definition}[Estrutura Uniforme]
    \label{definition:uniformidade}
    Seja $X$ um conjunto e $\uniform{d} \subset \parts{X^2}$.
    Dizemos que $\uniform{d}$ é uma estrutura uniforme em $X$ quando:
    \begin{enumerate}
      \item
        \label{it:definition:uniformidade:diagonal}
        $\uniform{\varepsilon} \in \uniform{d}
        \Rightarrow
        \diagonal \subset \uniform{\varepsilon}$.

      \item
        \label{it:definition:uniformidade:superconjunto}
        $\uniform{\alpha} \in \uniform{d},\,
        \uniform{\alpha} \subset \uniform{\beta} \subset X^2
        \Rightarrow
        \uniform{\beta} \in \uniform{d}$.

      \item
        \label{it:definition:uniformidade:intersecao}
        $\uniform{\alpha}, \uniform{\beta} \in \uniform{d}
        \Rightarrow
        \uniform{\alpha} \cap \uniform{\beta} \in \uniform{d}$.

      \item
        \label{it:definition:uniformidade:simetria}
        $\uniform{\varepsilon} \in \uniform{d}
        \Rightarrow
        \uniform{\varepsilon}^{-1} \in \uniform{d}$.

      \item
        \label{it:definition:uniformidade:epsilon_2}
        Para todo $\uniform{\varepsilon} \in \uniform{d}$,
        existe $\uniform{\varepsilon}_2 \in \uniform{d}$
        tal que
        \begin{equation*}
          \uniform{\varepsilon}_2
          \bullet
          \uniform{\varepsilon}_2
          \subset
          \uniform{\varepsilon}.
        \end{equation*}
    \end{enumerate}
  \end{definition}

  Na Definição \ref{definition:uniformidade},
  o item \refitem{it:definition:uniformidade:diagonal}
  corresponde à propriedade $d(x,x) = 0$ dos espaços métricos,
  que em termos de bolas, pode ser entendida como
  \begin{equation*}
    x \in \ball{\varepsilon}{x}
  \end{equation*}
  para qualquer $\varepsilon > 0$.
  Os itens
  \refitem{it:definition:uniformidade:superconjunto}
  e
  \refitem{it:definition:uniformidade:intersecao}
  mostram que $\uniform{d}$ tem propriedades semelhantes
  às vizinhanças de um ponto: é um \emph{filtro}.
  Já o item \refitem{it:definition:uniformidade:simetria}
  corresponde à simetria da métrica:
  $d(x,y) = d(y,x)$.
  E,
  juntamente com a propriedade
  \refitem{it:definition:uniformidade:intersecao},
  que os conjuntoos simétricos
  ($\uniform{\varepsilon} = \uniform{\varepsilon}^{-1}$)
  formam uma base para o filtro $\uniform{d}$.
  O item \refitem{it:definition:uniformidade:epsilon_2}
  viabiliza a estratégia
  de tomar uma \emph{``bola de raio''} $\frac{\varepsilon}{2}$,
  de modo que,
  partindo do \emph{``centro da bola''},
  ao se concatenar dois \emph{``saltos''},
  não é possível sair da \emph{``bola de raio''} $\varepsilon$.
  Este é o papel da desigualdade triangular.

  Vamos formalizar o conceito geral de \emph{continuidade uniforme}.

  \begin{definition}[Continuidade Uniforme]
    Sejam $X$ e $Y$ espaços uniformes.
    A aplicação
    \begin{equation*}
      \function{f}{X}{Y}
    \end{equation*}
    é \emph{uniformemente contínua} quando
    $f^{-1}(\uniform{\varepsilon}) \in \uniform{d}_X$
    para todo
    $\uniform{\varepsilon} \in \uniform{d}_Y$.
    De modo mais sucinto,
    \begin{equation*}
      f^{-1}(\uniform{d}_Y)
      \subset
      \uniform{d}_X.
    \end{equation*}
  \end{definition}

  \begin{example}
    Sabemos que
    \begin{equation*}
      \functionarray{f}{(0,\infty)}{\reals}{x}{\frac{1}{x}}
    \end{equation*}
    não é \emph{uniformemente contínua}.
    Vamos determinar
    $f^{-1}(\uniform{\varepsilon})$
    para
    \begin{equation*}
      \uniform{\varepsilon}
      =
      \setsuchthat{\ball{1}{x}}{x \in (0,\infty)}.
    \end{equation*}
    Veja a Figura \ref{figure:uniformity:counter_example:1_x}.
  \end{example}

  Como já imaginamos,
  uma \emph{estrutura uniforme} $\uniform{d}$ em $X$
  gera uma topologia $\tau_{\uniform{d}}$.
  Nessa topologia,
  as vizinhanças de $x \in X$ serão os conjuntos da forma
  \begin{equation*}
    \ball{\uniform{\varepsilon}}{x}
    =
    \uniform{\varepsilon}
    \cap
    (\set{x} \times X),
  \end{equation*}
  para $\uniform{\varepsilon} \in \uniform{d}$.
  A existência de tal topologia é garantida pela
  Proposição \ref{proposition:uniformity:topologia_induzida}.

  \begin{lemma}
  \end{lemma}

  No caso de espaços métricos,
  tínhamos uma base de vizinhanças para cada $x \in X$
  dada pelas bolas $\ball{\varepsilon}{x}$.
  Usávamos a desigualdade triangular para mostrar
  que cada bola era vizinhança de todos os seus pontos
  (Proposição \ref{prop:a_bola_eh_aberta}),
  ou seja,
  as bolas são abertas
  (Proposição \ref{prop:a_bola_eh_aberta-preparacao}).
  Depois disso,
  mostramos que toda vizinhança de $x$
  contém um conjunto que é vizinhança de todos os seus pontos,
  e que contém $x$
  (item \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
  da Proposição \ref{prop:espaco_metrico:propriedades_vizinhanca}).
  Agora,
  faremos parecido,
  e mostramos que todo conjunto
  $\ball{\uniform{\varepsilon}}{x}$
  contém um conjunto que é vizinhança de todos os seus pontos,
  e que contém $x$.

  \begin{proposition}
    \label{proposition:uniformity:topologia_induzida}
    Seja $X$ um conjunto munido da estrutura uniforme $\uniform{d}$.
    Então,
    a família
    \begin{equation*}
      tau_{\uniform{d}}
      =
      \setsuchthat{A \subset X}
                  {
                    \forall a \in A,\,
                    \exists \uniform{\varepsilon} \in \uniform{d},\,
                    \ball{\uniform{\varepsilon}}{a} \subset A
                  }
    \end{equation*}
    é uma topologia em $X$
    tal que para cada $x \in X$,
    \begin{equation*}
      \neighbourhoods{x}
      =
      \setsuchthat{\ball{\uniform{\varepsilon}}{x}}{\uniform{\varepsilon} \in \uniform{d}}.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    %% É trivial verificar que $tau_{\uniform{d}}$ é uma topologia.
    %% Precisamos apenas verificar que nessa topologia,
    %% as vizinhanças de um ponto $x \in X$
    %% são exatamente os conjuntos $\ball{\uniform{\varepsilon}}{x}$.

    %% Primeiramente,
    %% vamos mostrar que,
    %% fixados $x \in X$
    %% e
    %% $\uniform{\varepsilon} \in \uniform{d}$,
    %% existe
    %% $\uniform{\delta} \in \uniform{d}$
    %% tal que
    %% \begin{equation*}
    %%   \ball{\uniform{\delta}}
    %%   =
    %%   \setsuchthat{a \in \ball{\uniform{\varepsilon}}{x}}
    %%               {
    %%                 \exists \uniform{\varepsilon} \in \uniform{d},\,
    %%                 \ball{\uniform{\varepsilon}}{a} \subset A
    %%               }
    %% \end{equation*}
  \end{proof}

  Para entender melhor os conceitos envolvidos na demonstração
  da Proposição \ref{proposition:uniformity:topologia_induzida},
  veja o Capítulo \ref{ch:trocentas_maneiras_de_descrever_uma_topologia}.
  Em essência,
  o que precisava ser demonstrado é que o \emph{interior} de uma vizinhança $V$ de $x$
  (pontos $v \in V$ para os quais $V$ é vizinhança de $v$)
  é uma vizinhança de $x$.

  \begin{example}
    Considere $X = \set{1, 2, 3}$.
    Suponha que estamos tentando definir uma topologia em $X$
    tal que
    \begin{align*}
      \neighbourhoods{1}
      &=
      \set
      {
        \set{1,2},
        X
      }
      \\
      \neighbourhoods{2}
      &=
      \set
      {
        X
      }
      \\
      \neighbourhoods{3}
      &=
      \set
      {
        X
      }.
    \end{align*}
    Note que neste caso,
    $V = \set{1,2}$ seria uma vizinhança de $1$ tal que
    $\interior{V} = \set{1}$,
    pois $V$ seria vizinhança de $1$ mas não seria vizinhança de $2$.
    Ou seja,
    o interior de $V$ não seria vizinhança de $1$.
    Mas isso não é possível,
    porque a vizinhança $V$ de $1$ deveria conter um aberto $A$ que contém $1$,
    mas como $A$ é vizinhança de todos os seus pontos,
    teríamos que
    \begin{equation*}
      1
      \in
      A
      \subset
      \interior{V}.
    \end{equation*}
    E como $A$ é vizinhança de $1$,
    $\interior{V}$ também deveria ser.
  \end{example}

  \begin{definition}[Topologia Induzida pela Estrutura Uniforme]
    Seja $X$ um conjunto munido da estrutura uniforme $\uniform{d}$.
    A topologia
    $tau_{\uniform{d}}$
    da Proposição \ref{proposition:uniformity:topologia_induzida}
    é a \emph{topologia induzida} em $X$ pela estrutura uniforme $\uniform{d}$.
  \end{definition}
