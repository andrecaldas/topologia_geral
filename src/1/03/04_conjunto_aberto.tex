\section{Conjuntos Abertos}

  Um conjunto aberto é um conjunto que é vizinhança de
  todos os seus pontos. A Proposição
  \ref{prop:a_bola_eh_aberta-preparacao} mostra que em um espaço
  métrico, todas as bolas são abertas. Por isso, muitos autores usam a
  expressão \emph{bola aberta} para se referirem ao que neste livro
  definimos como \emph{bola}. Ainda vamos formalizar isso melhor, mas
  os conjuntos abertos caracterizam toda a topologia do espaço, haja
  visto que a família
  \begin{equation*}
    \family{A}_x = \setsuchthat{V \in \neighbourhoods{x}}{\text{$V$ é aberto}}
  \end{equation*}
  é uma base de vizinhanças de $x$ para todo $x \in X$. (veja o item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior} da Proposição
  \ref{prop:espaco_metrico:propriedades_vizinhanca})

  Conhecendo todos os conjuntos abertos, sabemos quem são as
  sequências convergentes, quais funções são ou não contínuas e,
  conforme já mencionado, quais são as vizinhanças de um ponto.


  \begin{definition}
    \label{def:espaco_metrico:aberto}

    Seja $X$ um espaço métrico. Dizemos que um conjunto $A \subset X$
    é aberto quando satisfaz
    \begin{equation*}
      x \in A \Rightarrow A \in \neighbourhoods{x}.
    \end{equation*}
  \end{definition}


  \begin{definition}
    \label{def:espaco_metrico:topolgia}

    Dado um espaço métrico $(X, d)$, a \emph{topologia de $X$ induzida
    por $d$}, denotada por $\topology{d}$ --- ou, mais comumente, por um
    abuso de notação, denotada por $\topology{X}$ --- é a família de todos os
    abertos de $X$. Isto é,
    \begin{equation*}
      \topology{X} = \setsuchthat{A \subset X}{\text{$A$ é aberto}}.
    \end{equation*}
  \end{definition}



  \begin{proposition}
    \label{prop:espaco_metrico:vizinhancas_abertas_eh_base}

    Seja $X$ um espaço métrico e $x \in X$. Então a família
    \begin{equation*}
      \family{A}_x = \neighbourhoods{x} \cap \topology{X}
    \end{equation*}
    é uma base de vizinhanças de $x$.
  \end{proposition}



  \begin{proof}
    Basta notar que, chamando de $\family{B}$ a coleção de todas as
    bolas centradas em $x$,
    \begin{equation*}
      \family{B} \subset \family{A}_x \subset \neighbourhoods{x}.
    \end{equation*}
    Como $\family{B}$ é uma base de vizinhanças de $x$, qualquer
    família ``entre'' $\family{B}$ e $\neighbourhoods{x}$ também é uma
    base de vizinhanças de $x$. (porquê?)
  \end{proof}



  \begin{proposition}
    \label{prop:espaco_metrico:propriedades_dos_abertos}

    Seja $X$ um espaço métrico. Então, $\topology{X}$ tem as seguintes
    propriedades:
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:propriedades_dos_abertos:conjuntos_triviais}
        $\emptyset, X \in \topology{X}$.

      \item
        \label{it:prop:espaco_metrico:propriedades_dos_abertos:intersecao}
        Se $A, B \in \topology{X}$, então $A \cap B \in \topology{X}$.

      \item
        \label{it:prop:espaco_metrico:propriedades_dos_abertos:uniao_arbitraria}
        Se $A_\lambda \in \topology{X}$ para uma família qualquer de índices $\lambda
        \in \Lambda$, então $\bigcup_{\lambda \in \Lambda} A_\lambda
        \in \topology{X}$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Para o item
    \refitem{it:prop:espaco_metrico:propriedades_dos_abertos:conjuntos_triviais},
    é fácil ver que $X$ é vizinhança de qualquer ponto $x \in X$.
    Para o conjunto vazio \ldots{} note que todos os elementos do
    conjunto vazio satisfazem a propriedade que você quiser. Neste
    caso, a propriedade de terem $\emptyset$ como vizinhança. Em suma:
    \begin{equation*}
      x \in \emptyset \Rightarrow \emptyset \in \neighbourhoods{x}.
    \end{equation*}
    E portanto, $\emptyset \in \topology{X}$.

    O item
    \refitem{it:prop:espaco_metrico:propriedades_dos_abertos:intersecao} é
    consequência do item
    \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:intersecao} da
    Proposição \ref{prop:espaco_metrico:propriedades_vizinhanca}. Ou
    seja, se $x \in A \cap B$, como $A$ e $B$ são vizinhanças de $x$,
    então $A \cap B$ também é. Assim, $A \cap B$ é
    vizinhança de todos os seus pontos.

    Do mesmo modo, o item
    \refitem{it:prop:espaco_metrico:propriedades_dos_abertos:uniao_arbitraria}
    é consequência do item
    \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:superset} da
    Proposição \ref{prop:espaco_metrico:propriedades_vizinhanca}, pois
    \begin{equation*}
      x \in \bigcup_{\lambda \in \Lambda} A_\lambda \Rightarrow
      \exists \lambda \in \Lambda,\, x \in A_\lambda \subset
        \bigcup_{\lambda \in \Lambda} A_\lambda \Rightarrow
      \bigcup_{\lambda \in \Lambda} A_\lambda \in \neighbourhoods{x}.
    \end{equation*}
    Ou seja, $\bigcup_{\lambda \in \Lambda} A_\lambda$ é vizinhança de
    todos os seus pontos e é portanto aberto.
  \end{proof}



  \begin{proposition}
    \label{prop:espaco_metrico:abeto_eh_uniao_de_bolas}

    Seja $X$ um espaço métrico e $A \subset X$. Então, são
    equivalentes:
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:aberto_e_uniao_de_bolas:aberto}
        O conjunto $A$ é aberto.

      \item
        \label{it:prop:espaco_metrico:aberto_e_uniao_de_bolas:uniao_de_bolas}
        O conjunto $A$ pode ser escrito como uma união de bolas.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Se $A$ é aberto, então para cada ponto $x \in A$ existe uma bola
    $B_x$ centrada em $x$ e contida em $A$. Desta forma, é evidente
    que
    \begin{equation*}
      A = \bigcup_{x \in A} B_x.
    \end{equation*}
    Ou seja, $A$ é uma união de bolas.

    Por outro lado, sabemos que as bolas são conjunto abertos. Assim,
    qualquer conjunto que seja uma união de bolas é, pelo item
    \refitem{it:prop:espaco_metrico:propriedades_dos_abertos:uniao_arbitraria}
    da Proposição \ref{prop:espaco_metrico:propriedades_dos_abertos},
    um conjunto aberto.
  \end{proof}



  \subsection{Sequências e Convergência com Abertos}

    Dado um espaço métrico $X$. Podemos caracterizar o fenômeno de
    convergência em termos de sua topologia $\topology{X}$? De fato, para
    dizer se $x_n \in X$ converge ou não para um certo $x \in X$, de
    acordo com a Proposição
    \ref{prop:espaco_metrico:convergencia:base_de_vizinhancas},
    precisamos apenas conhecer uma base de vizinhanças de $x$
    qualquer.  Sabemos que os abertos que contém $x$ formam uma base
    de vizinhanças de $x$. Sendo assim, colcluímos que $x_n$ converge
    para $x$ se, e somente se, para todo aberto $A$ que contenha o
    ponto $x$ existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow x_n \in A.
    \end{equation*}

%TODO: exercícios
% Se para todo x, x_n \xrightarrow{d_1} x \Rightarrow x_n
% \xrightarrow{d_2} x... o que pode ser dito sobre \topology{1} e \topology{2}.
