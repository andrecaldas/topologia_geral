\section{Vizinhanças}

  Quando falamos de convergência e continuidade nos capítulos anteriores,
  estávamos de posse de uma métrica. A métrica nos dava a noção de distância
  que nos permitia falar de ``proximidade''. Quando dizemos que
  $x_n$ converge para $x$, não estamos de fato interessado nos pontos
  que estão ``longe'' de $x$.
  Estamos interessados apenas nos que estão ``próximos''. De fato,
  poderíamos nos restringir apenas a bolas ``pequeninas''.
  Poderíamos nos restringir a bolas de raio menor que $1$. Ou então,
  a bolas de raio $\frac{1}{2^n}$. Ou, de modo um pouco mais geral,
  poderíamos nos restringir a bolas de raio $\varepsilon_n > 0$, onde
  $\varepsilon_n$ é uma sequência qualquer tal que $\varepsilon_n
  \rightarrow 0$.

  Quando $x_n$ converge para $x$, é porque se $V$ é um conjunto que contém
  $x$ e é de certa forma um conjunto \emph{suficientemente grande}, conterá
  toda a sequência $x_n$, exceto para uma quantidade finita de
  índices $n$. Esse \emph{suficientemente grande}, no caso de espaços métricos,
  significa que existe uma bola $B$ centrada em $x$ tal que
  $B \subset V$. A esses conjuntos \emph{suficientemente grandes},
  chamamos de \emph{vizinhanças de $x$}.
  (veja a Proposição \ref{prop:espaco_metrico:convergencia:bolas})



  \begin{definition}
    \label{def:espaco_metrico:vizinhanca}

    Seja $X$ um espaço métrico e $x \in X$. Todo conjunto $V \subset
    X$ que contém uma bola centrada em $x$ é chamado de
    \emph{vizinhança de $x$}.  Denotamos por $\neighbourhoods{x}$ o
    conjunto de todas as vizinhanças do ponto $x$.
  \end{definition}


  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/a_neighbourhood}
    \end{center}
    \caption{O conjunto $V$ é uma vizinhança $x$ pois é ``grande''
      o suficiente para conter uma bola centrada em $x$.}

    \label{fig:vizinhanca_de_um_ponto}
  \end{figure}

%TODO: Exercício.
%V \in \neighbourhoods{x} sse existir n tal que B_{\frac{1}{n}} \subset V.

  É imediato que toda bola centrada em $x$ é uma vizinhança de $x$.
  Mais do que isso, pela Proposição
  \ref{prop:a_bola_eh_aberta-preparacao},
  uma bola é vizinhança de todos os seus pontos. Esta propriedade está
  formalizada na proposição seguinte.

  \begin{proposition}
    \label{prop:a_bola_eh_aberta}

    Se $B \subset X$ é uma bola em um espaço métrico $X$. Então,
    \begin{equation*}
      y \in B \Rightarrow B \in \neighbourhoods{y}.
    \end{equation*}
    Ou seja, uma bola é vizinhança de todos os seus pontos.
  \end{proposition}

  \begin{proof}
    Veja a Proposição \ref{prop:a_bola_eh_aberta-preparacao}.
    Ou, para um argumento mais visual, compare as
    Figuras
    \ref{fig:a_bola_eh_aberta} e \ref{fig:vizinhanca_de_um_ponto}.
  \end{proof}



  A seguir, apresentamos algumas propriedades elementares das
  vizinhanças de um ponto.

  \begin{proposition}
    \label{prop:espaco_metrico:convergencia_com_vizinhancas}

    Seja $X$ um espaço métrico e $x_n, x \in X$. Então
    \begin{equation*}
      x_n \rightarrow x
    \end{equation*}
    se, e somente se, para toda vizinhança de $x$, $V \in
    \neighbourhoods{x}$, existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow x_n \in V.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Tome uma bola $B$ centrada em $x$ tal que $B \subset V$. Para esta
    bola existe $N$ tal que
    \begin{equation*}
      n \geq N \Rightarrow x_n \in B.
    \end{equation*}
    Em particular,
    \begin{equation*}
      n \geq N \Rightarrow x_n \in V.
    \end{equation*}
  \end{proof}



  \begin{proposition}
    \label{prop:espaco_metrico:propriedades_vizinhanca}

    Seja $X$ um espaço métrico e $x \in X$. Então valem as seguintes
    afirmações sobre a família $\neighbourhoods{x}$ de todas as
    vizinhanças de $x$:
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:propriedades_vizinhanca:superset}
        Se $A \in \neighbourhoods{x}$ e $A \subset B$,
        então $B \in \neighbourhoods{x}$.

      \item
        \label{it:prop:espaco_metrico:propriedades_vizinhanca:intersecao}
        A interseção de duas vizinhanças de $x$ também é uma vizinhança de $x$.
        Ou seja, se $A, B \in \neighbourhoods{x}$,
        então $A \cap B \in \neighbourhoods{x}$.

      \item
        \label{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
        Se $A \in \neighbourhoods{x}$ então existe $B \subset A$ tal
        que $x \in B$ e $B$ é vizinhança de todos os seus pontos.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    O item \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:superset}
    é imediato.

    O item \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:intersecao}
    é imediato do fato que as bolas centradas em $x$ são totalmente
    ordenadas. Ou seja, a que tiver o menor raio estará contida em ambos
    os conjuntos.

    O item \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
    é uma re-interpretação da Proposição \ref{prop:a_bola_eh_aberta}.
    Basta tomar $B$ como sendo uma bola centrada em $x$ contida em $A$.
  \end{proof}


  \begin{obs}
    \label{obs:espaco_metrico:fecho_idempotente}

    Das propriedades listadas na Proposição
    \ref{prop:espaco_metrico:propriedades_vizinhanca}, o item
    \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
    é o de interpretação mais difícil. Vamos voltar a discutí-lo no
    em vários momentos durante a exposição sobre topologia geral,
    e principalmente no
    Capítulo \ref{ch:trocentas_maneiras_de_descrever_uma_topologia}.
    Uma das implicações do item
    \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior},
    é a seguinte. A explicação pode ser acompanhada
    na Figura \ref{fig:diagonal_sequence}.
    Seja $V \in \neighbourhoods{x}$.
    Suponha que para cada $n \in \naturals$
    tenhamos uma sequência $x^n_m \not \in V$, indexada por $m$, que
    converge para $x_n$.  Então, não é possível que a sequência $x_n$
    convirja para $x$. De fato, o item
    \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
    da Proposição implica na existência de uma vizinhança aberta de
    $x$ contida em $V$. Vamos chamar essa vizinhança de $B$, que na
    figura representamos sugestivamente como uma ``bola''. Assim, se
    tivéssemos que $x_n$ converge para $x$, teríamos que para algum $k
    \in \naturals$, $x_k \in B$. Como $B$ também é vizinhança de $x_k$
    (já que é vizinhança de todos os seus pontos), e como $x_m^k
    \rightarrow x_k$, teríamos que para algum $j \in \naturals$,
    $x_j^k \in B$. Contrariando o fato de que $x_j^k \not \in V$.

    \begin{figure}[!htp]
      \begin{center}
        \includegraphics{figs/diagonal_sequence}
      \end{center}
      \caption{
        Se $x_n \rightarrow x$, então algum $x_m^n$ pertence a
        $V$. Este fato se deve ao item
        \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
        da Proposição
        \ref{prop:espaco_metrico:propriedades_vizinhanca}.
      }

      \label{fig:diagonal_sequence}
    \end{figure}
  \end{obs}


  {\input{1/03/exercicios_01}}
