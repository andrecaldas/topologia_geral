\section{Continuidade em Todo Ponto}

  Uma aplicação $\function{f}{X}{Y}$ entre espaços métricos $X$ e $Y$
  é contínua quando é contínua em todo ponto do seu domínio. Se
  considerarmos $\function{f^{-1}}{\parts{Y}}{\parts{X}}$, a função $f$
  será contínua em $x \in X$ quando $f^{-1}$ levar vizinhanças de
  $f(x)$ em vizinhanças de $x$. Sendo assim, para $f$ contínua,
  se $A \subset Y$ for um conjunto aberto (vizinhança de todos os seus
  pontos), então $f^{-1}(A)$ será também vizinhança de todos os seus
  pontos. Ou seja, se $f$ é contínua, então $f^{-1}(\topology{Y}) \subset
  \topology{X}$. Vamos formalizar isso.

  \begin{proposition}
    \label{prop:espaco_metrico:continuidade:abertos}

    Sejam $X$ e $Y$ espaços métricos, e $\function{f}{X}{Y}$ uma
    função qualquer. As seguintes afirmações são equivalentes:
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:continuidade:abertos:definicao}
        A função $f$ é contínua em todo ponto de $X$.

      \item
        \label{it:prop:espaco_metrico:continuidade:abertos:com_abertos}
        A imagem inversa de um aberto é também um conjunto aberto. Ou
        seja, $f^{-1}(\topology{Y}) \subset \topology{X}$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      \refitem{it:prop:espaco_metrico:continuidade:abertos:com_abertos}
      $\Rightarrow$
      \refitem{it:prop:espaco_metrico:continuidade:abertos:definicao}
    }
    {
      Seja $A \in \topology{Y}$. Então, para todo $x \in f^{-1}(A)$ temos que
      $A$ é vizinhança de $f(x)$, e pela Proposição
      \ref{prop:espaco_metrico:continuidade:vizinhancas}, $f^{-1}(A)$ é
      vizinhança de $x$. Ou seja, $f^{-1}(A)$ é aberto.
    }

    \proofitem
    {
      \refitem{it:prop:espaco_metrico:continuidade:abertos:definicao}
      $\Rightarrow$
      \refitem{it:prop:espaco_metrico:continuidade:abertos:com_abertos}
    }
    {
      Sabemos pela Proposição
      \ref{prop:espaco_metrico:vizinhancas_abertas_eh_base} que para
      todo $x \in X$,
      \begin{equation*}
        \family{A}_{f(x)}
        =
        \neighbourhoods{f(x)} \cap \topology{Y}
      \end{equation*}
      é uma base de vizinhanças de $f(x)$. Pelo item
      \refitem{it:prop:espaco_metrico:continuidade:abertos:com_abertos},
      temos que $f^{-1}(\family{A}_{f(x)})$ é aberto e obviamente contém
      $x$. Ou seja, $f^{-1}(\family{A}_{f(x)}) \in
      \neighbourhoods{x}$. Pela Proposição
      \ref{prop:espaco_metrico:continuidade_em_um_ponto:base_de_vizinhancas},
      segue que $f$ é contínua em $x$.
    }
  \end{proof}



  O exemplo seguinte mostra que nem toda bijeção contínua tem inversa
  contínua.

  \begin{example}
    Considere $(\reals, d_{\abs{\cdot}})$ e $(\reals, d_d)$ os espaços
    métricos dados pelos números reais com a métrica euclidiana
    (Exemplo \ref{ex:metrica_euclidiana_nos_reais}) e a métrica
    discreta (Exemplo \ref{ex:metrica_discreta}),
    respectivamente. Então, a aplicação identidade
    \begin{equation*}
      \function{\identity}{(\reals, d_d)}{(\reals, d_{\abs{\cdot}})}
    \end{equation*}
    é contínua, mas sua inversa não é. De fato, na topologia dada pela
    métrica discreta, todos os conjuntos são abertos. Ou seja,
    $\topology{d_d} = \parts{\reals}$.
  \end{example}

  E o que significa então dizer que $\function{f}{X}{Y}$ é bijetiva,
  contínua e sua inversa é contínua? O fato de ser uma bijeção implica
  que podemos identificar os pontos de $X$ com os pontos de $Y$. O
  fato de ser contínua com inversa contínua significa que com essa
  identificação as estruturas topológicas $\topology{X}$ e $\topology{Y}$ também
  são identificadas.
  Nesse caso, dizemos que $f$ é um \emph{homeomorfismo}.
  De modo geral, quando $\function{f}{(X,d_X)}{(Y,d_Y)}$ é
  uma função bijetiva qualquer, contínua ou não, com inversa
  também podendo ser ou não contínua, podemos transportar a métrica
  $d_Y$ para $X$ como feito no Exemplo
  \ref{ex:espaco_metrico:identificando_dois_conjuntos}:
  \begin{equation*}
    d'(a, b) = d_Y(f(a), f(b)).
  \end{equation*}
  Desta forma, reduzimos o problema ao caso da aplicação identidade
  \begin{equation*}
    \function{\identity}{(X, d_X)}{(X, d')},
  \end{equation*}
  pois a aplicação $f$ será contínua (ou sua inversa será contínua)
  se, e somente se, a identidade o for. Em outras palavras, dizer que
  $f$ é contínua é o mesmo que dizer que $\topology{d'} \subset
  \topology{d_X}$. Dizer que a inversa de $f$ é contínua, é o mesmo que
  dizer que $\topology{d_X} \subset \topology{d'}$. Assim, $f$ será um
  homeomorfismo quando $\topology{d'} = \topology{d_X}$.


  \begin{definition}
    \label{def:espaco_metrico:homomorfismo}

    Se $X$ e $Y$ são espaços métricos, então uma função
    $\function{f}{X}{Y}$ é chamada de \emph{homeomorfismo} quando é
    bijetiva, contínua com inversa também contínua.
  \end{definition}
