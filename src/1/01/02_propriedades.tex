\section{Propriedades Elementares}

  Nesta seção, $(X,d)$ é um espaço métrico.
  As propriedades mais interessantes dos espaços métricos são
  conseqüência da desigualdade triangular. Muitas vezes, essas
  propriedades são mais fáceis de serem visualizadas quando
  temos em mente a distância euclidiana em $\reals^2$.
  Ou seja, quando fazemos um desenho em uma folha de papel.
  É importante enfatizar no entanto, que os resultados dependem apenas
  das propriedades das métricas (Definição \ref{def:metrica}).
  O desenho melhora a intuição, mas não é uma demonstração.

  Todas as proposições deste capítulo são muito simples.
  O leitor deve ser capaz de completar as demonstrações
  que afirmam, por exemplo, que basta tomar um certo $\delta > 0$
  para concluir a demonstração.

  \begin{proposition}
    \label{prop:espaco_metrico_tem_base_local_enumeravel}

    Sejam $x \in X$ e $\varepsilon > 0$. Então existe $n \in \naturals$
    tal que
    \begin{equation*}
      \ball{\frac{1}{n}}{x} \subset \ball{\varepsilon}{x}.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Basta tomar $n$ grande o suficiente para que
    $\frac{1}{n} \leq \varepsilon$.
  \end{proof}



  A seguinte Proposição, apesar de muito simples, é fundamental
  para o desenvolvimento de toda a teoria que se seguirá,
  e é conseqüência direta da desigualdade triangular.

  \begin{proposition}
    \label{prop:a_bola_eh_aberta-preparacao}

    Sejam $x \in X$, $\varepsilon > 0$ e
    \begin{equation*}
      y \in \ball{\varepsilon}{x}.
    \end{equation*}
    Então, existe $\delta > 0$ tal que
    \begin{equation*}
      \ball{\delta}{y} \subset \ball{\varepsilon}{x}.
    \end{equation*}
    Veja a Figura \ref{fig:a_bola_eh_aberta}.
  \end{proposition}

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/the_ball_is_open}
    \end{center}
    \caption{Para cada ponto $y$ da bola $B_\varepsilon(x)$, temos uma
    ``bolinha'' centrada em $y$ e toda contida em $B_\varepsilon(x)$.}

    \label{fig:a_bola_eh_aberta}
  \end{figure}

  \begin{proof}
    Basta tomar $\delta < \varepsilon - d(x,y)$.
    Neste caso,
    \begin{equation*}
      \begin{split}
        z \in \ball{\delta}{y} &\Rightarrow d(y,z) < \delta \\
          &\Rightarrow d(x,z) < d(x,y) + \delta < \varepsilon \\
          &\Rightarrow z \in \ball{\varepsilon}{x}.
      \end{split}
    \end{equation*}
  \end{proof}



  \begin{proposition}
    \label{prop:intersecao_de_duas_bolas_eh_aberta-preparacao}

    Sejam $x_1,x_2 \in X$, e $\varepsilon_1, \varepsilon_2 > 0$. Então,
    dado $z \in \ball{\varepsilon_1}{x_1} \cap \ball{\varepsilon_2}{x_2}$,
    existe $\delta > 0$ tal que
    \begin{equation*}
      \ball{\delta}{z} \subset \ball{\varepsilon_1}{x_1} \cap \ball{\varepsilon_2}{x_2}.
    \end{equation*}
    Veja a Figura \ref{fig:intersecao_de_duas_bolas_eh_aberta}.
  \end{proposition}

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/ball_intersection_is_open}
    \end{center}
    \caption{Para cada ponto $z$ da interseção
    $B_{\varepsilon_1}(x) \cap B_{\varepsilon_2}(y)$, temos uma
    ``bolinha'' centrada em $z$ e toda contida na interseção.}

    \label{fig:intersecao_de_duas_bolas_eh_aberta}
  \end{figure}

  \begin{proof}
    Pela Proposição \ref{prop:a_bola_eh_aberta-preparacao}, existem
    $\delta_1, \delta_2 > 0$ tais que
    \begin{eqnarray*}
      \ball{\delta_1}{z} &\subset& \ball{\varepsilon_1}{x_1} \\
      \ball{\delta_2}{z} &\subset& \ball{\varepsilon_2}{x_2}.
    \end{eqnarray*}
    Basta portanto tomar qualquer $\delta \leq \min(\delta_1,\delta_2)$.
  \end{proof}

  Repare que a proposição ``vale'' para qualquer número finito de
  bolas $\ball{\varepsilon_1}{x_1}, \dotsc, \ball{\varepsilon_n}{x_n}$.
  Mas não ``vale'' para um número infinito de bolas.



  \begin{proposition}
    \label{prop:espaco_metrico_eh_hausdorff}

    Sejam $x,y \in X$ dois pontos distintos de $X$.
    Então existe $\varepsilon > 0$ tal que
    \begin{equation*}
      \ball{\varepsilon}{x} \cap \ball{\varepsilon}{y} = \emptyset.
    \end{equation*}
    Veja a Figura \ref{fig:espaco_metrico_eh_hausdorff}.
  \end{proposition}

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/metric_space_is_hausdorff}
    \end{center}
    \caption{Dois pontos distintos $x$ e $y$ podem ser ``separados''
    por bolas disjuntas.}

    \label{fig:espaco_metrico_eh_hausdorff}
  \end{figure}

  \begin{proof}
    Como $x \neq y$, temos que $d(x,y) > 0$. Basta tomar
    \begin{equation*}
      \varepsilon \leq \frac{d(x,y)}{2}.
    \end{equation*}
  \end{proof}



  \begin{proposition}
    \label{prop:intersecao_de_todas_as_bolas}

    Seja $x \in X$. Então,
    \begin{equation*}
      \bigcap_{\varepsilon > 0} \ball{\varepsilon}{x} = \{x\}.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Basta mostrar que dado $y \in X$ com $y \neq x$, existe
    $\varepsilon > 0$ tal que
    \begin{equation*}
      y \not \in \ball{\varepsilon}{x}.
    \end{equation*}
    Basta tomar $\varepsilon \leq d(x, y)$.
    Ou então notar que isso segue como um caso particular
    da Proposição \ref{prop:espaco_metrico_eh_hausdorff}.
  \end{proof}


  {\input{1/01/exercicios_02}}
