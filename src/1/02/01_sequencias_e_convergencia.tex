\section{Seqüências e Convergência}

  Seja $n \in \naturals$.
  A sequência de pontos $x_n = \frac{1}{n}$ é tal que,
  \emph{``na medida que $n$ se torna suficientemente grande,
  a sequência $x_n$ se aproxima de $0$''}.
  Nesta sessão, vamos formalizar o que entendemos por:
  \begin{quote}
    Na medida que $n$ se torna suficientemente grande,
    $\frac{1}{n}$ se aproxima de $0$.
  \end{quote}
  Para um espaço métrico $X$, a noção de ``se aproxima de''
  é um tanto quanto natural, já que temos uma métrica
  que nos dá uma noção de distância.
  A grosso modo, $x_n \in X$ se aproxima de $x$ quando a distância entre
  $x_n$ e $x$, $d(x_n, x)$, se aproxima de $0$. Faltaria então definir
  o que significa dizer que a sequência de números reais $d(x_n, x)$
  ``se aproxima'' de $0$.

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/frac_pow2_-n_aproaches_0}
    \end{center}
    \caption{A sequência $\frac{1}{2^n}$ ``se aproxima'' de $0$.}

    \label{fig:se_aproxima_de_0}
  \end{figure}


  \begin{definition}[Convergência]
    \label{def:espaco_metrico:convergencia_de_sequencias}

    Sejam $(X, d)$ um espaço métrico e $x_n \in X$ ($n \in \naturals$)
    uma sequência de pontos de $X$. Dizemos que $x_n$ converge para
    um certo $x \in X$, quando para todo $\varepsilon > 0$,
    existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow d(x_n, x) < \varepsilon.
    \end{equation*}
    Denotamos tal fato por
    \begin{equation*}
      x_n \rightarrow x,
    \end{equation*}
    ou por $x_n \xrightarrow{d} x$ se quisermos enfatizar que a
    convergência é na métrica $d$.

    Também dizemos que $x$ é o limite da sequencia $x_n$ e escrevemos
    $x = \lim x_n$.
  \end{definition}

  A Definição \ref{def:espaco_metrico:convergencia_de_sequencias} generaliza
  o que já fazemos para os números reais. No caso dos números reais,
  usualmente adotamos a métrica $d(x,y) = \abs{y-x}$.

  \begin{definition}[Convergência usual em $\reals$]
    \label{def:espaco_metrico:convergencia_nos_reais}

    Seja $\alpha_n \in \reals$ ($n \in \naturals$).
    Dizemos que $\alpha_n$ converge para $\alpha \in \reals$,
    e denotamos tal fato por
    $\alpha_n \rightarrow \alpha$, quando para todo $\varepsilon > 0$,
    existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow \abs{\alpha - \alpha_n} < \varepsilon.
    \end{equation*}
  \end{definition}

  Poderíamos ter tomado um outro caminho. Já de posse da definição
  \ref{def:espaco_metrico:convergencia_nos_reais}, poderíamos ter
  definido convergência em espaços métricos de acordo com a seguinte
  proposição.

  \begin{proposition}
    Seja $x_n \in X$ uma sequencia. Faça $d_n = d(x_n, x)$. Então
    \begin{equation*}
      x_n \rightarrow x \Leftrightarrow d_n \rightarrow 0.
    \end{equation*}
    Onde a convergência do lado direito é dada pela
    Definição \ref{def:espaco_metrico:convergencia_nos_reais} ou,
    equivalentemente, pela métrica euclidiana em $\reals$.
  \end{proposition}

  \begin{proof}
    É evidente, pois $d(x_n, x) \rightarrow 0$ se, e somente se,
    para todo $\varepsilon > 0$, existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow d(x_n, x) < \varepsilon.
    \end{equation*}
  \end{proof}


  \begin{proposition}
    \label{prop:espaco_metrico:convergencia_caracterizacao}
    Seja $x_n \in X$ uma sequencia e $x \in X$. Então são equivalentes:
    \begin{enumerate}
      \item
        \label{prop:espaco_metrico:convergencia_caracterizacao:definicao}
        A sequência converge para $x$: $x_n \rightarrow x$.

      \item
        \label{prop:espaco_metrico:convergencia_caracterizacao:por_bolas_quaisquer}
        Para todo $\varepsilon > 0$, existe $N \in \naturals$ tal que
        $n \geq N \Rightarrow x_n \in B_\varepsilon(x)$.

      \item
        \label{prop:espaco_metrico:convergencia_caracterizacao:por_bolas_1_m}
        Para todo $m \in \naturals$, existe $N \in \naturals$ tal que
        $n \geq N \Rightarrow x_n \in B_{\frac{1}{m}}(x)$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    A equivalência entre os
    itens \refitem{prop:espaco_metrico:convergencia_caracterizacao:por_bolas_quaisquer}
    e \refitem{prop:espaco_metrico:convergencia_caracterizacao:por_bolas_1_m}
    segue da Proposição \ref{prop:espaco_metrico_tem_base_local_enumeravel}.

    Para a equivalência entre
    \refitem{prop:espaco_metrico:convergencia_caracterizacao:definicao}
    e \refitem{prop:espaco_metrico:convergencia_caracterizacao:por_bolas_quaisquer},
    basta notar que
    $x_n \in B_\varepsilon(x) \Leftrightarrow d(x_n, x) < \varepsilon$,
    e então fazer a substituição na
    Definição \ref{def:espaco_metrico:convergencia_de_sequencias}.
  \end{proof}


  \begin{definition}[Métricas topologicamente equivalentes]
    \label{def:metricas_topologicamente_equivalentes}
    Enquanto não definimos o que é uma topologia, vamos dizer que duas
    métricas $d_1$ e $d_2$ sobre $X$ determinam a mesma topologia
    (são \emph{topologicamente equivalentes}) quando
    \begin{equation*}
      x_n \xrightarrow{d_1} x \Leftrightarrow x_n \xrightarrow{d_2} x.
    \end{equation*}
  \end{definition}


  O objetivo da primeira parte deste livro é o de dar motivação
  para os conceitos de topologia geral que serão apresentados
  na segunda parte. A este propósito serve a
  Proposição \ref{prop:espaco_metrico:convergencia_caracterizacao},
  que apresenta maneira alternativas de se olhar para a convergência de
  sequencias em espaços métricos. Na medida em que
  substituímos a métrica $d(x_n,x)$ pela bola $B_\varepsilon(x)$,
  as formulações ficam mais parecidas com suas correspondentes para
  espaços topológicos gerais


  {\input{1/02/exercicios_01}}
