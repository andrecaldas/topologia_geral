\section{Topologia com Bolas}
  \label{sec:topologia_com_bolas}

  Até o presente momento, temos trabalhado com sequências.
  Nesta seção vamos formular os mesmos conceitos utilizando bolas.
  Para que a transição entre sequências e bolas seja suave, vamos
  começar reavaliando a
  Proposição \ref{prop:espaco_metrico:convergencia_caracterizacao}.

  A proposição afirma que dizer que $x_n$ converge para $x$ é o mesmo que dizer
  que toda bola centrada em $x$ contém todos os $x_n$,
  exceto talvez para uma quantidade finita de índices $n$.
  Note que na Proposição \ref{prop:espaco_metrico:convergencia_caracterizacao}
  falávamos em ``para todo $\varepsilon > 0$'', mas isso é o mesmo que
  dizer ``para toda bola''!

  Resumindo o que já havia sido feito, temos a seguinte
  caracterização para a convergência de uma sequência.

  \begin{proposition}
    \label{prop:espaco_metrico:convergencia:bolas}

    Seja $X$ um espaço métrico e $x_n \in X$ uma sequência de elementos
    de $X$. Então, $x_n$ converge para $x \in X$ se, e somente se,
    para toda bola $B_\varepsilon(x)$ centrada em $x$,
    existir $N \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow x_n \in B_\varepsilon(x).
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Veja a Proposição \ref{prop:espaco_metrico:convergencia_caracterizacao}.
  \end{proof}


  \begin{proposition}
    \label{prop:espaco_metrico:continuidade:bolas}

    Sejam $X$ e $Y$ espaços métricos. Então as seguintes afirmações
    são equivalentes:
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:continuidade:bolas:definicao}
        A função $\function{f}{X}{Y}$ é contínua em $a \in X$.

      \item
        \label{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
        Para toda bola $B_{f(a)} = B_\varepsilon(f(a))$ centrada em $f(a)$,
        existe uma bola $B_a = B_\delta(a)$ centrada em $a$, tal que
        \begin{equation*}
          f(B_a) \subset B_{f(a)}.
        \end{equation*}

      \item
        \label{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
        Para toda bola $B = B_\varepsilon(f(a))$ centrada em $f(a)$,
        $f^{-1}(B)$ contém alguma bola centrada em $a$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      \refitem{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
      $\Leftrightarrow$
      \refitem{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
    }
    {
      A equivalência entre os
      itens \refitem{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
      e \refitem{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
      é evidente, já que dizer que existe uma bola é o mesmo que dizer que
      existe $\delta > 0$.
    }


    \proofitem
    {
      \refitem{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
      $\Rightarrow$
      \refitem{it:prop:espaco_metrico:continuidade:bolas:definicao}
    }
    {
      Vamos mostrar que
      o item \refitem{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
      implica na continuidade de $f$ no ponto $a$ de acordo com a
      Definição \ref{def:espaco_metrico:continuidade_em_um_ponto:sequencias}.
      Seja $x_n \rightarrow a$. Vamos mostrar que $f(x_n) \rightarrow f(a)$.
      Tome uma bola qualquer $B$ centrada em $f(a)$.
      Por hipótese, existe uma bola $B_a$ centrada em $a$ tal que
      \begin{equation*}
        f(B_a) \subset B.
      \end{equation*}
      Pela Proposição \ref{prop:espaco_metrico:convergencia:bolas},
      temos que $x_n \in B_a$ exceto para um número
      finito de índices $n$. Ou seja, $f(x_n) \in f(B_a) \subset B$,
      exceto para um
      número finito de índices. O que pela
      Proposição \ref{prop:espaco_metrico:convergencia:bolas} é o mesmo que
      dizer que $f(x_n) \rightarrow f(a)$.
    }


    \proofitem
    {
      \refitem{it:prop:espaco_metrico:continuidade:bolas:definicao}
      $\Rightarrow$
      \refitem{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
    }
    {
      Suponha então que
      o item \refitem{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
      não vale. Neste caso, existe uma bola $B$ centrada em $f(a)$
      tal que $f^{-1}(B)$ não contém nenhuma bola centrada em $a$.
      Para cada $n \in \naturals$,
      escolha $x_n \in B_{\frac{1}{n}}(a)$ tal que $f(x_n) \not \in B$.
      A sequência $x_n$ converge para $a$ (por quê?), mas
      $f(x_n)$ não converge para $f(a)$ (por que?).
    }
  \end{proof}



  \begin{obs}
    Repare como o item
    \refitem{it:prop:espaco_metrico:continuidade:bolas:toda_existe}
    se assemelha à definição de continuidade que utiliza argumentos
    do tipo $\varepsilon-\delta$:

    \begin{quote}
      Para todo $\varepsilon > 0$, existe $\delta > 0$ tal que
      \begin{equation*}
        d(x, a) < \delta \Rightarrow d(f(x), f(a)) < \varepsilon.
      \end{equation*}
    \end{quote}
  \end{obs}


  \begin{obs}
    \label{obs:base_local_enumeravel_e_continuidade}

    Para mostrar que a negação do item
    \refitem{it:prop:espaco_metrico:continuidade:bolas:inversa_eh_vizinhanca}
    implica na não continuidade de $f$, construímos uma
    sequência $x_n \rightarrow a$ tal que $f(x_n) \not \rightarrow
    f(x)$. Para isso, utilizamos as bolas $B_{\frac{1}{n}}(a)$ e a
    Proposição \ref{prop:espaco_metrico_tem_base_local_enumeravel}.
  \end{obs}


%TODO: Exercícios.
%Abertos e fechados usando bolas e sequencias.

  {\input{1/02/exercicios_03}}
