\section{Convergência}

  Em um espaço topológico $X$, se $F \subset X$ é fechado, e $x \not
  \in F$, então nenhuma sequência $x_n \in F$ pode convergir para
  $x$. De fato, $\complementset{F}$ é uma vizinhança de $x$ que não
  contém nenhum ponto da sequência $x_n$. Portanto, se uma sequência
  $x_n \in F$ converge para $x$, teremos que $x \in F$. Em particular,
  esquecendo um pouco o conjunto $F$, se $x_n \rightarrow x$, então,
  \begin{equation*}
    x \in \closure{\setsuchthat{x_n}{n \in \naturals}}.
  \end{equation*}
  Indo um pouco além,
  \begin{equation*}
    x \in \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}}.
  \end{equation*}
  Pois a sequência $x_{N+n}$ também converge para $x$.

  Por outro lado, dada a sequência $x_n \in X$, suponha que
  \begin{equation*}
    x \in \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}}.
  \end{equation*}
  Podemos concluir que $x_n \rightarrow x$? A resposta é não! Mas por
  que não? Por exemplo, por que $x_n = (-1)^n$ não converge? E se
  \begin{equation*}
    \{x\} = \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}},
  \end{equation*}
  então vale que $x_n \rightarrow x$? Considere
  \begin{equation*}
    x_n = \eqsystem
          {
            0, &\text{$n$ é impar} \\
            n, &\text{$n$ é par}
          }
  \end{equation*}
  para verificar que não vale. Mas é verdade que se $x_n \rightarrow
  x$, então
  \begin{equation*}
    \{x\} = \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}}?
  \end{equation*}
  Para ver que não, basta considerar a topologia $\{ \emptyset, X\}$,
  onde $X$ é um conjunto qualquer com mais de um elemento. Neste caso,
  \begin{equation*}
    X = \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}},
  \end{equation*}
  pois o fecho de qualquer conjunto não vazio é igual a $X$.

  Vamos supor, então, que
  \begin{equation*}
    x \in \bigcap_{N \in \naturals} \closure{\setsuchthat{x_n}{n \geq N}}.
  \end{equation*}
  Neste caso, quando é que $x_n \not \rightarrow x$? Se
  \begin{equation*}
    x_n \not \rightarrow x,
  \end{equation*}
  então existe uma vizinhança de $x$, $V$, tal que infinitos
  $x_{n_1}, x_{n_2}, x_{n_3}, \dotsc$ não pertencem a $V$. Ou seja,
  \begin{equation*}
    x \not \in \closure{\setsuchthat{x_{n_k}}{k \in \naturals}}.
  \end{equation*}
  É como dizer que $x_n \rightarrow x$ quando para toda
  ``subsequência'' $x_{n_k}$ tivermos que $x_{n_k} \rightarrow x$.


  \begin{definition}
    \label{def:subsequencia}

    Dado um conjunto $X$ e uma sequência $x_n \in X$. Uma
    \emph{sub-sequência} de $x_n$ é simplesmente uma sequência
    $y_k = x_{n_k}$, onde $n_1 < n_2 < n_3 < \dotsb$.
  \end{definition}

  \begin{obs}
    \label{obs:subsequencia}

    Dada uma sequência $x_n \in X$, o que determina as subsequências
    de $x_n$, são as aplicações
    \begin{equation*}
      \functionarray{f}{\naturals}{\naturals}{k}{n_k},
    \end{equation*}
    que preservam a ordem $\leq$ de $\naturals$. Ou seja,
    \begin{equation*}
      k_1 \leq k_2 \Leftrightarrow n_{k_1} \leq n_{k_2}.
    \end{equation*}
    Poderíamos ter definido \emph{subsequência} como uma aplicação
    $\function{f}{\naturals}{\naturals}$ que satisfaz
    \begin{equation*}
      k_1 < k_2 \Rightarrow f(k_1) < f(k_2).
    \end{equation*}
  \end{obs}


  \begin{proposition}
    \label{prop:convergencia_de_sequencia_com_fechados}

    Dado um espaço topológico $X$, uma sequência $x_n \in X$ converge
    para $x \in X$ se, e somente se, para toda subsequência $x_{n_k}$,
    \begin{equation*}
      x \in \closure{\setsuchthat{x_{n_k}}{k \in \naturals}}.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Se $x_n \rightarrow x$, então toda subsequência $x_{n_k}$ converge
    para $x$ (por quê?). Portanto,
    \begin{equation*}
      x \in \closure{\setsuchthat{x_{n_k}}{k \in \naturals}}.
    \end{equation*}
    Pois se
    $x \not \in \closure{\setsuchthat{x_{n_k}}{k \in \naturals}}$,
    então
    $\complementset{\closure{\setsuchthat{x_{n_k}}{k \in \naturals}}}$
    é uma vizinhança de $x$ que não contém nenhum $x_{n_k}$.

    Por outro lado, se $x_n \not \rightarrow x$, então existe uma
    vizinhança $V$ de $x$, tal que para infinitos índices $n_1, n_2,
    n_3, \dotsc$, $x_{n_k} \not \in V$. Portanto, para esses índices,
    \begin{equation*}
      x \not \in \closure{\setsuchthat{x_{n_k}}{k \in \naturals}}.
    \end{equation*}
  \end{proof}

  Uma das implicações da Proposição
  \ref{prop:convergencia_de_sequencia_com_fechados}, é que basta
  conhecer o fecho dos conjuntos enumeráveis para sabermos quais são e
  quais não são as sequências convergentes. Em espaços métricos, por
  exemplo, os conjuntos fechados $F$, são exatamente aqueles que
  \begin{equation*}
    x_n \in F,\, x_n \rightarrow x \Rightarrow x \in F.
  \end{equation*}
  Em espaços topológicos em geral, isso não é necessariamente válido.
  Mais adiante, veremos como o conceito de \emph{redes} pode
  remediar esta deficiência das sequências.
  Por exemplo, considere a topologia em $\reals$ do exercício
  \ref{exerc:reais_com_fechados_enumeraveis}, onde os fechados são,
  além do próprio $\reals$, os conjuntos enumeráveis (com
  cardinalidade menor ou igual à de $\naturals$). Neste caso, as
  sequências convergentes são constantes a menos de um número finito
  de índices:
  \begin{equation}
    \label{eq:sequencias_convergentes_da_topologia_discreta}
    x_n \rightarrow x
    \Leftrightarrow
    \exists N \in \naturals,\, \forall n \geq N,\, x_n = x.
  \end{equation}
  De fato, se houvessem infinitos índices $n_k$ tais que $x_{n_k} \neq
  x$, então, $\setsuchthat{x_{n_k}}{k \in \naturals}$ seria um fechado
  que não contém $x$, contradizendo a Proposição
  \ref{prop:convergencia_de_sequencia_com_fechados}. Por outro lado,
  as sequências de
  \refeq{eq:sequencias_convergentes_da_topologia_discreta}, são
  exatamente as sequências convergentes na topologia discreta.

  Essa mesma construção poderia ser feita com qualquer conjunto $X$ no
  lugar de $\reals$, para se ter uma topologia em $X$ onde as
  sequências convergentes são as mesmas da topologia
  discreta. Precisamos que $X$ seja não enumerável para que a
  topologia construída seja diferente de topologia discreta
  $\parts{X}$.
% TODO: Capítulo 0 -- notação.
% TODO: Apêndice -- enumerabilidade.
% TODO: Apêndice -- ordem.
% TODO: exercício -- cl_{seq} satisfaz as propriedades de ``fecho''.


  Em espaços métricos, uma aplicação $\function{f}{X}{Y}$ era contínua
  quando
  \begin{equation}
    \label{eq:sequencialmente_continuo}
    x_n \rightarrow x \Rightarrow f(x_n) \rightarrow f(x).
  \end{equation}
  Para o caso de espaços topológicos, a continuidade de $f$ implica na
  condição da equação \refeq{eq:sequencialmente_continuo}. No entanto,
  a volta nem sempre vale.

  \begin{proposition}
    \label{prop:continuo_eh_sequencialmente_continuo}

    Seja $\function{f}{X}{Y}$ uma aplicação entre espaços topológicos,
    contínua no ponto $x \in X$. Então,
  \begin{equation*}
    x_n \rightarrow x \Rightarrow f(x_n) \rightarrow f(x).
  \end{equation*}
  \end{proposition}

  \begin{proof}
    Se $f(x_n) \not \rightarrow f(x)$, então existe uma vizinhança
    aberta $A$ de $f(x)$, tal que para um número infinito de índices,
    a sequência $f(x_n)$ não pertence a $A$. Portanto, para um número
    infinito de índices, a sequência $x_n$ não pertence a $f^{-1}(A)$,
    que é, pela continuidade de $f$ em $x$, uma vizinhança de $x$. O
    que mostra que $x_n \not \rightarrow x$.
  \end{proof}

%% TODO: Sugerir investigação da topologia das sequencias convergentes
%% por conta do leitor.
%  \begin{furtherinvestigation}
%    \begin{definition}
%      \label{def:fecho_sequencial}

%      Dado um espaço topológico $\topologicalspace{X}$, o \emph{fecho sequêncial}
%      de $\topology{X}$ é a aplicação
%      \begin{equation*}
%        \functionarray{\sequentialclosureoperator{\topology{X}}}{\parts{X}}{\parts{X}}
%                      {A}{\setsuchthat{x \in X}{\exists x_n \in A,\, x_n \rightarrow x}}.
%      \end{equation*}
%    \end{definition}
%  \end{furtherinvestigation}
