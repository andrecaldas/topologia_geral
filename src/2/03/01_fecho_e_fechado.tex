\section{Fecho e Fechado}
  \label{sec:fecho_e_fechado}

  \begin{definition}
    \label{def:fecho}

    Seja $\topologicalspace{X}$ um espaço topológico e $B \subset X$ um
    subconjunto qualquer de $X$. Definimos o \emph{fecho de $B$}, e
    denotamos por $\closure{B}$ o conjunto
    \begin{equation*}
      \closure{B} = \setsuchthat{x \in X}{\forall V \in
                                 \neighbourhoods{x},\, V \cap B \neq \emptyset }.
    \end{equation*}
    Também escrevemos $\closureop{B}$; ou quando queremos enfatizar a
    topologia $\topology{X}$, escrevemos $\closureop[\topology{X}]{B}$.
  \end{definition}


  \begin{obs}
    O operador de fecho é uma aplicação
    \begin{equation*}
      \functionarray{\closureoperator}{\parts{X}}{\parts{X}}{B}{\closure{B}}.
    \end{equation*}
  \end{obs}


  \begin{lemma}
    \label{lemma:fecho_com_base_de_vizinhancas}
    \label{lemma:fecho_com_base}

    Seja $\topologicalspace{X}$ um espaço topológico. E para cada $x$, seja
    $\beta_x \subset \neighbourhoods{x}$ uma base de vizinhanças de
    $x$. Então,
    \begin{equation*}
      \closure{B} = \setsuchthat{x \in X}{\forall V \in
                                 \beta_x,\, V \cap B = \emptyset }.
    \end{equation*}
    Em particular, $x$ está no fecho de $B$ se, e somente se, para
    todo vizinhança aberta $U$ de $x$, $U \cap B = \emptyset$.
  \end{lemma}
% TODO: exercício -- não vale para uma sub-base!


  Vamos então verificar algumas propriedades do operador
  $\closureoperator_{\topology{X}}$.

  \begin{proposition}
    \label{prop:propriedades_do_fecho}

    A operação de fecho no espaço topológico $\topologicalspace{X}$ satisfaz:
    \begin{enumerate}
      \item
        \label{it:prop:propriedades_do_fecho:vazio_e_todo}
        $\closure{\emptyset} = \emptyset,\, \closure{X} = X$.

      \item
        \label{it:prop:propriedades_do_fecho:fecho_eh_maior}
        $B \subset \closure{B}$.

      \item
        \label{it:prop:propriedades_do_fecho:inclusao}
        $A \subset B \Rightarrow \closure{A} \subset \closure{B}$.

      \item
        \label{it:prop:propriedades_do_fecho:idempotencia}
        $\closure{\closure{B}} =
        \closure{B}$.\quad($\closureoperator_{\topology{X}}^2 = \closureoperator_{\topology{X}}$)

      \item
        \label{it:prop:propriedades_do_fecho:uniao_qualquer}
        Se $B_\lambda$ ($\lambda \in \Lambda$) é uma família qualquer
        de subconjuntos de $X$, então
        \begin{equation*}
          \bigcup_{\lambda \in \Lambda} \closure{B_\lambda} \subset
          \closure{\bigcup_{\lambda \in \Lambda} B_\lambda}.
        \end{equation*}

      \item
        \label{it:prop:propriedades_do_fecho:uniao_finita}
        Se $A$ $B$ são subconjuntos de $X$, então
        \begin{equation*}
          \closure{A} \cup \closure{B} = \closure{A \cup B}.
        \end{equation*}
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      Itens
      \refitem{it:prop:propriedades_do_fecho:vazio_e_todo},
      \refitem{it:prop:propriedades_do_fecho:fecho_eh_maior} e
      \refitem{it:prop:propriedades_do_fecho:inclusao}.
    }

    Consequências imediatas da definição de fecho.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_do_fecho:idempotencia}.
    }

    Por \refitem{it:prop:propriedades_do_fecho:fecho_eh_maior},
    $\closure{B} \subset \closure{\closure{B}}$. Seja então
    $x \in \closure{\closure{B}}$, e seja $U \in \topology{X}$ uma vizinhança
    aberta de $x$. Então existe $y \in \closure{B}$, tal que
    $y \in U$. Ou seja, $U$ é vizinhança de $y$. E portanto, como $y$
    está no fecho de $B$, existe $z \in B$ tal que $z \in U$.
    Provamos que toda vizinhança aberta de $x$ intercepta $B$. Pelo
    lema \ref{lemma:fecho_com_base}, $x \in \closure{B}$.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_do_fecho:uniao_qualquer}.
    }

    O item
    \refitem{it:prop:propriedades_do_fecho:uniao_qualquer} é imediato
    da definição de fecho. No entanto, também pode ser demonstrado
    através do item
    \refitem{it:prop:propriedades_do_fecho:inclusao}, pois para todo
    $\gamma \in \Lambda$,
    \begin{equation*}
      B_\gamma \subset \bigcup_{\lambda \in \Lambda} B_\lambda
      \Rightarrow
      \closure{B_\gamma} \subset \closure{\bigcup_{\lambda \in \Lambda} B_\lambda}.
    \end{equation*}
    Basta então fazer a união para todo $\gamma \in \Lambda$.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_do_fecho:uniao_finita}.
    }

    Por
    \refitem{it:prop:propriedades_do_fecho:uniao_qualquer}, basta
    mostrar que
    \begin{equation*}
      \closure{A \cup B} \subset \closure{A} \cup \closure{B}.
    \end{equation*}
    Suponha que $x \in \closure{A \cup B}$, mas
    $x \not \in \closure{A}$. Vamos mostrar que
    $x \in \closure{B}$. Existe uma vizinhança $V$ de $x$ tal que
    $V \cap A = \emptyset$. Toda vizinhança de $x$ intercepta
    $A \cap B$. Então, toda vizinhança de $x$ contida em $V$, já que
    não intercepta $A$, tem que interceptar $B$. Observando que a
    família de todas as vizinhanças de $x$ contidas em $V$ forma uma
    base de vizinhanças de $x$ (por quê?), concluímos pelo lema
    \ref{lemma:fecho_com_base_de_vizinhancas} que
    $x \in \closure{B}$.
% TODO: exercício -- V \cap \neighbourhoods{x} é base de vizinhanças
% de $x$.
% TODO: exercício -- fazer sem usar diretamente o lema.
% TODO: exercício -- A \subset \closure{B} IFF U \cap A \Rightarrrow U
% \cap B.
% TODO: exercício -- Mostrar idempotência utilizando o exercício
% anterior. (\closure{\closure{B}} \subset \closure{B})
% TODO: exercício -- mostrar o item 5 direto da definição.
  \end{proof}


  \begin{obs}
    A demonstração do item
    \refitem{it:prop:propriedades_do_fecho:idempotencia}
    da Proposição \ref{prop:propriedades_do_fecho} (de fato, o lema
    \ref{lemma:fecho_com_base_de_vizinhancas} utilizou, de maneira
    essencial, o fato de a família das vizinhanças abertas de um ponto
    formar uma base de vizinhanças. Veja a Observação
    \ref{obs:espaco_metrico:fecho_idempotente}, o texto introdutório
    da seção \ref{obs:espaco_metrico:fecho_idempotente} e
    a Figura \ref{fig:distant_set_distant_closure}.
  \end{obs}



  \begin{definition}
    \label{def:fechado}

    Dado um espaço topológico $\topologicalspace{X}$, um conjunto $F \subset X$ é
    \emph{fechado} quando $\closure{F} = F$.
  \end{definition}

  Pela definição de fechado, os conjuntos fechados são os pontos fixos
  da aplicação $\closureoperator_{\topology{X}}$. Por outro lado, o item
  \refitem{it:prop:propriedades_do_fecho:idempotencia} da Proposição
  \refitem{prop:propriedades_do_fecho} mostra que todo conjunto da forma
  $\closure{B}$ é fechado. Ou seja, a família dos conjuntos fechados é
  exatamente a imagem de $\closureoperator_{\topology{X}}$. Se o ``fecho'' de um
  conjunto não fosse ``fechado'', precisaríamos dar outro nome para ao
  menos um dos dois conceitos. :-)


  \begin{proposition}
    \label{prop:complemento_de_fechado_eh_aberto}

    Em um espaço topológico $\topologicalspace{X}$, $F \subset X$ é fechado se, e
    somente se, $\complementset{F} \in \topology{X}$.
  \end{proposition}

  \begin{proof}
    Tome $x \in \complementset{F}$. Então existe $V \in
    \neighbourhoods{x}$ tal que $V \cap F = \emptyset$. Ou seja,
    $V \subset \complementset{F}$. Portanto, $\complementset{F}$ é
    aberto, já que é vizinhança de todos os seus pontos.

    Por outro lado, suponha que $\complementset{F} \in \topology{X}$. Então,
    nenhum ponto de $\complementset{F}$ pertence a $\closure{F}$ (por
    quê?). Ou seja,
    \begin{equation*}
      \closure{F} \subset F.
    \end{equation*}
    Portanto, $\closure{F} = F$ (por quê?).
  \end{proof}


  \begin{obs}
    A Proposição \ref{prop:complemento_de_fechado_eh_aberto} mostra
    que a topologia de $X$ pode ser determinada pela família
    \begin{equation*}
      \family{F} = \setsuchthat{F \subset X}{\closure{F} = F},
    \end{equation*}
    dos subconjuntos fechados de $X$. Quando é então que uma família
    $\family{F} \subset \parts{X}$ define os conjuntos fechados de uma
    topologia $\topology{X}$? Ou seja, quando é que a família
    \begin{equation*}
      \topology{\family{F}} = \setsuchthat{A \subset X}{\complementset{A} \in \family{F}}
    \end{equation*}
    é uma topologia de $X$? A resposta é simples: a família
    $\family{F}$ terá que satisfazer as condições listadas na
    Proposição \ref{prop:topologia_com_fechados}.

    Indo um pouco além, se conhecermos $\closureoperator_{\topology{X}}$, também
    sabemos quem são os fechados de $\topology{X}$, e por consequência,
    sabemos quem é a topologia $\topology{X}$. Desta forma, quando é então que
    uma aplicação
    \begin{equation*}
      \function{c}{\parts{X}}{\parts{X}}
    \end{equation*}
    é igual à operação de fecho $\closureoperator_{\topology{X}}$ de uma
    topologia $\topology{X}$? A resposta a esta pergunta está contida na
    Proposição \ref{prop:propriedades_do_fecho}.
    Veja o
    Exercício \ref{exerc:topologia_com_fecho}.
  \end{obs}


  \begin{proposition}
    \label{prop:topologia_com_fechados}

    Dado um espaço topológico $\topologicalspace{X}$, a família $\family{F}$
    formada pelos subconjuntos fechados de $X$ satisfaz:
    \begin{enumerate}
      \item
        $\emptyset, X \in \family{F}$.

      \item
        $F_1, F_2 \in \family{F} \Rightarrow F_1 \cup F_2 \in
        \family{F}$.

      \item
        $F_\lambda \in \family{F}\, (\lambda \in \Lambda) \Rightarrow
        \bigcap_{\lambda \in \Lambda} F_\lambda \in \family{F}$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Basta utilizar as leis de De Morgan
    \begin{align*}
% TODO: Fazer o ``parentesis'' ser automático para \complementset...
      \bigcup_{\lambda \in \Lambda} \complementset{A_\lambda} &=
      \complementset{\left(\bigcap_{\lambda \in \Lambda} A_\lambda\right)} \\
      \bigcap_{\lambda \in \Lambda} \complementset{A_\lambda} &=
      \complementset{\left(\bigcup_{\lambda \in \Lambda} A_\lambda\right)}
    \end{align*}
    para verificar a equivalência entre os itens da proposição e os
    itens da definição de topologia \ref{def:topologia}.
  \end{proof}


  O fecho de um conjunto $B$ pode ser facilmente determinado se
  utilizarmos a família $\family{F}$ dos fechados.

  \begin{proposition}
    \label{prop:fecho_com_fechados}

    Seja $X$ um espaço topológico e $\family{F}$ a família formada
    pelos subconjuntos fechados de $X$. Então, para um subconjunto
    qualquer $B \subset X$,
    \begin{equation*}
      \closure{B} = \bigcap_{\substack{B \subset F \\ \text{$F$ é fechado}}} F.
    \end{equation*}
    Em outras palavras, $\closure{B}$ é o menor conjunto fechado que
    contém $B$.
  \end{proposition}

  \begin{proof}
    Como $\closure{B}$ é fechado e $B \subset \closure{B}$, temos que
    \begin{equation*}
      \bigcap_{B \subset F \in \family{F}} F \subset \closure{B}.
    \end{equation*}

    \subproof
    {
      Se $F$ é fechado, então
      \begin{equation*}
              B \subset F \Rightarrow \closure{B} \subset F.
      \end{equation*}
    }
    {
      De fato,
      \begin{equation*}
        B \subset F \Rightarrow \closure{B} \subset \closure{F} = F.
      \end{equation*}
    }

    Pela afirmação anterior, é evidente que
    \begin{equation*}
      \closure{B} \subset \bigcap_{B \subset F \in \family{F}} F.
    \end{equation*}
  \end{proof}
