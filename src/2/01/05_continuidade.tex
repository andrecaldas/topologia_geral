\section{Continuidade}

  Continuidade é um conceito central em topologia. Uma aplicação
  contínua transporta aspectos topológicos entre os espaços em
  questão. Dada uma aplicação $\function{f}{X}{Y}$ entre os conjuntos
  $X$ e $Y$, podemos ver $f^{-1}$ como uma aplicação
  \begin{equation*}
    \function{f^{-1}}{\parts{Y}}{\parts{X}}.
  \end{equation*}
  Se $\topologicalspace{X}$ e $\topologicalspace{Y}$ são espaços topológicos e $f$ é
  contínua em $x \in X$, podemos olhar para $f^{-1}$ restrita a
  $\neighbourhoods{f(x)}$ como sendo uma aplicação
  \begin{equation*}
    \function{f^{-1}}{\neighbourhoods{f(x)}}{\neighbourhoods{x}}.
  \end{equation*}
  A proposição a seguir demonstra que quando $f$ é contínua em todo
  ponto de $X$, então a restrição de $f^{-1}$ a $\topology{Y}$ pode ser
  vista como uma aplicação
  \begin{equation*}
    \function{f^{-1}}{\topology{Y}}{\topology{X}}.
  \end{equation*}

  \begin{proposition}
    \label{prop:continuidade_e_abertos}

    Sejam $\topologicalspace{X}$ e $\topologicalspace{Y}$ espaços topológicos e
    $\function{f}{X}{Y}$ uma aplicação de $X$ em $Y$. Neste caso, são
    equivalentes:
    \begin{enumerate}
      \item
        \label{prop:continuidade_e_abertos:continua_em_todo_ponto}
        A função $f$ é contínua em todo ponto $x \in X$.

      \item
        \label{prop:continuidade_e_abertos:continua_com_abertos}
        Para todo aberto $A \in \topology{Y}$, $f^{-1}(A) \in \topology{X}$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Para todo ponto $x \in f^{-1}(A)$, $f(x) \in A$.
    Então, dado $A \in \topology{Y}$,
    temos que para todo ponto $x \in f^{-1}(A)$,
    como $A$ é aberto, $A \in \neighbourhoods{f(x)}$.
    Pela continuidade de $f$ no ponto $x$,
    temos que $f^{-1}(A) \in \neighbourhoods{x}$.
    Acabamos de mostrar que $f^{-1}(A)$ é vizinhança de todos os seus pontos,
    e portanto,
    pela Proposição \ref{prop:abertos_sao_vizinhancas_de_seus_pontos},
    $f^{-1}(A)$ é um aberto de $X$.

    Por outro lado,
    para $x \in X$ qualquer,
    denotando por $\family{B}_{f(x)}$
    a família dos abertos que contém $f(x)$,
    como a imagem inversa desses abertos contém $x$,
    temos que
    \begin{equation*}
      f^{-1}
      \left(
        \family{B}_{f(x)}
      \right)
      \subset
      \neighbourhoods{x}.
    \end{equation*}
    E como $\family{B}_{f(x)}$ é uma base de $\neighbourhoods{f(x)}$,
    temos que
    \begin{equation*}
      f^{-1}
      \left(
        \neighbourhoods{f(x)}
      \right)
      \subset
      \neighbourhoods{x}.
    \end{equation*}
    Ou seja, $f$ é contínua em $x$.
  \end{proof}


  \begin{definition}[Função Contínua]
    Dizemos que uma função $\function{f}{X}{Y}$ entre os espaços
    topológicos $\topologicalspace{X}$ e $\topologicalspace{Y}$
    é contínua quando é contínua em todo ponto $x \in X$.
    Ou, equivalentemente, quando
    $f^{-1}(\topology{Y}) \subset \topology{X}$.
  \end{definition}

%TODO: exercício.
%Composição de contínuas é contínua.



  \subsection{Homeomorfismos}

    Para dois conjuntos $X$ e $Y$, uma bijeção $\function{f}{X}{Y}$
    identifica cada ponto de $X$ a um único ponto de $Y$ e
    vice-versa. Se $X$ e $Y$ forem espaços topológicos, $f$ for
    contínua e sua inversa $\function{f^{-1}}{Y}{X}$ também for
    contínua, então também serão identificados cada aberto de $X$ com
    um único aberto de $Y$ e vice-versa. Tudo o que puder ser dito
    sobre a topologia de $X$ poderá ser afirmado sobre a topologia de
    $Y$ através da identificação dada por $f$.

    \begin{definition}[Homeomorfismo]
      \label{def:homeomorfismo}

      Sejam $X$ e $Y$ espaços topológicos. Dizemos que uma aplicação
      \begin{equation*}
        \function{f}{X}{Y}
      \end{equation*}
      é um homeomorfismo de $X$ em $Y$ quando $f$ for bijetiva,
      contínua e sua inversa $f^{-1}$ também for contínua.

      Quando existe um homeomorfismo entre dois espaços topológicos,
      dizemos que estes espaços são homeomorfos.
    \end{definition}

%TODO: exercício.
%Homeomorfo é relação de equivalência.



  \subsection{Aplicação Aberta}

    Com uma aplicação $\function{f}{X}{Y}$ entre espaços topológicos,
    podemos tentar relacionar as topologias de $\topologicalspace{X}$ e $(Y,
    \topology{Y})$. Se $f$ for um homeomorfismo, sabemos que $X$ e $Y$
    possuem exatamente a mesma topologia quando os pontos de $X$ são
    identificados com os de $Y$ através de $f$. Se $f$ for uma bijeção
    contínua, podemos identificar cada elemento de $X$ com um único
    elemento de $Y$.
    Com esta identificação,
    teremos que
    $\topology{Y} \subset \topology{X}$.
    Uma outra propriedade que $f$ pode ter, que ajudaria a
    relacionar os espaços $X$ e $Y$ é a de levar abertos de $X$ em
    abertos de $Y$.
    Neste caso, dizemos que $f$ é uma
    \emph{aplicação aberta}.

    \begin{definition}
      \label{def:aplicacao_aberta}

      Seja $\function{f}{\topologicalspace{X}}{\topologicalspace{Y}}$ uma aplicação
      entre os espaços topológicos $X$ e $Y$. Dizemos que $f$ é uma
      \emph{aplicação aberta} quando $f(\topology{X}) \subset \topology{Y}$.
    \end{definition}

    Um homeomorfismo é uma bijeção contínua e aberta. Nossa motivação
    para a definição de \emph{aplicação aberta} é simplesmente
    imaginar, ignorando o fato de que $f$ pode nem mesmo ser bijetiva,
    o que seria necessário para que $f^{-1}$ seja contínua. Mais
    adiante, veremos maneiras de se transportar topologias de um
    espaço topológico a um conjunto qualquer através de aplicações
    entre eles. Quando temos uma bijeção entre um espaço topológico e
    um conjunto qualquer, fica fácil transportar a topologia de um
    para o outro.
    Imagine que $\function{f}{X}{Y}$, não uma bijeção, mas
    uma sobrejeção do espaço topológico $X$ no espaço topológico $Y$.
    Podemos definir o conjunto
    \begin{equation*}
      \tilde{X} = \setsuchthat{f^{-1}(y)}{y \in Y},
    \end{equation*}
    que nada mais é do que ``agrupar'' todos os elementos de $X$ que
    têm a mesma imagem. A projeção natural de $X$ em $\tilde{X}$ é
    dada por
    \begin{equation*}
      \functionarray{\pi}{X}{\tilde{X}}{x}{f^{-1}(f(x))}.
    \end{equation*}
    A projeção leva um elemento $x \in X$ na ``classe'' formada por
    todos os elementos de $X$ que tem, por $f$, a mesma imagem que
    $x$. A aplicação $f$ pode ser fatorada, de modo que o seguinte
    diagrama é comutativo (ou seja, $f = \tilde{f} \circ \pi$):
    \begin{equation*}
      \xymatrix{
        X           \ar[d]_{\pi} \ar[r]^f    &Y \\
        \tilde{X}   \ar[ur]_{\tilde{f}}
      }.
    \end{equation*}
    Basta definir $\tilde{f}(f^{-1}(y)) = y$. Agora, $\tilde{f}$ é
    uma bijeção.
    Faz sentido esperar que a topologia de $X$ possa
    ser transportada para $\tilde{X}$,
    de modo que as propriedades topológicas de $f$ possam ser
    investigadas em função de $\tilde{f}$ e vice-versa.
    Em particular, se $f$ for contínua e aberta,
    podemos esperar que $\tilde{f}$ seja um homeomorfismo
    entre $Y$ e $\tilde{X}$
    (Veja o exercício \ref{ex:aberta_e_fechada:quociente_eh_homeomorfismo}).
    Trataremos desse tipo de topologia,
    a topologia quociente,
    no Capítulo \ref{ch:topologias_induzidas}.

    Assim como podemos falar de \emph{continuidade em um ponto},
    podemos também definir o que seria dizer que $\function{f}{X}{Y}$
    é aberta em $x \in X$. Assim como no caso de continuidade, a
    definição fica melhor se usarmos \emph{vizinhanças de $x$} ao
    invés de abertos.

    \begin{definition}
      \label{def:aplicacao_aberta_em_um_ponto}

      Seja $\function{f}{\topologicalspace{X}}{\topologicalspace{Y}}$ uma aplicação
      entre os espaços topológicos $X$ e $Y$. Dizemos que $f$ é uma
      \emph{aplicação aberta em $x \in X$} quando
      $f(\neighbourhoods{x}) \subset \neighbourhoods{f(x)}$.
    \end{definition}
