\section{Vizinhanças e Base de Vizinhanças de um Ponto}

  Assim como fizemos para espaços métricos, podemos definir para um
  espaço topológico $\topologicalspace{X}$, o que é para cada ponto $x \in X$, a
  família de todas as suas vizinhanças.

  \begin{definition}
    \label{def:vizinhanca}

    Seja $\topologicalspace{X}$ um espaço topológico. Dado $x \in X$, uma
    \emph{vizinhança aberta} de $x$ é um aberto $A \in \topology{X}$ que
    contém o ponto $x$. Uma \emph{vizinhança} de $x$ é qualquer
    conjunto que contenha uma vizinhança aberta de $x$.  Denotamos por
    $\neighbourhoods{x}$ a família de todas as vizinhanças de $x$.
  \end{definition}

  \begin{obs}
    Alguns autores usam o termo
    \emph{vizinhança} para designar apenas as
    vizinhanças abertas.
    Provavelmente, a causa disso, é a sobrevalorização
    dos conjuntos abertos.
    Em muitos casos, onde seria melhor considerar
    vizinhanças, muitos matemáticos insistem em enxergar
    apenas os conjuntos abertos.
    Neste livro, se quisermos uma vizinhança aberta de $x$,
    diremos ``vizinhança aberta de $x$'',
    ou simplesmente,
    ``um aberto que contém $x$''.
    Caso contrário, diremos apenas
    \emph{vizinhança} para o que outros autores chamariam de
    ``um conjunto que contém uma vizinhança aberta de $x$''.
  \end{obs}


  Em um espaço topológico qualquer, as vizinhanças de um ponto possuem
  as mesmas propriedades para o caso de espaços métricos
  que as descritas na Proposição
  \ref{prop:espaco_metrico:propriedades_vizinhanca}.

  \begin{proposition}
    \label{prop:propriedades_vizinhanca}

    Seja $X$ um espaço topológico, e $x \in X$ um ponto de $X$. Então
    valem as seguintes afirmações sobre a família $\neighbourhoods{x}$
    de todas as vizinhanças de $x$:
    \begin{enumerate}
      \item
        \label{it:prop:propriedades_vizinhanca:superset}
        Se $A \in \neighbourhoods{x}$ e $A \subset B$,
        então $B \in \neighbourhoods{x}$.

      \item
        \label{it:prop:propriedades_vizinhanca:intersecao}
        A interseção de duas vizinhanças de $x$ também é uma vizinhança de $x$.
        Ou seja, se $A, B \in \neighbourhoods{x}$,
        então $A \cap B \in \neighbourhoods{x}$.

      \item
        \label{it:prop:propriedades_vizinhanca:interior}
        Se $A \in \neighbourhoods{x}$ então existe $B \subset A$ tal
        que $x \in B$, e $B$ é vizinhança de todos os seus pontos.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Todos os itens são evidentes da definição de vizinhança. O item
    \refitem{it:prop:propriedades_vizinhanca:intersecao} é
    consequência do fato de $\topology{X}$ ser fechado por interseção finita.
  \end{proof}

  Assim como no caso de espaços métricos, podemos caracterizar os
  conjuntos abertos como sendo aqueles que são vizinhanças de todos os
  seus pontos.

  \begin{proposition}
    \label{prop:abertos_sao_vizinhancas_de_seus_pontos}

    Dado um espaço topológico $X$, um conjunto $A \subset X$ é aberto
    se, e somente se, for vizinhança de todos os seus pontos.
  \end{proposition}

  \begin{proof}
    Pela definição de vizinhança, um conjunto aberto é evidentemente
    vizinhança de todos os seus pontos. Suponha então que $A \subset
    X$ é vizinhança de todos os seus pontos. Vamos mostrar que $A$ é
    aberto.

    Por hipótese, para cada $a \in A$ existe um aberto $U_a$ tal que
    $a \in U_a \subset A$. Neste caso,
    \begin{equation*}
      A = \bigcup_{a \in A} U_a.
    \end{equation*}
    Como $A$ é uma união de abertos $U_a$, temos que $A$ também é
    aberto.
  \end{proof}



  \begin{definition}
    \label{def:base_de_vizinhancas}

    Seja $\topologicalspace{X}$ um espaço topológico e $x \in X$ um ponto qualquer de
    $x$. Uma família formada por vizinhanças de $x$, $\family{B} \subset
    \neighbourhoods{x}$, é chamada de \emph{base de vizinhanças} de $x$
    quando para toda vizinhança $V \in \neighbourhoods{x}$ existir $B
    \in \family{B}$ tal que $B \subset V$. Se todos os conjuntos de
    $\family{B}$ forem abertos, ou seja, se $\family{B} \subset \topology{X}$,
    então diremos que $\family{B}$ é uma
    \emph{base de vizinhanças abertas} de $x$.
  \end{definition}


  \begin{obs}
    Alguns autores dizem
    \emph{base local} ao invés de
    \emph{base de vizinhanças}.
  \end{obs}

  Agora que, mesmo sem uma métrica, definimos o que vem a ser uma
  vizinhança de um ponto, podemos definir convergência de
  sequências. As sequências não serão tão importantes para a teoria
  geral. No entanto, motivarão a definição de \emph{redes}; um
  conceito que será trabalhado no
  Capítulo \ref{ch:trocentas_maneiras_de_descrever_uma_topologia}.

  \begin{definition}
    \label{def:topologia_geral:convergencia_de_sequencia}

    Seja $\topologicalspace{X}$ um espaço topológico e $x_n \in X$ uma sequência de
    elementos de $X$. Dizemos que $x_n$ \emph{converge} para $x \in
    X$ na topologia $\topology{X}$, quando para toda vizinhança $V \in
    \neighbourhoods{x}$ existir $N = N(V) \in \naturals$ tal que
    \begin{equation*}
      n \geq N \Rightarrow x_n \in V.
    \end{equation*}
    De maneira semelhante ao caso dos espaços métricos, denotamos tal
    fato por $x_n \xrightarrow{\topology{X}} x$, ou simplesmente $x_n
    \rightarrow x$.
  \end{definition}

  \begin{obs}
    \label{obs:convergencia_com_base_de_vizinhanca}
    Novamente, como no caso métrico,
    para saber se uma sequência $x_n \in X$
    converge para $x \in X$, basta verificar a condição da
    Definição \ref{def:topologia_geral:convergencia_de_sequencia}
    para uma base de vizinhanças de $x$.
    Em particular,
    assim como no caso dos espaços métricos,
    dados $x_n, x \in X$, teremos que
    $x_n \xrightarrow{\topology{X}} x$
    se, e somente se,
    \begin{quote}
      para toda vizinhança aberta $A$ de $x$
      existe $N = N(A) \in \naturals$ tal que
      \begin{equation*}
        n \geq N \Rightarrow x_n \in A.
      \end{equation*}
    \end{quote}
    Isso porque a família das vizinhanças abertas de $x$
    formam uma base para $\neighbourhoods{x}$.
  \end{obs}
%TODO: Exercício.
% Na topologia da convergência inferior, o que significa x_n
% \rightarrow x? Pode acontecer de $x_n \rightarrow y \neq x$?
