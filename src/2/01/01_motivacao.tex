\section{Motivação}
  \label{sec:topologia_geral_motivacao}

  Esta seção não é formal. Nosso propósito aqui é apenas dar motivação
  para as definições e propriedades que serão estudadas nas seções
  seguintes. Se o leitor não se sentir confortável com as divagações
  desta seção, pode omití-la sem problemas.

  Nossa motivação é obviamente o estudo que acabamos de fazer de
  \emph{espaços métricos}. Devidamente motivados pelo estudo feito
  na primeira parte do livro, vamos abstrair o que seria a
  essência dos fenômenos de \emph{convergência} e
  \emph{continuidade}. Uma alternativa seria associar ao espaço $X$ em
  questão uma estrutura que identificasse, para cada um dos pontos $x
  \in X$, quais são e quais não são as sequências que convergem para
  $x$. Uma deficiência desta abordagem está na dependência para com o
  conjunto dos números naturais, que indexam as tais
  sequências. Futuramente, veremos que uma solução alternativa é o uso
  de \emph{redes} em substituição ao de sequência.
  Esta abordagem será feita
  no Capítulo \ref{ch:topologia_com_redes}.

  Outra maneira seria associar a $X$ uma estrutura que indicasse,
  quais são os conjuntos que formam as ``vizinhanças''
  de cada ponto de $X$.
  A família das vizinhanças de $x \in X$,
  denotadas por $\neighbourhoods{x}$,
  indica do que é que $x$ está ``próximo'' e do que é que está ``afastado''.
  O ponto $x$ está próximo de um conjunto $B \in X$ quando
  para todo $V \in \neighbourhoods{x}$,
  tivermos $B \cap V \neq \emptyset$.
  A palavra ``próximo'' está entre
  aspas porque esse não é o termo matemático utilizado.
  Dizemos que $x$ está no \emph{fecho} de $B$
  (Definição \ref{def:fecho}), ou que é um ponto
  de \emph{aderência} de $B$.

%% TODO: exercício.
%% Diremos que $x_n \in X$ converge para $x$ quando para
%% todo $\Gamma \subset \naturals$ com $\cardinality{\Gamma} = \infty$,
%% os conjuntos
%% \begin{equation*}
%%   B_\Gamma = \setsuchthat{x_n}{n \in \Gamma}
%% \end{equation*}
%% estiverem ``próximos'' de $x$.
%% (Porque não utilizamos $n
%% \geq N$ para todo $N \in \naturals$ ao invés de $n \in \Gamma$?)


  As famílias $\neighbourhoods{x}$
  deveriam satisfazer as propriedades listadas na Proposição
  \ref{prop:espaco_metrico:propriedades_vizinhanca}. O item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:superset}
  não gera grandes polêmicas. Para que um conjunto esteja próximo do
  ponto, tem que interceptar todas as suas vizinhanças, portanto,
  acrescentar conjuntos maiores não modificaria a ``convergência''.
  Talvez o nome ``vizinhança'' não seja realmente uma
  boa escolha, já que sugere que sejam conjuntos pequenos.
  Mas ao contrário disso, as vizinhanças são conjuntos
  que são grandes o suficiente para conter uma bola,
  no caso dos espaços métricos.
  Quando dizemos
  \begin{quote}
    [\ldots]
    para todas as vizinhanças
    [\ldots],
  \end{quote}
  geralmente estamos omitindo uma referência a uma expressão do
  tipo ``por menores que sejam''.
  Em uma conversa, se quiséssemos enfatizar, diríamos
  \begin{quote}
    [\ldots]
    para todas as vizinhanças,
    \textbf{por menores que sejam}
    [\ldots]
  \end{quote}
  Esse tipo de argumento poderia ser restrito às vizinhanças
  ``pequenas'', e é por isso que existe a noção de
  \emph{base de vizinhanças de um ponto}
  (Veja a Definição \ref{def:espaco_metrico:base_de_vizinhancas}
  e a Proposição \ref{prop:espaco_metrico:convergencia:base_de_vizinhancas}).
  Assim, o item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:superset}
  se presta mesmo a maximizar a família $\neighbourhoods{x}$ de modo
  que o fenômeno de convergência não seja alterado.

  Do ponto de vista do fenômeno de convergência, o item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:intersecao}
  também serve ao mesmo propósito de maximizar a família
  $\neighbourhoods{x}$. Isso porque, se para $U,V \in
  \neighbourhoods{x}$, existem $N_U, N_V \in \naturals$ tais que
  \begin{align*}
    n \geq N_U &\Rightarrow x_n \in U \\
    n \geq N_V &\Rightarrow x_n \in V,
  \end{align*}
  então, fazendo $N_{U \cap V} = \max \{ N_U, N_V \}$,
  \begin{equation*}
    n \geq N_{U \cap V} \Rightarrow x_n \in U \cap V.
  \end{equation*}
  Assim, se $U$ e $V$ são vizinhanças de $x$, levar ou não em
  consideração o conjunto $U \cap V$ como sendo vizinhança de $x$ não
  afetaria o fenômeno de convergência.
  O que não é convergente continua não convergente,
  e o que é convergente permanece convergente.
  Já do ponto de vista da idéia de ``proximidade'',
  a condição do item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:intersecao}
  garante que se dois conjuntos $A$ e $B$ estão longe de $x$, então a
  união $A \cup B$ também está longe de $x$.
  Veja a Figura \ref{fig:distant_sets_union}.
  Em espaços métricos, todos os pontos
  estão distantes uns dos outros. Assim, nenhum conjunto finito está
  ``próximo'' de $x$, a menos que o contenha. Por outro lado, uma
  sequência \emph{infinita} de pontos $x_n$ distintos de $x$ pode
  convergir para $x$. No caso de espaços topológicos gerais, é
  possível que uma família infinita de conjuntos $A_n$ ``afastados''
  de $x$ seja tal que $\bigcup A_n$ esteja ``próximo'' de $x$, mas as
  uniões finitas estarão sempre ``afastados'' de $x$.

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/distant_sets_union}
    \end{center}
    \caption{
      O conjunto $A$ está ``afastado'' de $x$ por não interceptar a
      vizinhança $V$. Da mesma forma, $B$ também está ``afastado''de
      $x$. Então, a união $A \cup B$ também está ``afastada'' de $x$,
      pois $U \cap V$ é vizinhança de $x$.
    }

    \label{fig:distant_sets_union}
  \end{figure}


  O item mais difícil de aceitar
  da Proposição \ref{prop:espaco_metrico:propriedades_vizinhanca},
  é o item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}.
  Como já mencionamos anteriormente
  (Observação \ref{obs:espaco_metrico:fecho_idempotente}),
  este item serve para garantir que se $x^n_m \rightarrow x_n$
  e para alguma vizinhança $V \in \neighbourhoods{x}$
  for verdade que $x^n_m \not \in V$,
  então não é possível acontecer
  que $x_n \rightarrow x$. É equivalente a dizer que os abertos que
  contém $x \in X$ formam uma base de vizinhanças de $X$. É o que
  garante que se conhecermos os abertos, conheceremos toda a
  topologia.

  Uma outra interpretação para o item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
  pode ser vista através
  da Figura \ref{fig:distant_set_distant_closure}.
  Suponha que $A \subset X$ é
  um conjunto ``afastado'' de $x$, e $L \subset X$ é tal que $A$ está
  próximo de todos os pontos de $L$. Então, $L$ também é um conjunto
  ``afastado'' de $x$. De fato, se toda vizinhança $V$ de $x$
  intersectar $L$, a condição do item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
  garante a existência de uma vizinhança aberta de $x$ $U$ contida em
  $V$. Esta vizinhança $U$ intersecta $L$, mas por ser um conjunto
  \emph{aberto}, $U$ é também vizinhança dos pontos $y \in U \cap
  L$. Como estes pontos estão ``próximos'' de $A$, temos que $U$ e, a
  fortiori, $V$ intersectam $A$. Ou seja, toda vizinhança de $x$
  intersecta $A$ e portanto, $A$ está ``próximo'' de $x$.
  Mais adiante, veremos que a condição imposta ao
  conjunto $L$ é o mesmo que dizer que $L$ está no \emph{fecho}
  (Definição \ref{def:fecho}) de $A$. E a condição do item
  \refitem{it:prop:espaco_metrico:propriedades_vizinhanca:interior}
  equivale a dizer que a \emph{operação de fecho} é
  \emph{idempotente}. (veja o item
  \refitem{it:prop:propriedades_do_fecho:idempotencia} da Proposição
  \ref{prop:propriedades_do_fecho})

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/distant_set_distant_closure}
    \end{center}
    \caption{
      Todos os pontos de $L$ estão ``próximos'' de $A$, que por sua
      vez, está ``afastado'' de $x$. Então $L$ também está afastado de
      $x$, pois se existisse $y \in L \cap U$, então $U$ seria vizinhança
      de $y$ e, por hipótese, intersectaria $A$.
    }

    \label{fig:distant_set_distant_closure}
  \end{figure}


  Já que nossa tentativa de definir ``topologia'' de um modo abstrato
  utilizando o conceito de \emph{vizinhança} passa necessariamente
  pela idéia de \emph{conjunto aberto}, e os conjuntos abertos por si
  só determinam o que vem a ser uma vizinhança de $x$ (é um conjunto
  que contém um \emph{aberto} que contém $x$ --- Proposição
  \ref{prop:espaco_metrico:vizinhancas_abertas_eh_base}),
  a opção que vamos adotar, ao menos por enquanto, é a de definir a
  topologia especificando quais seriam os conjuntos abertos. Para um
  conjunto $X$, escolhemos $\topology{X} \subset \parts{X}$ de modo que $\topology{X}$
  tenha as propriedades listadas na Proposição
  \ref{prop:espaco_metrico:propriedades_dos_abertos}. Essas
  propriedades são semelhantes às correspondentes para
  ``vizinhanças''. Dizer que $X \in \topology{X}$ é o mesmo que dizer que todo
  ponto tem ao menos uma vizinhança (aberta).

%TODO: Exercício.
%De que forma as propriedades das vizinhanças equivalem às
%propriedades dos abertos da topologia?
