\section{Compacidade em Espaços Métricos}
  \label{section:compacidade_em_espacos_metricos}

  A topologia dos espaços métricos pode ser descrita em
  termos de convergência de sequências.
  Dois conceitos simplificam o elo entre compacidade e
  convergência de sequências:
  \emph{completude}
  e
  \emph{limitação total}.

  \begin{definition}[Sequência de Cauchy e Completude]
    Em um espaço métrico $(X, d)$,
    dizemos que uma sequência $x_n \in X$
    é uma \emph{sequência de Cauchy} quando
    para todo $\varepsilon > 0$ existir $N \in \naturals$
    tal que
    \begin{equation*}
      m,n \geq N
      \Rightarrow
      d(x_n, x_m) \leq \varepsilon.
    \end{equation*}
    Dizemos que $X$ é \emph{completo} quando
    toda sequência de Cauchy $x_n$ convergir para algum
    $x \in X$.
  \end{definition}

  Sequências convergentes são sempre de Cauchy. % TODO: EXERCÍCIO.
  Assim, um espaço métrico é completo quando as sequências
  forem convergentes se, e somente se, forem de Cauchy.
  De certa forma, as sequências de Cauchy podem ser entendidas como
  sequências que ``deveriam convergir'',
  e que se não convergem, é porque em um certo sentido
  o suposto ponto de convergência está faltando.
  Ou seja, se a sequência de Cauchy não converge é
  porque o espaço é incompleto.

  \begin{example}
    Com a métrica usual,
    conjunto $(0,1]$ não é completo.
    O ponto que ``falta'' seria justamente o $0$.
  \end{example}

  Não vamos discutir propriedades dos espaços métricos
  além do necessário para discutir questões topológicas.
  A completude de um espaço topológico não é uma propriedade
  topológica.
  Duas métricas $d$ e $r$ em um mesmo conjunto $X$ podem
  ser compatíveis (induzem a mesma topologia)
  e serem tais que $(X,d)$ é completo, $(X,r)$ é incompleto.

  \begin{example}
    O conjunto $(0,1]$ é homeomorfo a $[1,\infty)$.
    Ou seja, podemos colocar em $(0,1]$ a métrica Euclideana,
    e obtermos um espaço incompleto,
    mas também podemos transportar para $(0,1]$,
    através do homeomorfismo $x \mapsto \frac{1}{x}$,
    a métrica Euclideana de $[1,\infty)$.
    Em outras palavras,
    $(0,1]$ é completo com a métrica
    \begin{equation*}
      d(x,y)
      =
      \abs{\frac{1}{y} - \frac{1}{x}}.
    \end{equation*}
  \end{example}

  Em nossa discussão sobre compacidade,
  a propriedade mas importante das sequências de Cauchy
  é dada pelo seguinte Lema.

  \begin{lemma}
    \label{lemma:Cauchy_com_subsequencia_convergente}
    Seja $(X,d)$ um espaço métrico.
    E seja $x_n$ uma sequência de Cauchy tal que existe uma
    subsequência $x_{n_k}$ que converge para $x$.
    Então, $x_n$ converge para $x$.
  \end{lemma}

  \begin{proof}
    Seja $\varepsilon > 0$.
    Então, existe $N$ tal que
    \begin{equation*}
      n,m \geq N
      \Rightarrow
      d(x_n, x_m)
      <
      \frac{\varepsilon}{2},
    \end{equation*}
    e $n_k \geq N$ tal que
    $
      d(x_{n_k}, x)
      <
      \frac{\varepsilon}{2}
    $.
    Assim, substituindo $m$ por $n_k$,
    temos que
    \begin{align*}
      n \geq N
      \Rightarrow
      d(x_n, x)
      &\leq
      d(x_{n_k}, x)
      +
      d(x_n, x_{n_k})
      \\
      &<
      \frac{\varepsilon}{2}
      +
      \frac{\varepsilon}{2}
      =
      \varepsilon.
    \end{align*}
  \end{proof}


  \begin{definition}[Limitação Total]
    Um espaço métrico $(X, d)$ é \emph{totalmente limitado}
    quando, dado $\varepsilon > 0$, existirem finitas bolas
    $\ball{\varepsilon}{x_1}, \dotsc, \ball{\varepsilon}{x_n}$
    com
    \begin{equation*}
      X
      =
      \bigcup_{j=1}^n
      \ball{\varepsilon}{x_j}.
    \end{equation*}
  \end{definition}

  Dizer que um espaço métrico é totalmente limitado,
  é o mesmo que dizer que toda sequência possui
  uma subsequência de Cauchy.

  \begin{lemma}
    \label{lemma:totalmente_limitado:caracterizacao}
    Um espaço métrico $(X,d)$ é totalmente limitado
    se, e somente se, toda sequência possuir uma
    subsequência de Cauchy.
  \end{lemma}

  \begin{proof}
    Suponha que $X$ seja totalmente limitado.
    Para uma sequência arbitrária $x_n$,
    vamos escolher uma subsequência de Cauchy.
    Faça $X_0 = X$.
    Para cada $k = 1, 2, \dotsc$, $X_{k-1}$ pode ser coberto
    por uma quantidade finita de bolas de raio $\frac{1}{k}$.
    Seja $B_k$ a bola tal que $X_{k-1} \cap B_k$
    contém infinitos termos da sequência original.
    Faça $X_k = X_{k-1} \cap B_k$, e
    escolha $n_k$ (maior que $n_{k-1}$) tal que
    $x_{n_k} \in X_k$.
    Esta é uma subsequência de Cauchy.
    De fato, para $k, j \geq N$,
    como $X_k$ e $X_j$ tem diâmetro menor
    ou igual a $\frac{1}{N}$, temos que
    \begin{equation*}
      d(x_{n_k}, x_{n_j})
      \leq
      \frac{2}{N}.
    \end{equation*}

    Por outro lado, se $X$ não é totalmente limitado,
    então, existe $\varepsilon > 0$ tal que nenhuma cobertura de
    $X$ por bolas de raio $\varepsilon$ é finita.
    Sendo assim, escolha $x_1 \in X$,
    e escolhido $x_n$, escolha
    \begin{equation*}
      x_{n+1}
      \in
      X
      \setminus
      \bigcup_{j=1}^n
      \ball{\varepsilon}{x_j}.
    \end{equation*}
    Para esta sequência, quando $j \neq k$,
    $d(x_j, x_k) \geq \varepsilon$.
    Para esta sequência, nenhuma subsequência é de Cauchy.
  \end{proof}

  Um fato simples sobre espaços (métricos) totalmente limitados
  é que eles possuem uma base enumerável.

  \begin{lemma}
    \label{lemma:totalmente_limitado_tem_base_enumeravel}
    Todo espaço métrico $(X,d)$ totalmente limitado possui uma
    base enumerável.
  \end{lemma}

  \begin{proof}
    Pela
    Proposição \ref{prop:espaco_metrico:separavel_sse_tem_base_enumeravel},
    basta mostrar que existe um subconjunto enumerável denso.
    Para cada $n \in \notnullnaturals$,
    existe um conjunto finito $S_n \subset X$ tal que
    \begin{equation*}
      X
      =
      \bigcup_{x \in S_n}
      \ball{\frac{1}{n}}{x}.
    \end{equation*}
    Neste caso,
    \begin{equation*}
      S
      =
      \bigcup_{n=1}^\infty
      S_n
    \end{equation*}
    é um enumerável denso.
    De fato, se $A \subset X$ é aberto,
    então $A$ contém uma bola
    $\ball{\frac{1}{n}}{a}$.
    Pela definição de $S_n$,
    existe $s \in S_n$ tal que
    $a \in \ball{\frac{1}{n}}{s}$.
    Mas isso significa que
    $s \in \ball{\frac{1}{n}}{a} \subset A$.
  \end{proof}

  Em espaços com base enumerável, a compacidade é mais
  fácil de ser caracterizada.

  \begin{lemma}
    \label{lemma:compacidade_com_base_enumeravel}
    Se $X$ é um espaço topológico com uma base
    $\family{B}$ enumerável, então,
    são equivalentes:
    \begin{enumerate}
      \item
        \label{it:lemma:compacidade_com_base_enumeravel:nao_compacto}
        $X$ não é compacto.

      \item
        \label{it:lemma:compacidade_com_base_enumeravel:nao_compacto_enumeravel}
        Existe uma cobertura aberta enumerável de $X$
        sem subcobertura finita.

      \item
        \label{it:lemma:compacidade_com_base_enumeravel:abertos_crescentes}
        Existe uma sequência de abertos
        \begin{equation*}
          U_1
          \subsetneq
          U_2
          \subsetneq
          \dotsb,
        \end{equation*}
        com $X = \bigcup_{n=1}^\infty U_n$.
    \end{enumerate}
  \end{lemma}

  \begin{proof}
    \proofitem
    {
      $
        \refitem{it:lemma:compacidade_com_base_enumeravel:abertos_crescentes}
        \Rightarrow
        \refitem{it:lemma:compacidade_com_base_enumeravel:nao_compacto_enumeravel}
        \Rightarrow
        \refitem{it:lemma:compacidade_com_base_enumeravel:nao_compacto}
      $
    }
    {
      Estas implicações são evidentes.
    }

    \proofitem
    {
      $
        \refitem{it:lemma:compacidade_com_base_enumeravel:nao_compacto}
        \Rightarrow
        \refitem{it:lemma:compacidade_com_base_enumeravel:abertos_crescentes}
      $
    }
    {
      Este é o conteúdo
      do Corolário \ref{corolary:compacidade:sequencia_crescente_de_abertos}.
    }
  \end{proof}


  Agora podemos caracterizar os espaços métricos compactos em
  termos de convergência de sequências.
  Note que os lemas anteriores implicam que um espaço métrico
  é completo e totalmente limitado se, e somente se,
  toda sequência possuir uma subsequência convergente.

  \begin{proposition}
    \label{prop:espaco_metrico:compacidade:caracterizacao}
    \label{prop:espaco_metrico:compacidade_com_sequencias}
    Seja $(X,d)$ um espaço métrico.
    Então, as seguintes afirmações são equivalentes.
    \begin{enumerate}
      \item
        \label{it:prop:espaco_metrico:compacidade:caracterizacao:compacto}
        $X$ é compacto.

      \item
        \label{it:prop:espaco_metrico:compacidade:caracterizacao:sequencialmente_compacto}
        Toda sequência $x_n \in X$ tem uma subsequência convergente.

      \item
        \label{it:prop:espaco_metrico:compacidade:caracterizacao:totalmente_limitado_completo}
        $X$ é completo e totalmente limitado.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Já vimos que os itens
    \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:sequencialmente_compacto}
    e
    \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:totalmente_limitado_completo}
    são equivalentes,
    mas mesmo assim, vamos formalizar aqui a demonstração.

    \proofitem
    {
      $
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:sequencialmente_compacto}
        \Leftrightarrow
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:totalmente_limitado_completo}
      $
    }
    {
      Se $x_n$ é uma sequência,
      e $X$ é totalmente limitado,
      então,
      pelo Lema \ref{lemma:totalmente_limitado:caracterizacao},
      $x_n$ possui uma subsequência de Cauchy.
      Mas se $X$ também é completo,
      essa subsequência é convergente.

      Por outro lado,
      se $X$ não é completo,
      então, existe uma sequência de Cauchy $x_n$ que não converge.
      Pelo Lema \ref{lemma:Cauchy_com_subsequencia_convergente},
      $x_n$ não possui subsequência convergente.
      E se $X$ não é totalmente limitado,
      o Lema \ref{lemma:totalmente_limitado:caracterizacao}
      implica que existe uma sequência $x_n$ sem subsequência de Cauchy.
      Em particular, $x_n$ não possui subsequência convergente,
      já que toda sequência convergente é de Cauchy.
    }


    \proofitem
    {
      $
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:compacto}
        \Rightarrow
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:sequencialmente_compacto}
      $
    }
    {
      Suponha que $X$ é compacto.
      Seja
      \begin{equation*}
        F_N
        =
        \closure
        {
          \setsuchthat{x_n}{n \geq N}
        }.
      \end{equation*}
      Os conjuntos $F_N$ formam uma sequência decrescente de fechados não vazios.
      Pela compacidade de $X$, sabemos que o limite
      $F = \bigcap_{N=1}^\infty F_N$ não pode ser vazio.
      Portanto, existe $x \in F$.
      Agora, para cada $k = 1, 2, \dotsc$,
      escolha $n_k \rightarrow \infty$ tal que
      $x_{n_k} \in \ball{\frac{1}{k}}{x}$.
      Então, a sequência $x_{n_k}$ é uma subsequência de $x_n$
      que converge para $x$.
    }


    \proofitem
    {
      $
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:sequencialmente_compacto}
        \text{ e }
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:totalmente_limitado_completo}
        \Rightarrow
        \refitem{it:prop:espaco_metrico:compacidade:caracterizacao:compacto}
      $
    }
    {
      Se $X$ é totalmente limitado, então,
      pelo Lema \ref{lemma:totalmente_limitado_tem_base_enumeravel},
      $X$ tem base enumerável.
      Neste caso, se $X$ não é compacto,
      pelo Lema \ref{lemma:compacidade_com_base_enumeravel},
      existe uma sequência de abertos
      $U_1 \subsetneq U_2 \subsetneq \dotsb$,
      tais que $X = \bigcup_{n=1}^\infty U_n$.

      Escolha $x_n \in U_{n+1} \setminus U_n$.
      Para qualquer $x \in X$, existe $N$ tal que
      $x \in U_N$.
      Portanto, para $n \geq N$, $x_n \not \in U_N$.
      Ou seja, nenhuma subsequência de $x_n$ pode convergir
      para $x$.
      Como $x \in X$ é arbitrário,
      nenhuma subsequência de $x_n$ converge.
    }
  \end{proof}


  {\input{2/06/05/exercicios}}
