\section{Espaços de Hausdorff}

  Os espaços métricos possuem propriedades que nem sempre
  estão presentes nos espaços topológicos em geral.
  Uma dessas propriedades é
  a Proposição \ref{prop:espaco_metrico_eh_hausdorff},
  que diz que dois pontos distintos podem ser separados
  por bolas disjuntas.
  Foi esta propriedade que nos permitiu mostrar
  que os subconjuntos compactos de
  $\reals$ com sua topologia usual são fechados
  (Lema \ref{lemma:compactos_nos_reais_sao_fechados}).
  Da mesma forma,
  a Proposição \ref{prop:espaco_metrico_eh_hausdorff}
  pode ser usada para demonstrar que em um espaço métrico,
  os subconjuntos compactos são sempre fechados.

  \begin{definition}[Espaço de Hausdorff]
    \label{def:espaco_de_Hausdorff}
    Um \emph{espaço (topológico) de Hausdorff}
    é um espaço topológico $X$ tal que para todos os
    elementos $a,b \in X$ distintos,
    existem
    $U \in \neighbourhoods{a}$
    e
    $V \in \neighbourhoods{b}$
    com $U \cap V = \emptyset$.
    Também dizemos que $X$ é \emph{de Hausdorff},
    ou simplesmente que $X$ é \emph{Hausdorff}.
  \end{definition}

  %TODO: exercícios -- U e V abertos.
  A Definição \ref{def:espaco_de_Hausdorff}
  poderia ter sido feita com $U$ e $V$ abertos.
  A demonstração e o enunciado precisos
  deste fato ficam como exercício.

  O axioma
  da Definição \ref{def:espaco_de_Hausdorff}
  garante que de uma certa forma,
  dois pontos distintos $a$ e $b$ podem ser
  separados por vizinhanças.
  Esse tipo de axioma é chamado de
  \emph{axioma de separação}.
  Veremos outros tipos de axioma de separação
  no Capítulo \ref{ch:axiomas_de_separacao}.
  Veja também
  o Exercício \ref{exerc:Hausdorff:conjunto_unitario_eh_fechado}.

  Os espaços métricos são espaços de Hausdorff.
  Talvez por isso, os espaços que não são de Hausdorff
  fujam um pouco à nossa intuição.
  Quando um espaço é de Hausdorff,
  em certos casos podemos tratá-lo como se fosse um espaço métrico.
  Ao invés de dizermos
  \begin{quote}
    Tome $\varepsilon > 0$ tal que $\varepsilon < \frac{1}{2}d(a,b)$.
  \end{quote}
  podemos simplesmente dizer
  \begin{quote}
    Tome vizinhanças disjuntas de $a$ e $b$.
  \end{quote}
  Mesmo argumentos com espaços métricos ficam mais elegantes
  se evitarmos escolher $\varepsilon$ para ao invés disso,
  utilizarmos
  a Proposição \ref{prop:espaco_metrico_eh_hausdorff}.
  Por outro lado, nem todos os espaços topológicos são de Hausdorff.
  Ao identificarmos que um espaço não é de Hausdorff,
  sabemos que existem certas propriedades que
  este espaço pode ter,
  mas que fogem à nossa intuição.

  \begin{example}
    O conjunto dos números reais, com sua topologia usual,
    é um espaço de Hausdorff.
    De fato,
    pela Proposição \ref{prop:espaco_metrico_eh_hausdorff},
    qualquer espaço métrico é um espaço de Hausdorff.
  \end{example}

  \begin{example}[Topologia caótica]
    Seja $X$ um conjunto com mais de um elemento.
    Então, dotado da topologia caótica
    $\set{X, \emptyset}$, $X$ não é de Hausdorff.
    Note que na topologia caótica,
    todos os subconjuntos de $X$ são compactos,
    mas os fechados são apenas $X$ e $\emptyset$.
  \end{example}

  \begin{example}[Convergência pontual]
    Dados os conjuntos $X$ e o espaço topológico $Y$,
    se $Y$ é de Hausdorff, então, o conjunto das funções
    $\function{f}{X}{Y}$ com a topologia da convergência pontual
    (Exemplo \ref{ex:topologia_produto:convergencia_pontual})
    é de Hausdorff.
    Isso porquê, se duas funções $f$ e $g$ são distintas,
    então existe $x \in X$ tal que $f(x) \neq g(x)$.
    Tome duas vizinhanças disjuntas $U$ e $V$ de
    $f(x)$ e $g(x)$, e perceba que os conjuntos
    $\pi_x^{-1}(U)$
    e
    $\pi_x^{-1}(V)$
    são vizinhanças disjuntas de $f$ e $g$.
  \end{example}

  % TODO: exercício -- \prod X_\lambda é Hausdorff sse todos são.

  Assim como no caso dos espaços métricos,
  os subconjuntos compactos de um espaço de Hausdorff
  são sempre fechados.

  \begin{proposition}
    \label{prop:Hausdorff:compactos_sao_fechados}
    \label{prop:compacto_Hausdorff:compacto_sse_fechado}
    Se $X$ é um espaço de Hausdorff,
    então todo subconjunto compacto é fechado.
    Se $X$ é compacto Hausdorff,
    então, os subconjuntos de $X$ que são compactos
    são exatamente os subconjuntos fechados.
  \end{proposition}

  \begin{proof}
    A demonstração da primeira parte é idêntica à demonstração
    do Lema \ref{lemma:compactos_nos_reais_sao_fechados}.
    Suponha que $K \subset X$ é compacto.
    Tome $a \not \in K$.
    Vamos mostrar que $a \not \in \closure{K}$.
    Para cada $k \in K$, existem vizinhanças abertas e disjuntas
    $U_k$ e $V_k$ de $k$ e $a$.
    Note que
    \begin{equation*}
      K
      \subset
      \bigcup_{k \in K}
      U_k
    \end{equation*}
    é uma cobertura aberta de $K$.
    Pela compacidade de $K$,
    existem $k_1, \dotsc, k_n$ tais que,
    \begin{equation*}
      K
      \subset
      U_{k_1} \cup \dotsb \cup U_{k_n}.
    \end{equation*}
    Faça $U = U_{k_1} \cup \dotsb \cup U_{k_n}$
    e $V = V_{k_1} \cap \dotsb \cap V_{k_n}$.
    Então,
    $V$ é uma vizinhança de $a$, tal que
    \begin{equation*}
      V \cap K
      \subset
      V \cap U
      =
      \emptyset.
    \end{equation*}
    E portanto, $a \not \in \closure{K}$.
    Assim, concluímos que $K$ é fechado.

    A última afirmação é evidente.
  \end{proof}


  \input{2/06/06/01_rigidez_compacto_hausdorff}
  \input{2/06/06/02_unicidade_da_convergencia}

  \input{2/06/06/exercicios}
