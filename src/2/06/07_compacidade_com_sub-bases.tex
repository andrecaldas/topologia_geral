\section{Compacidade com Sub-Bases}

  É bastante claro que,
  ao verificarmos a compacidade de um espaço,
  é suficiente verificarmos as coberturas
  formadas por elementos de uma base fixada.
  Isso porque, toda cobertura de abertos $\family{U}$
  pode ser \emph{``refinada''} por uma cobertura
  formada apenas por elementos da base da topologia
  (veja
  a Proposição \ref{prop:compacidade_com_bases}).
  É um fato surpreendente (ao menos para o autor),
  que para verificar a compacidade de um espaço,
  é suficiente verificar a existência de subcoberturas
  finitas para coberturas formadas por elementos de
  uma sub-base.
  Este é o conteúdo do teorema a seguir.
  Vamos demonstrar de duas formas.
  A primeira utiliza \emph{indução transfinita}
  e o \emph{Princípio da Boa Ordenação}.
  A segunda demonstração utiliza o
  \emph{Lema de Zorn}.
  Antes, vamos precisar de um Lema.
  Material sobre esses assuntos pode ser encontrado
  no Apêndice \ref{appendix:teoria_de_conjuntos}.

  \begin{lemma}
    \label{lemma:sub-base_de_Alexander:passo_de_inducao}
    Seja $\family{S}$ uma sub-base para a topologia de $X$,
    e seja $\family{B}$ a base gerada por $\family{S}$.
    Se $\family{V} \subset \family{B}$ é uma cobertura
    sem subcobertura finita, e
    $\emptyset \neq V \in \family{V}$,
    então, podemos adicionar a $\family{V}$,
    um conjunto
    $S_V \in \family{S}$
    com $S_V \supset V$,
    de modo que a família
    \begin{equation*}
      \family{V}'
      =
      \family{V}
      \cup
      \set{S_V}
    \end{equation*}
    também não possui subcobertura finita.
  \end{lemma}

  \begin{proof}
    Sabemos que $V \neq X$.
    Escreva $V = V_1 \cap \dotsb \cap V_n$,
    com $V_j \in \family{S}$.
    Para $j = 1, \dotsc, n$, faça
    \begin{equation*}
      \family{V}_j
      =
      \family{V}
      \cup
      \set{V_j}.
    \end{equation*}
    Se todas as coberturas $\family{V}_j$ tivessem
    subcobertura finita, $\family{V}$ também teria
    (por quê?).
    Portanto, fazendo $S_V = V_j$ para algum $j$
    tal que $\family{V}_j$ não tem subcobertura finita,
    temos a família $\family{V}'$ satisfazendo
    a condição desejada.
  \end{proof}


  \begin{theo}[Teorema de Sub-Base de Alexander]
    \label{theorem:Alexander}
    Seja $\family{S}$ uma sub-base para a topologia
    do espaço $X$.
    Então, $X$ é compacto se, e somente se,
    toda cobertura $\family{U} \subset \family{S}$
    possuir uma subcobertura finita.
  \end{theo}

  \begin{proof}[(Demonstração utilizando o princípio da boa ordenação)]
    É evidente que a condição é necessária.
    Vamos mostrar que se um espaço não é compacto,
    então existe uma cobertura formada por elementos da
    sub-base, mas que não possui subcobertura finita.
    Fica como exercício mostrar que
    se $\family{S}$ não cobre $X$,
    então $X$ é compacto.
    Portanto, podemos assumir que $\family{S}$ cobre $X$.
    Seja $\family{B}$ a base gerada por $\family{S}$.
    Como $X$ não é compacto, existe uma família
    $\family{U}' \subset \family{B}$ que cobre $X$ e que
    não possui subcobertura finita.

    Seja $\prec$ uma boa ordem em $\family{U}'$.
    Vamos utilizar a seguinte notação.
    Defina
    \begin{equation*}
      \family{U}_U^*
      =
      \setsuchthat{S_W}{W \precneqq U}
      \cup
      \family{U}'
    \end{equation*}
    e
    \begin{equation*}
      \family{U}_U
      =
      \setsuchthat{S_W}{W \prec U}
      \cup
      \family{U}'.
    \end{equation*}
    Definidos $S_W \in \family{S}$ para todo $W \precneqq U$ tal que
    $\family{U}_W$ não possui subcobertura finita,
    então $\family{U}_U^*$ também não tem subcobertura finita.
    De fato,
    se $U$ é o primeiro elemento de $\family{U}$,
    $\family{U}_U^* = \family{U}$ não tem subcobertura finita por hipótese.
    Caso contrário,
    uma subcobertura finita de $\family{U}_U^*$ estaria
    toda contida em $\family{U}_W$ para algum $W < U$,
    mas $\family{U}_W$ não tem subcobertura finita.

    O Lema \ref{lemma:sub-base_de_Alexander:passo_de_inducao}
    implica que existe $S_U \in \family{S}$,
    com $S_U \supset U$,
    tal que $\family{U}_U$ é uma cobertura sem subcobertura finita.
    Por indução transfinita,
    para todo $U \in \family{U}'$,
    $\family{U}_U$ é uma cobertura sem subcobertura finita.
    Mas isso implica que
    $\family{U} = \setsuchthat{S_U}{U \in \family{U}'}$
    é uma cobertura, pois $S_U \supset U$,
    mas sem subcobertura finita.
    De fato,
    se $\family{U}$ tivesse subcobertura finita
    $S_1, \dotsc, S_n$, então existiria $U \in \family{U}'$
    tal que $S_1, \dotsc, S_n \in \family{U}_U$,
    contrariando o fato de $\family{U}_U$ não possuir
    subcobertura finita.
    Como $\family{U} \subset \family{S}$,
    a proposição fica demonstrada.
  \end{proof}

  Vamos demonstrar o mesmo fato usando o
  \emph{Lema de Zorn}.
  É um bom exercício comparar as duas demonstrações.
  % TODO: exercício -- comparar as demonstrações.

  \begin{proof}[(Demonstração utilizando o lema de Zorn)]
    Denotando por $\family{B}$ a base gerada por $\family{S}$,
    basta mostrar que quando $X$ não é compacto,
    existe uma cobertura $\family{U} \subset \family{S}$
    sem subcobertura finita.
    O conjunto $\Gamma$ das subfamílias de $\family{B}$ sem subcobertura
    finita não é vazio, pois $X$ não é compacto.
    Ordenando as subfamílias de $\family{B}$ por inclusão,
    se $\family{U}_\lambda \in \Gamma$ ($\lambda \in \Lambda$)
    é uma cadeia de subcoberturas sem subcobertura finita,
    então,
    utilizando o mesmo argumento da demonstração por
    indução transfinita, concluímos que
    \begin{equation*}
      \family{U}^*
      =
      \bigcup_{\lambda \in \Lambda}
      \family{U}_\lambda
    \end{equation*}
    é uma cobertura sem subcobertura finita,
    pois se $\family{U}^*$ tivesse subcobertura finita,
    essa subcobertura estaria contida em
    $\family{U}_\lambda$ para algum $\lambda \in \Lambda$.
    Assim, $\Gamma$ é indutivamente ordenado,
    e por isso possui um elemento maximal $\family{U}_m$.

    Pelo Lema \ref{lemma:sub-base_de_Alexander:passo_de_inducao},
    assim como na demonstração utilizando indução transfinita,
    vemos que se $U = U_1 \cap \dotsb \cap U_n \in \family{U}_m$,
    com $U_j \in \family{S}$,
    então existe $S_U \in \family{U}'$,
    com $S_U \supset U$, tal que
    $\family{U}_m \cup \set{S_U}$ não possui subcobertura finita.
    Pela maximalidade de $\family{U}_m$, temos que
    $S_U \in \family{U}_m$.
    Mas isso implica que $\family{U} = \family{U}_m \cap \family{S}$
    cobre $X$ (por quê?).
    E como $\family{U}_m$ não tem subcobertura finita,
    $\family{U} \subset \family{U}_m$ também não tem,
    concluindo a demonstração.
    % TODO: exercício S \cap U_m cobre X.
  \end{proof}


  Um exemplo interessante de aplicação do Teorema de Alexander
  é a compacidade dos intervalos $[a,b] \subset \reals$.

  \begin{example}[Compacidade com sub-base em $\reals$]
    \label{example:alexander:0_1_compacto}
    Uma sub-base para a topologia usual de $\reals$ é
    a família
    \begin{equation*}
      \family{S}
      =
      \setsuchthat{(-\infty, x)}{x \in \reals}
      \cup
      \setsuchthat{(x, \infty)}{x \in \reals}.
    \end{equation*}
    Suponha que $\family{U} \subset \family{S}$ seja
    uma cobertura de $[a,b]$.
    Se os conjuntos da forma $(-\infty, x)$ de $\family{U}$
    cobrem $[a,b]$, então existe $x > b$,
    com $(-\infty, x) \in \family{U}$.
    O mesmo argumento vale se os conjuntos da forma
    $(x, \infty)$ de $\family{U}$ cobrirem $[a,b]$.
    Caso contrário, tomando como $B$ o supremo dos $x \in \reals$
    tais que $(-\infty, x) \in \family{U}$,
    e $A$ o ínfimo dos $x \in \reals$
    tais que $(x, \infty) \in \family{U}$,
    é fácil ver que $A < B$.
    Ou seja, existem $\alpha, \beta \in \reals$, com
    $A < \alpha < \beta < B$ tais que
    $(-\infty, \beta), (\alpha, \infty) \in \family{U}$.
    Assim,
    \begin{equation*}
      [a,b]
      \subset
      \reals
      =
      (-\infty, \beta)
      \cup
      (\alpha, \infty).
    \end{equation*}
    Pelo Teorema \ref{theorem:Alexander},
    $[a,b]$ é compacto.
  \end{example}


  \input{2/06/07/exercicios}
