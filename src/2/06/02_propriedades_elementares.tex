\section{Propriedades Elementares}

  Vamos estabelecer algumas propriedades elementares
  e algumas caracterizações de compacidade.
  A mais importante dessas propriedades é o fato de
  a imagem de um compacto por uma aplicação contínua também
  ser um conjunto compacto.
  A mais simples é o fato de todo fechado dentro de
  um compacto ser compacto.

  \begin{proposition}
    \label{prop:fechado_no_compacto_eh_compacto}
    Se $K \subset X$ é compacto e
    $F \subset K$ é fechado,
    então $F$ é compacto.
  \end{proposition}

  \begin{proof}
    Seja $\family{U}$ uma cobertura de $F$.
    Então,
    $\family{V} = \family{U} \cup \set{\complementset{F}}$
    é uma cobertura de $K$.
    Pela compacidade de $K$, existe uma subcobertura
    $\family{V}' \subset \family{V}$ finita.
    Neste caso,
    $\family{U}' = \family{V}' \setminus \set{\complementset{F}}$
    é uma subfamília finita de $\family{U}$,
    e cobre $F$.
  \end{proof}

  % TODO: exercício -- a proposição é mais forte que F C Y C X,
  % com F fechado em X.


  \begin{proposition}
    \label{prop:imagem_de_compacto}
    Sejam $X$ e $Y$ espaços topológicos,
    $K \subset X$ um compacto e
    $\function{f}{X}{Y}$ uma aplicação contínua.
    Então, $f(K)$ é um compacto de $Y$.
  \end{proposition}

  \begin{proof}
    Seja $\family{U}$ uma cobertura aberta de $f(K)$,
    então, $f^{-1}(\family{U})$ é uma cobertura aberta de $K$.
    Pela compacidade de $K$, existe uma subfamília finita
    $\family{V} \subset \family{U}$ tal que
    $f^{-1}(\family{V})$ cobre $K$.
    Mas isso implica que $\family{V}$ cobre $f(K)$.
    Ou seja, $f(K)$ é compacto.
  \end{proof}


% TODO: Exercício -- Onde usamos que $X_\lambda$ não é vazio?
  \begin{example}
    Seja $X_\lambda$ ($\lambda \in \Lambda$) uma família de
    espaços topológicos não vazios.
    Então,
    \begin{equation*}
      X
      =
      \prod_{\lambda \in \Lambda}
      X_\lambda,
    \end{equation*}
    com a topologia produto,
    só pode ser compacto se todos os $X_\lambda$ forem compactos.
    De fato, se $X$ é compacto,
    como as projeções canônicas
    $\function{\pi_\lambda}{X}{X_\lambda}$
    são contínuas e sobrejetivas,
    os espaços $X_\lambda = \pi_\lambda(X)$ são todos compactos.
    O Teorema
    \ref{theorem:produto_de_compactos},
    mais adiante, mostrará que vale a recíproca:
    se todos os $X_\lambda$ forem compactos,
    então $X$ é compacto.
  \end{example}


  Para verificarmos se um espaço topológico é ou não compacto,
  a princípio, precisamos verificar que toda cobertura por abertos
  possui uma subcobertura finita.
  No entanto, de posse de uma base da topologia,
  a verificação pode ser restrita a subcoberturas desta base.

  \begin{proposition}
    \label{prop:compacidade_com_bases}
    Seja $\family{B}$ uma base de um espaço topológico $X$.
    Então, $X$ é compacto se, e somente se,
    toda cobertura $\family{U} \subset \family{B}$
    possuir subcobertura finita.
  \end{proposition}

  \begin{proof}
    É evidente que a condição é necessária.
    Vamos mostrar que é suficiente.
    Seja $\family{V}$ uma cobertura de abertos de $X$.
    Cada aberto $V \in \family{V}$ pode ser escrito da forma
    \begin{equation*}
      V
      =
      \bigcup_{U \in \family{U}_V}
      U,
    \end{equation*}
    para uma família $\family{U}_V \subset \family{B}$
    adequada.
    Por hipótese,
    \begin{equation*}
      \family{U}
      =
      \bigcup_{V \in \family{V}}
      \family{U}_V
    \end{equation*}
    possui uma subcobertura finita.
    Em particular, existem
    $V_1, \dotsc, V_n \in \family{V}$
    tais que a família
    $\family{U}_{V_1} \cup \dotsb \cup \family{U}_{V_n}$
    cobre $X$.
    Ou seja, $V_1, \dotsc, V_n$ é uma subcobertura finita de
    $\family{V}$.
  \end{proof}

  Vamos ao exemplo mais importante de conjunto compacto.

  \begin{example}[Intervalo Fechado Limitado em $\reals^n$]
    \label{ex:intervalo_fechado_eh_compacto}
    O conjunto $I = [a,b]$ é compacto na topologia usual
    induzida de $\reals$.
    Vamos usar
    a Proposição \ref{prop:compacidade_com_bases}.
    Primeiramente,
    note que a família de todos os intervalos abertos
    forma uma base para a topologia de $I$.
    Lembre-se que intervalos da forma $[a,c)$
    e $(c,b]$ são abertos na topologia de $I$.

    Seja $\family{U}$ uma cobertura de $I$ formada por intervalos abertos.
    Seja $J \subset I$ o conjunto de todos os elementos $c \in I$
    tais que $[a,c]$ possui uma subcobertura finita de $\family{U}$.
    É claro que $a \in J$.

    Note que para cada $V \in \family{U}$,
    \begin{equation*}
      V \cap J \neq \emptyset
      \Leftrightarrow
      V \subset J.
    \end{equation*}
    Mas isso implica que tanto $J$
    quanto $I \setminus J$ são abertos.
    Pela conexidade de $I$, temos que $J = I$.
  \end{example}


  Em um espaço com base enumerável,
  podemos caracterizar compacidade em termos de
  sequências de abertos.

  \begin{corolary}
    \label{corolary:compacidade:sequencia_crescente_de_abertos}
    Um espaço topológico $X$ com base enumerável é compacto
    se, e somente se, para toda sequência crescente de abertos
    $A_1 \subset A_2 \subset \dotsb$
    tal que
    \begin{equation*}
      \bigcup_{n = 1}^\infty
      A_n
      =
      X,
    \end{equation*}
    existir $N$ tal que $A_N = X$.
  \end{corolary}

  \begin{proof}
    Tal sequência $A_n$ é uma cobertura de $X$.
    Se $X$ é compacto, existe uma subcobertura finita
    $A_{n_1}, \dotsc, A_{n_k}$.
    Basta tomar $N = \max \set{n_1, \dotsc, n_k}$,
    para ter $A_N = X$.

    Por outro lado,
    se $\family{B}$ é uma base enumerável,
    e se $X$ não é compacto, existe uma subfamília
    $\set{B_1, B_2, \dotsc} \subset \family{B}$ que cobre $X$,
    mas que não possui subcobertura finita.
    Fazendo $A_n = B_1 \cup \dotsb \cup B_n$,
    temos uma sequência crescente de abertos
    $A_1 \subset A_2 \subset \dotsb$
    tal que sua união é $X$,
    mas todos os $A_n$ são diferentes de $X$.
  \end{proof}


  \begin{proposition}
    \label{prop:produto_finito_de_compactos_eh_compacto}
    Sejam $X_1, \dotsc, X_n$ espaços topológicos compactos não vazios.
    Então, com a topologia produto, o espaço
    $X = X_1 \times \dotsb \times X_n$
    é compacto.
  \end{proposition}

% TODO: desenho.
  \begin{proof}
    Basta mostrar para o caso $n = 2$.
    Seja $\family{U}$ uma cobertura aberta de $X$.
    Pela Proposição \ref{prop:compacidade_com_bases},
    podemos assumir que os abertos em $\family{U}$
    são da forma $U \times V$,
    com $U$ e $V$ abertos de $X_1$ e $X_2$,
    respectivamente.

    Para cada $a \in X_1$,
    o subespaço
    $\set{a} \times X_2$
    é compacto
    (veja
    a Proposição \ref{prop:identificando_a_componente_do_produto_com_um_subespaco}).
    Assim, existe uma subfamília
    $\family{U}_a \subset \family{U}$
    que cobre $\set{a} \times X_2$.
    Note que $\family{U}_a$ é da forma
    \begin{equation*}
      \family{U}_a
      =
      \set{U_1 \times V_1, \dotsc, U_m \times V_m}.
    \end{equation*}
    Em particular, fazendo
    $U^a = U_1 \cap \dotsb \cap U_m$,
    temos que $U^a \subset X_1$ é aberto e
    $\family{U}_a$ cobre $U^a \times X_2$.

    Como os conjuntos da forma $U^a$ cobrem $X_1$,
    e $X_1$ é compacto,
    existem $a_1, \dotsc, a_k \in X_1$ tais que
    $X_1 = U^{a_1} \cup \dotsb \cup U^{a_k}$.
    Ou seja,
    \begin{equation*}
      \family{U}'
      =
      \family{U}_{a_1}
      \cup \dotsb \cup
      \family{U}_{a_k}
    \end{equation*}
    é uma subfamília finita de $\family{U}$
    que cobre $X$.
  \end{proof}


  Como os conjuntos fechados
  são exatamente os complementares dos abertos,
  a compacidade pode ser facilmente descrita em
  termos de conjuntos fechados.

  \begin{proposition}
    Um espaço topológico é compacto se, e somente se,
    toda família de fechados $\family{F}$, com
    \begin{equation*}
      \bigcap_{F \in \family{F}'} F
      \neq
      \emptyset
    \end{equation*}
    para toda subfamília finita
    $\family{F}' \subset \family{F}$,
    for tal que
    \begin{equation*}
      \bigcap_{F \in \family{F}} F
      \neq
      \emptyset.
    \end{equation*}
    Se o espaço tiver base enumerável,
    então, toda sequência decrescente de fechados
    não vazios $F_1 \supset F_2 \supset \dotsb$
    for tal que
    \begin{equation*}
      \bigcap_{n = 1}^\infty
      F_n
      \neq
      \emptyset.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    A primeira parte é a definição de compacidade
    escrita em termos de conjuntos fechados.
    A segunda parte é
    o Corolário \ref{corolary:compacidade:sequencia_crescente_de_abertos}.
  \end{proof}


  {\input{2/06/02/exercicios}}
