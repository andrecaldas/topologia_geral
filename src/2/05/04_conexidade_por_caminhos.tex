\section{Conexidade por Caminhos}

  Os espaços como os do Exemplo
  \ref{ex:conexidade_por_caminhos_em_abertos_do_rn},
  onde todos os pontos podem ser ligados por um
  ``caminho contínuo'', são os espaços
  \emph{conexos por caminhos}.
  Vamos definir formalmente e verificar algumas propriedades
  interessantes dos espaços conexos por caminhos.
  Em especial, vamos ver que a conexidade por caminhos é uma
  propriedade mais forte que a conexidade.
  Ou seja, todos os espaços conexos por caminhos são conexos.

  \begin{definition}[Caminho]
    Seja $X$ um espaço topológico.
    Um \emph{caminho} em $X$ é uma aplicação contínua
    \begin{equation*}
      \function{f}{[0,1]}{X}.
    \end{equation*}
    Dados $a,b \in X$, um \emph{caminho ligando $a$ a $b$} é um
    caminho em $X$ tal que $f(0) = a$ e $f(1) = b$.
  \end{definition}

  \begin{obs}
    Uma aplicação contínua
    $\function{f}{I}{X}$,
    onde $I$ é um intervalo fechado e limitado de $\reals$
    pode ser facilmente transformada em um caminho
    (com domínio $[0,1]$).
    Por isso, de agora em diante, vamos usar um certo abuso
    de linguagem e, neste caso,
    também vamos dizer que $f$ é um caminho em $X$.
  \end{obs}

  \begin{proposition}
    \label{prop:concatenacao_de_caminhos_eh_caminho}

    Sejam $f$ e $g$ caminhos em um espaço topológico $X$ ligando os
    pontos $a,b \in X$ e $b,c \in X$ respectivamente.
    Então, a aplicação
    \begin{equation*}
      (f*g)(t)
      =
      \glueconditions
      {
        \oneconditiontoglue{f(2t)}{0 \leq t \leq \frac{1}{2}}
        \\
        \oneconditiontoglue{g(2t - 1)}{\frac{1}{2} \leq t \leq 1}
      }
    \end{equation*}
    é um caminho em $X$ ligando $a$ e $c$.
  \end{proposition}

  \begin{proof}
    A parte mais difícil é mostrar que $f * g$ é contínua no ponto
    $\frac{1}{2}$.
    Seja $V \subset X$ uma vizinhança de
    $b = (f*g)\left(\frac{1}{2}\right)$
    Então,
    \begin{align*}
      (f*g)^{-1}(V)
      &=
      \frac{1}{2} f^{-1}(V)
      \cup
      \frac{1}{2}
      \left(
        1
        +
        g^{-1}(V)
      \right)
      \\&\supset
      \frac{1}{2}
      \left(\alpha,1\right]
      \cup
      \frac{1}{2}
      \left(
        1
        +
        \left[0,\beta\right)
      \right)
      \\&=
      \left( \frac{\alpha}{2}, \frac{\beta+1}{2} \right).
    \end{align*}
    Portanto, $f * g$ é contínua em $\frac{1}{2}$.
  \end{proof}


  A Proposição
  \ref{prop:concatenacao_de_caminhos_eh_caminho}
  mostra que a relação de
  \emph{``existir um caminho ligando $x$ a $y$} é transitiva.
  Como é evidentemente simétrica e reflexiva, é uma relação de
  equivalência.  Cada classe de equivalência dessa relação será uma
  \emph{componente conexa por caminhos}.

  \begin{definition}[Conexidade por Caminhos]
    Um espaço topológico $X$ é \emph{conexo por caminhos} quando para
    quaisquer dois pontos $x,y \in X$, existir um caminho ligando $x$ a
    $y$.  Dado $x \in X$, a
    \emph{componente conexa por caminhos} de $x$ é o conjunto de todos
    os pontos $y \in X$ tais que existe um caminho ligando $x$ a $y$.

    Um subconjunto de um espaço topológico é conexo por caminhos
    quando o for na topologia induzida.
  \end{definition}

  \begin{proposition}
    Em um espaço topológico $X$, se um conjunto $Y \subset X$é conexo
    por caminhos, então é conexo.
  \end{proposition}

  \begin{proof}
    Tome $a \in Y$.
    Então,
    \begin{equation*}
      Y
      =
      \bigcup_{\substack{\text{$f$: caminho em $Y$} \\ f(0) = a}}
      f([0,1]).
    \end{equation*}
    Os conjuntos $f([0,1])$ são conexos por serem imagem do conexo
    $[0,1]$ pela aplicação contínua $f$.
    Assim, esta é uma união de conjuntos conexos que possuem $a$ como
    ponto em comum.
    Pela Proposição \ref{prop:uniao_de_conexos_com_ponto_em_comum_eh_conexa},
    $Y$ é conexo.
  \end{proof}
% TODO: exercício --- equivalência entre a demonstração e demonstrar
% que a componente conexa por caminhos é conexa.

% TODO: exercício --- por que podemos assumir que Y = X?


  Sabemos que um conjunto conexo por caminhos é conexo.
  Vejamos um exemplo de um espaço conexo que não é conexo por caminhos.

  \begin{example}[Espaço \emph{Pente}]
    \label{ex:espaco_pente}
    Seja $K = \setsuchthat{\frac{1}{n}}{n = 1, 2, \dotsc}$.
    Considere os subconjuntos de $\reals^2$
    \begin{equation*}
      P_1
      =
      \set{0} \times (0,1]
      \quad \text{e} \quad
      P_2
      =
      \left(
        (0,1] \times \set{0}
      \right)
      \cup
      \left(
        K \times [0,1]
      \right).
    \end{equation*}
    O \emph{espaço pente} é o conjunto
    $P = P_1 \cup P_2$ com a topologia induzida de $\reals^2$.
    Veja a Figura
    \ref{fig:espaco_pente}.
    É fácil ver que $P$ é conexo.
    De fato,
    como $P_2$ é conexo e
    \begin{equation*}
      P_2
      \subset
      P
      \subset
      \closure{P_2},
    \end{equation*}
    $P$ é conexo
    pela Proposição
    \ref{prop:fecho_de_um_conexo}.
    No entanto, $P$ não é conexo por caminhos.
    A demonstração será feita
    no Exercício \ref{exerc:conexidade_por_caminhos:pente:nao_conexo_por_caminhos}
    e também no Exemplo
    \ref{ex:espaco_pente:compacidade}.

    \begin{figure}[!htp]
      \begin{center}
        \includegraphics{figs/espaco_pente}
      \end{center}
      \caption{Um espaço topológico que é conexo,
      mas que não é conexo por caminhos.}

      \label{fig:espaco_pente}
    \end{figure}


    Uma variação mais simples do \emph{espaço pente},
    é o conjunto
    \begin{equation*}
      P'
      =
      \set{(0,1)}
      \cup
      P_2.
    \end{equation*}
    Assim como $P$, $P'$ também é conexo.
    Suponha que $\function{f}{[0,1]}{P'}$ seja um caminho
    em $P'$, partindo de $p = (0,1)$.
    Evidentemente que $F = f^{-1}(p)$
    é um fechado de $[0,1]$.
    Vamos mostrar que $F$ também é aberto, para concluirmos
    que $f$ é um caminho constante.
    Ou seja, que $p$ não pode ser ligado a nenhum outro ponto de $P_2$
    por um caminho em $P'$.
    Tome $a \in F$.
    Seja $B_p$ a bola de raio $\frac{1}{2}$ com centro em $p$.
    Como $f^{-1}(B_p)$ é um aberto que contém $a$,
    existe um intervalo fechado $I \subset f^{-1}(B_p)$
    que é vizinhança de $a$.
    Então, $f|_I$ é um caminho em $B_p \cap P'$.
    Mas todos os caminhos em $B_p \cap P'$ que passam em
    $p$ são constantes,
    pois a componente conexa de $p$ em $B_p \cap P'$ é $\set{p}$.
    Como $a \in F$ é arbitrário, $F$ é aberto.
  \end{example}


  {\input{2/05/04/exercicios}}
