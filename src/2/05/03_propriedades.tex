\section{Propriedades}

  Por vezes, é importante construir um conjunto e ao mesmo tempo
  garantir que o conjunto construído será conexo.
  A maneira mais simples de se fazer isso, é utilizando a
  proposição que segue.

  \begin{proposition}
    \label{prop:uniao_de_conexos_com_ponto_em_comum_eh_conexa}

    Seja $C_\lambda$ uma família de subconjuntos conexos de um espaço
    topológico $X$, tal que existe
    \begin{equation*}
      c
      \in
      \bigcap C_\lambda.
    \end{equation*}
    Então a união $\bigcup C_\lambda$ é um conjunto conexo.
  \end{proposition}

  \begin{proof}
    A forma tradicional de se demonstrar é tomando um par de abertos
    $U$ e $V$ que ``particionam'' $\bigcup C_\lambda$, e mostrar que
    esses abertos ``particionam'' ao menos um dos $C_\lambda$.
    Demonstrar dessa forma fica como exercício. Vamos fazer por um
    outro ângulo. ;-)

    Podemos assumir sem perda de generalidade que a união dos
    $C_\lambda$ é todo o espaço $X$ (porquê?).
    Suponha, então, que $F \subset X$ é um conjunto que é aberto e
    fechado com $c \in F$.
    Na topologia induzida em $C_\lambda$,
    os conjuntos $C_\lambda \cap F$ são
    abertos e fechados não-vazios,
    e portanto, são iguais a $C_\lambda$.
    Ou seja, $C_\lambda \subset F$.
    O que mostra que $F = X$.
  \end{proof}


  \begin{proposition}
    \label{prop:fecho_de_um_conexo}

    Seja $X$ um espaço topológico e $C \subset X$ um subconjunto
    conexo.  Então, qualquer conjunto $D \subset X$ satisfazendo
    \begin{equation*}
      C
      \subset
      D
      \subset
      \closure{C}
    \end{equation*}
    é conexo.
  \end{proposition}

  \begin{proof}
    Novamente, fica como exercício para o leitor utilizar um
    argumento que envolva um particionamento por abertos como o do
    item
    \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:particao}
    da Proposição
    \ref{prop:caracterizacao_de_conexidade}.

    Podemos assumir que $D = X$ (por quê?).
    Seja $F$ um conjunto aberto e fechado não vazio.
    Por ser aberto, $F$ intersecta $C$
    (veja a Seção \ref{sec:fecho_e_fechado}).
    Mas, como $C$ é conexo, temos que $F \cap C = C$.  Ou seja,
    \begin{equation*}
      C
      \subset
      F.
    \end{equation*}
    Mas como $F$ é fechado, tomando o fecho, obtemos
    \begin{equation*}
      X
      =
      \closure{C}
      \subset
      F.
    \end{equation*}
    Portanto, os únicos conjuntos que são abertos e fechados ao mesmo
    tempo são $\emptyset$ e $X$.
  \end{proof}


  Por vezes, nos deparamos com propriedades em classes de conjuntos,
  que são \emph{fechadas por união}.  Ou seja, se a família de
  conjuntos $C_\lambda$ possui a propriedade, então o conjunto formado
  pela união dos $C_\lambda$ também possui a mesma propriedade.
  Neste caso, podemos falar do maior conjunto que tem a tal
  propriedade.  No caso de conexidade em espaços topológicos, a
  Proposição
  \ref{prop:uniao_de_conexos_com_ponto_em_comum_eh_conexa}
  nos permite fazer isso.  Seja $\family{F}_x$ a família de todos os
  subconjuntos do espaço topológico que sejam conexos e contenham $x$.
  Então, pela Proposição
  \ref{prop:uniao_de_conexos_com_ponto_em_comum_eh_conexa},
  o conjunto
  \begin{equation*}
    C_x
    =
    \bigcup_{C \in \family{F}}
    C
  \end{equation*}
  é conexo e contém $x$.
  Evidentemente que este é o maior conexo que contém $x$.


  \begin{definition}[Componente Conexa]
    \label{def:componente_conexa}
    Seja $X$ um espaço topológico, e $x \in X$ um ponto qualquer de
    $X$.  Então, a \emph{componente conexa} de $x$ é o maior conexo de
    $X$ que contém o ponto $x$.
  \end{definition}


  \begin{proposition}
    As componentes conexas particionam um espaço topológico $X$.
    Em especial, a relação
    \emph{``$x$ e $y$ estão na mesma componente conexa''} é uma
    relação de equivalência.
  \end{proposition}

  \begin{proof}
    Para um elemento qualquer $x \in X$, vamos denotar por $C_x$ a
    componente conexa de $x$.
    É evidente que $X = \bigcup_{x \in X} C_x$.
    Precisamos mostrar apenas que
    \begin{equation*}
      y \in C_x
      \Rightarrow
      C_x = C_y.
    \end{equation*}
    Mas isso é evidente, já que
    \begin{equation*}
      y \in C_x
      \Rightarrow
      \text{$C_x \cup C_y$ é conexo}.
    \end{equation*}
  \end{proof}


  \begin{proposition}
    \label{prop:componente_conexa_eh_fechada}
    Em um espaço topológico $X$, a componente conexa de um ponto
    $x \in X$ qualquer é fechada.
  \end{proposition}

  \begin{proof}
    É imediato da Proposição
    \ref{prop:fecho_de_um_conexo}
    e da maximalidade da componente conexa.
  \end{proof}

  As componentes conexas de um aberto $A \subset \reals^n$ são abertas,
  mas isso nem sempre acontece em outros espaços topológicos.
  Os Exemplos
  \ref{ex:racionais_totalmente_disconexo:particao}
  e
  \ref{ex:racionais_totalmente_disconexo:aberto_fechado}
  mostram que as componentes conexas de $\rationals$ são
  conjuntos unitários, que não são abertos na toplogia
  induzida de $\reals$ em $\rationals$.

  \begin{example}
    \label{ex:reals-n:componente_conexa_de_aberto}
    Seja $A \subset \reals$ um aberto,
    e $C \subset A$ uma componente conexa de $A$.
    Vamos verificar que $C$ é aberto.
    Para tanto, note que dado $a \in C$,
    existe um intervalo (conexo) aberto $V$,
    com $a \in V \subset A$.
    Pela maximalidade de $C$, temos que
    $V \subset C$.
    Ou seja, $C$ é vizinhança de todos os seus pontos.
  \end{example}


  Se, em uma família de espaços topológicos um deles não é conexo,
  é fácil ver que o produto desses espaços também não é conexo.
  E se todos forem conexos, será que ainda assim o produto pode ser
  desconexo?

  \begin{proposition}
    \label{proposition:produto_de_conexos_eh_conexo}
    O produto $X$ de uma família $X_\lambda$ ($\lambda \in \Lambda$)
    de espaços topológicos não
    vazios é conexo se, e somente se, todos os espaços $X_\lambda$
    forem conexos.
  \end{proposition}

  \begin{proof}
    Se o produto é conexo, então
    $X_\lambda = \pi_\lambda(X)$ é a imagem de um conexo por uma
    aplicação contínua.
    Portanto, pelo Teorema \ref{theo:imagem_de_conexo_eh_conexa},
    cada $X_\lambda$ é conexo.

    Suponha que todos os $X_\lambda$ são conexos.
    Tome $x = (x_\lambda) \in X$.
    Pela Proposição
    \ref{prop:identificando_a_componente_do_produto_com_um_subespaco},
    para cada $\gamma \in \Lambda$, os conjuntos
    \begin{equation*}
      X(\gamma, x)
      =
      \bigcap_{\substack{\lambda \in \Lambda \\ \lambda \neq \gamma}}
      \pi_\lambda^{-1}( x_\lambda )
    \end{equation*}
    são homeomorfos a $X_\gamma$, e portanto, são conexos.
    Note que todos eles contém o elemento $x$.  Pela Proposição
    \ref{prop:uniao_de_conexos_com_ponto_em_comum_eh_conexa}, a união
    \begin{equation*}
      X(x)
      =
      \bigcup_{\gamma \in \Lambda}
      X(\gamma, x)
    \end{equation*}
    é conexa.
    Note que $X(x)$ é o conjunto de todos os elementos de $X$ que
    diferem de $x$ em no máximo uma entrada.

    Seja $u \in X$ um elemento qualquer.
    Denote por $C_u$ a componente conexa de $u$.
    Vamos mostrar que $C_u$ é denso em $X$.
    O argumento anterior, mostra que se $x \in C_u$,
    então $X(x) \subset C_u$.
    Por indução, todos os elementos que diferem de $u$ em
    apenas um número finito de entradas pertencem a $u$.

    Tome um aberto $A \subset X$ da forma
    \begin{equation*}
      A
      =
      \bigcap_{j=1}^n
      \pi_{\lambda_j}^{-1}(A_j).
    \end{equation*}
    Esses abertos formam uma base da topologia produto.
    Seja $a = (a_\lambda)$ tal que
    $a_\lambda \in A_j$ para
    $\lambda \in \set{\lambda_1, \dotsc, \lambda_n}$,
    e
    $a_\lambda = u_\lambda$ para
    $\lambda \not \in \set{\lambda_1, \dotsc, \lambda_n}$.
    Então,
    $a \in A$, e $a \in X(u) \subset C_u$.
    E portanto, $C_u$ é denso em $X$.
    Como $C_u$ é fechado,
    $C_u = X$.
  \end{proof}


  {\input{2/05/03/exercicios}}
