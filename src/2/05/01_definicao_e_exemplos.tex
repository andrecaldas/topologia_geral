\section{Definição e Exemplos}

  \begin{definition}[Conexidade]
    \label{def:conexidade}

    Um espaço topológico $X$ é \emph{conexo} quando não puder ser
    escrito como união disjunta não trivial de abertos.  Ou seja,
    se
    \begin{equation*}
      X
      =
      \bigcup_{\lambda \in \Lambda}
      A_\lambda,
    \end{equation*}
    onde todos os $A_\lambda$ são abertos, não-vazios e disjuntos,
    então $\cardinality{\Lambda} \leq 1$.

    Um subconjunto de um espaço topológico é \emph{conexo} quando for
    conexo na topologia induzida.  Um subconjunto que não é conexo é
    \emph{desconexo}.
  \end{definition}


  \begin{example}
    \label{ex:conexo_nos_reais_eh_intervalo}

    Um intervalo $I \subset \reals$ é um conjunto que satisfaz
    \begin{equation*}
      a,b \in I,\, a < x < b \Rightarrow x \in I.
    \end{equation*}
    Se $Y \subset \reals$ não é um intervalo, então não é conexo.
    De fato, tome $a,b \in Y$ e $x \not \in Y$ com $a < x < b$.
    Então $Y = \left(Y \cap (-\infty,x)\right) \cup \left(Y \cap (x,\infty)\right)$,
    e portanto, $Y$ é desconexo.
  \end{example}


  Em um espaço conexo $X$,
  um argumento padrão consiste em mostrar que
  os pontos $x \in X$ que satisfazem determinada propriedade $P(x)$
  formam um aberto, e os pontos que não satisfazem $P(x)$
  também formam um aberto.
  Como o espaço não é união disjunta não-trivial de abertos,
  ou teremos que todos os pontos satisfazem $P(x)$, ou que nenhum
  ponto satisfaz $P(x)$.

  \begin{example}
    \label{ex:conexidade_por_caminhos_em_abertos_do_rn}

    Seja $A \subset \reals^n$ um aberto conexo.
    Então, dois pontos quaisquer de $A$ podem ser ligados por um
    ``caminho contínuo'' em $A$.
    Ou seja, dados $a,b \in A$, existe $\function{f}{[0,1]}{A}$
    contínua, com $f(0) = a$ e $f(1) = b$.
    Vamos apenas esboçar a demonstração.
    Os pormenores da demonstração ficam como exercício.

    Seja $C$ o conjunto dos pontos que podem ser ligados a $a$.
    Então, $C$ é aberto.
    De fato, se $c \in C$, tomando $\varepsilon > 0$ tal que
    $\ball{\varepsilon}{c} \subset A$, temos que todos os pontos
    $b \in \ball{\varepsilon}{c}$ podem ser ligados a $c$ por um
    ``caminho retilíneo''.
    Assim, ``concatenando'' o caminho de $a$ até $c$ com o caminho de
    $c$ até $b$, temos um caminho de $a$ até $b$.
    Portanto, $\ball{\varepsilon}{c} \subset C$.
    Ou seja, $C$ é aberto.

    Por outro lado, se $c \not \in C$, tomando novamente
    $\varepsilon > 0$ tal que $\ball{\varepsilon}{c} \subset A$, temos
    que nenhum ponto de $\ball{\varepsilon}{c}$ pode ser ligado a $a$
    (por quê?).
    Ou seja, $A \cap \complementset{C}$ é aberto.
    Como $a \in C$, $C$ é não-vazio.
    Assim, podemos concluir pela conexidade de $A$ que $C = A$.
  \end{example}
% TODO: Desenho.


  \begin{example}
    \label{ex:numero_de_rotacoes}

    Mais adiante
    (Proposição \ref{prop:caracterizacao_dos_conexos_nos_reais}),
    mostraremos que os intervalos são conexos na topologia usual de
    $\reals$.

    Para $t \in [0,1]$, considere uma família de curvas
    \begin{equation*}
      \function{\alpha_t}{\sphere}{\notnullcomplexes}.
    \end{equation*}
    Seja $\function{N}{[0,1]}{\integers}$ o ``número total de voltas''
    que a curva $\alpha_t$ faz em torno da origem.  Imagine que de
    alguma forma saibamos que $N(t)$ é contínua.  Então, o
    ``número total de voltas'' é o mesmo para todas as curvas
    $\alpha_t$.  De fato, o intervalo $[0,1]$ pode ser escrito como
    \begin{equation*}
      [0,1]
      =
      \bigcup_{n \in \integers}
      N^{-1}(n).
    \end{equation*}
    Como $\integers$ é discreto, todo subconjunto de $\integers$ é
    aberto.  Assim, pela continuidade de $N(t)$, os conjuntos
    $N^{-1}(n)$ são todos abertos (e disjuntos).  Pela conexidade do
    intervalo $[0,1]$, existe $n_0 \in \integers$ tal que
    \begin{equation*}
      [0,1]
      =
      N^{-1}(n_0).
    \end{equation*}
  \end{example}
% TODO: Desenho.


  \begin{example}
    \label{ex:racionais_totalmente_disconexo:particao}
    Nenhum subconjunto de $\rationals$ com mais de um elemento é
    conexo (na topologia induzida da topologia usual de $\reals$).
    De fato, seja $S \subset \rationals$, com $a,b \in S$ distintos.
    Escolha $c \in \reals \setminus \rationals$ entre $a$ e $b$.
    Então,
    \begin{equation*}
      S
      =
      \left( S \cap (-\infty,c) \right)
      \cup
      \left( S \cap (c,\infty) \right).
    \end{equation*}

    Note que esse exemplo é um caso particular do Exemplo
    \ref{ex:conexo_nos_reais_eh_intervalo}.  O que de fato fizemos,
    foi mostrar que $S$ não é um intervalo, escolhendo
    $c \not \in S$ entre $a$ e $b$.
    Por ter essa propriedade,
    de que todos os conjuntos com mais de um elemento são desconexos,
    dizemos que $\rationals$ é
    \emph{totalmente desconexo}.
  \end{example}
% TODO: Desenho.


  Como de costume, vamos ver maneiras diferentes para dizer se um
  conjunto é ou não conexo.
  Note que em um espaço topológico $X$,
  os conjuntos $\emptyset$ e $X$ são abertos e fechados ao mesmo tempo.
  Diremos que um conjunto $F \subset X$ é aberto e fechado não-trivial
  quando for diferente de $\emptyset$ e $X$.

  \begin{proposition}
    \label{prop:caracterizacao_de_conexidade}

    Seja $\topologicalspace{X}$ um espaço topológico.
    Então, são equivalentes:
    \begin{enumerate}[1.]   %   @comment@NO_customize_enumerate@
%    \begin{enumerate}      % @uncomment@NO_customize_enumerate@
      \item
        \label{it:prop:caracterizacao_de_conexidade:definicao}
        $X$ é conexo.

      \item
        \label{it:prop:caracterizacao_de_conexidade:particao}
        Não existem $U,V \in \topology{X}$ não-vazios e disjuntos tais
        que $X = U \cup V$.

      \item
        \label{it:prop:caracterizacao_de_conexidade:aberto_e_fechado}
        Não existe $A \subsetneq X$ aberto e fechado não trivial.
        Ou seja, os únicos subconjuntos de $X$ que são abertos e
        fechados ao mesmo tempo são $\emptyset$ e o próprio $X$.
    \end{enumerate}
    Se $Y \subset X$, então são equivalentes:
    \begin{enumerate}[a.]   %   @comment@NO_customize_enumerate@
%    \begin{enumerate}      % @uncomment@NO_customize_enumerate@
%TODO: numerar automaticamente.
      \item
        \label{it:prop:caracterizacao_de_conexidade:subconjunto:definicao}
        $Y$ é conexo.

      \item
        \label{it:prop:caracterizacao_de_conexidade:subconjunto:particao}
        Se $U,V \in \topology{X}$ são tais que
        $Y \cap U \cap V = \emptyset$ e $Y \subset U \cup V$, então ou
        $Y \subset U$, ou $Y \subset V$.

      \item
        \label{it:prop:caracterizacao_de_conexidade:subconjunto:aberto_e_fechado}
%TODO: exercício --- pode ser que $U \cap V$ nunca seja vazio!!
        Não existem um aberto $A$ e um fechado $F$ tais que
        \begin{equation*}
          \emptyset
          \subsetneq
          A \cap Y
          =
          F \cap Y
          \subsetneq
          Y.
        \end{equation*}
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      $
      \refitem{it:prop:caracterizacao_de_conexidade:definicao}
      \Leftrightarrow
      \refitem{it:prop:caracterizacao_de_conexidade:particao}.
      $
    }
    {
      É evidente que se $X$ for conexo, não podem existir
      $U$ e $V$ como os do item
      \refitem{it:prop:caracterizacao_de_conexidade:particao}.
      Por outro lado, se $X$ não for conexo, existe uma família de
      abertos $A_\lambda$ $(\lambda \in \Lambda)$, com
      $\cardinality{\Lambda} > 1$, não-vazios disjuntos, tais que
      \begin{equation*}
        X
        =
        \bigcup_{\lambda \in \Lambda}
        A_\lambda.
      \end{equation*}
      Agora é só separar $\Lambda$ em duas partes não triviais
      $\Lambda_1$ e $\Lambda_2$, e fazer
      \begin{equation*}
        U
        =
        \bigcup_{\lambda \in \Lambda_1}
        A_\lambda
        \quad \text{e} \quad
        V
        =
        \bigcup_{\lambda \in \Lambda_2}
        A_\lambda.
      \end{equation*}
    }

    \proofitem
    {
      $
      \refitem{it:prop:caracterizacao_de_conexidade:particao}
      \Leftrightarrow
      \refitem{it:prop:caracterizacao_de_conexidade:aberto_e_fechado}.
      $
    }
    {
      Basta fazer $A = U$ para obter um conjunto aberto e fechado a
      partir do item
      \refitem{it:prop:caracterizacao_de_conexidade:particao}.
      Ou então, fazer $U = A$ e $V = \complementset{A}$ para obter os
      conjuntos do item
      \refitem{it:prop:caracterizacao_de_conexidade:particao}
      a partir de um aberto e fechado $A$.
    }

    \proofitem
    {
      $
      \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:definicao}
      \Leftrightarrow
      \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:particao}
      \Leftrightarrow
      \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:aberto_e_fechado}.
      $
    }
    {
      É só usar o fato de que um aberto (um fechado) de $Y$ na
      topologia induzida é da forma $A \cap Y$,
      onde $A$ é um aberto (um fechado) de $X$.
    }
  \end{proof}

  \begin{corolary}
    Um espaço topológico $X$ é desconexo se, e somente se,
    todo $x \in X$ for tal que exista um conjunto
    $F \subsetneq X$ aberto e fechado, com $x \in F$.
  \end{corolary}

  \begin{proof}
    É evidente que se existe um tal $F$, então $X$ não é conexo.
    Por outro lado, se $X$ é desconexo, então existe um aberto
    e fechado não trivial $A$.
    Se $x \in A$, então basta tomar $F = A$.
    Se $x \not \in A$, então basta tomar $F = \complementset{A}$.
  \end{proof}


  \begin{example}
    \label{ex:racionais_totalmente_disconexo:aberto_fechado}
    Seja $S \subset \rationals$, com $a,b \in S$ distintos.
    Vamos mostrar novamente
    (veja o Exemplo
    \ref{ex:racionais_totalmente_disconexo:particao})
    que $S$ não é conexo.
    Tome $c \in \reals \setminus \rationals$ entre $a$ e $b$.
    Então,
    \begin{equation*}
      A
      =
      (c,\infty)
      \quad \text{e} \quad
      F
      =
      [c,\infty)
    \end{equation*}
    satisfazem
    \begin{equation*}
          \emptyset
          \subsetneq
          A \cap S
          =
          F \cap S
          \subsetneq
          S.
    \end{equation*}
    Contrariando o item
    \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:aberto_e_fechado}
    da Proposição
    \ref{prop:caracterizacao_de_conexidade}.
  \end{example}



  Como já é esperado, vamos mostrar que os subconjuntos conexos de
  $\reals$ são exatamente os intervalos.

  \begin{proposition}
    \label{prop:caracterizacao_dos_conexos_nos_reais}
    Um subconjunto de $\reals$ é conexo se, se somente se, for um
    intervalo.
  \end{proposition}

  \begin{proof}
    No Exemplo
    \ref{ex:conexo_nos_reais_eh_intervalo}, já mostramos que os
    conjuntos conexos são intervalos.  Vamos mostrar então que todos
    os intervalos são conexos.

    Suponha então que $D \subset \reals$ é um intervalo desconexo.
    Sejam $U$ e $V$ abertos como os do item
    \refitem{it:prop:caracterizacao_de_conexidade:subconjunto:particao}
    da Proposição \ref{prop:caracterizacao_de_conexidade}.
    Escolha $a \in U \cap D$, e $b \in V \cap D$.
    Podemos supor que $a < b$.
    Seja $I_a$ o maior intervalo aberto tal que
    \begin{equation*}
      a
      \in
      I_a
      \subset
      U.
    \end{equation*}
    Para ver que tal $I_a$ existe, basta tomar a união de todos os
    intervalos abertos que contém $a$ e estão contidos em $U$.
    Então, $I_a = (s,t)$, com $t \leq b$.
    Como $D$ é um intervalo, $t \in D$.
    Além disso, pela maximalidade de $I_a$, temos que
    $t \not \in U$.
    Assim,
    \begin{equation*}
      [a,t)
      \subset
      U \cap D
      \quad \text{e} \quad
      t \in V \cap D.
    \end{equation*}
    Mas como $V$ é vizinhança de $t$,
    e $t$ está no fecho de $[a,t)$,
    temos que
    $V \cap [a,t) \neq \emptyset$.
    Em particular,
    $V \cap U \cap D \neq \emptyset$.
    Contrariando a escolha de $U$ e $V$.
  \end{proof}
