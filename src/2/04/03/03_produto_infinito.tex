\subsection{Produto Infinito}

% TODO: Usar (X_\lambda, \tau_\lambda).
  Se temos um espaço topológico
  $\indexedtopologicalspace{X}{\lambda}$ para cada
  $\lambda \in \Lambda$,
  pela experiência com o produto de uma família finita de espaços
  topológicos podemos logo imaginar duas topologias que poderíamos
  chamar de \emph{topologia produto}. Uma delas, seria a topologia em
  $\prod_{\lambda \in \Lambda} X_\lambda$ gerada pela família dos
  conjuntos da forma $\prod_{\lambda \in \Lambda} A_\lambda$, onde
  $A_\lambda \in \indexedtopology{X}{\lambda}$. Se estivéssemos
  falando de espaços métricos, seria como definir a métrica do supremo
  (veja o Exemplo \ref{ex:produto_infinito:supremo}).
  Esta topologia é bastante geométrica e intuitiva.
  No entanto, não é esta a topologia que chamamos de
  \emph{topologia produto} da família
  $\indexedtopology{X}{\lambda}$ ($\lambda \in \Lambda$).
  A topologia produto é um pouco mais fraca, possui propriedades
  importantes (por exemplo, o Teorema \ref{theorem:produto_de_compactos})
  e é, em geral, mais fácil de se trabalhar.

  \begin{definition}
    Dada uma coleção qualquer de espaços topológicos
    $\indexedtopologicalspace{X}{\lambda}$,
    a \emph{topologia produto} é a menor topologia de
    $\prod_{\lambda \in \Lambda} X_\lambda$ onde as projeções em cada
    coordenada,
    \begin{equation*}
      \functionarray{\pi_\gamma}{\prod_{\lambda \in \Lambda} X_\gamma}{X_\gamma}
                                {(x_\lambda)_{\lambda \in \Lambda}}{x_\gamma},
    \end{equation*}
    são contínuas. O \emph{espaço (topológico) produto}
    $\prod_{\lambda \in \Lambda} \indexedtopologicalspace{X}{\lambda}$,
    é o conjunto $\prod_{\lambda \in \Lambda} X_\lambda$, dotado da
    topologia produto.
  \end{definition}

  Desta forma, a topologia produto é a topologia mais fraca tal que
  as projeções canônicas $\pi_\lambda$ são contínuas.
  Novamente,
  o leitor fica encarregado de enunciar e demonstrar uma proposição
  análoga a \ref{prop:equivalencias_topologia_produto_de_dois}.

  \begin{proposition}
    \label{prop:topologia_produto:continuidade}
    Dada uma coleção qualquer de espaços topológicos
    $\indexedtopologicalspace{X}{\lambda}$,
    seja $X$ o espaço produto munido da topologia produto.
    Então,
    $\function{f}{Y}{X}$
    é contínua se, e somente se,
    $\pi_\lambda \circ f$ é contínua para todo $\lambda \in \Lambda$.
  \end{proposition}

  \begin{proof}
    Basta aplicar
    a Proposição \ref{prop:topologia_inicial_com_diagramas:generalizacao}.
  \end{proof}


  \begin{example}
    \label{example:topologia_produto_com_normas}
    Considere o conjunto $X = [0,1]^\naturals$
    e as seguintes normas
    \begin{align*}
      \norm{x}_1
      &=
      \sup_{n \in \naturals}
      \abs{x_n}
      \\
      \norm{x}_2
      &=
      \sup_{n \in \naturals}
      \frac{1}{n+1}
      \abs{x_n}.
    \end{align*}
    A topologia da norma $\norm{\cdot}_2$
    é a topologia produto.
    De fato,
    nesta norma,
    a bola centrada em $a$, de raio $\varepsilon > 0$
    é igual a
    \begin{equation*}
      \prod_{n \in \naturals}
      \ball{(n+1)\varepsilon}{a_n}
      =
      \ball{\varepsilon}{a_0}
      \times \dotsb \times
      \ball{(N+1)\varepsilon}{a_N}
      \times
      \prod_{n \in \naturals}
      [0,1],
    \end{equation*}
    onde $N$ é tal que $(N+1)\varepsilon > 1$.

    A topologia da norma $\norm{\cdot}_1$
    é mais forte que a topologia produto.
    Neste caso,
    a bola centrada em $a$, de raio $\varepsilon > 0$
    é igual a
    \begin{equation*}
      [0,1]^\naturals
      \cap
      \prod_{n \in \naturals}
      \ball{\varepsilon}{a_n},
    \end{equation*}
    que pode não é aberto quando
    $\ball{\varepsilon}{a_n} \neq [0,1]$
    para uma quantidade infinita de índices $n$.
  \end{example}


  \begin{proposition}
    \label{prop:projecoes_sao_abertas_no_produto}
    Sejam $\indexedtopologicalspace{X}{\lambda}$ espaços topológicos,
    e $\pi_\gamma$ as projeções canônicas
    \begin{equation*}
      \function{\pi_\gamma}{\prod_{\lambda \in \Lambda} X_\lambda}{X_\gamma}.
    \end{equation*}
    Então, cada $\pi_\gamma$ é uma aplicação aberta.
  \end{proposition}

  \begin{proof}
    De fato, vamos mostrar que $\pi_\gamma$ é uma aplicação aberta
    quando o produto $\prod_{\lambda \in \Lambda} X_\lambda$ é dotado
    da topologia cuja base são os conjuntos da forma
    \begin{equation*}
      A
      =
      \prod_{\lambda \in \Lambda}
      A_\lambda,
    \end{equation*}
    onde $A_\lambda \in \indexedtopology{X}{\lambda}$.
    Note que esta topologia é mais forte que a topologia produto, e
    portanto, se $\pi_\gamma$ for uma aplicação aberta nesta
    topologia, será aberta na topologia produto.
    A imagem por $\pi_\gamma$ de $A$ é $A_\gamma$, que é aberto.
    Como a imagem de uniões é a união das imagens, e os
    abertos são uniões de elementos da base, segue que
    $\pi_\gamma$ é uma aplicação aberta.
  \end{proof}


  \begin{proposition}
    \label{prop:identificando_a_componente_do_produto_com_um_subespaco}
    Sejam $\indexedtopologicalspace{X}{\lambda}$ espaços topológicos,
    $X = \prod_{\lambda \in \Lambda} X_\lambda$ o espaço produto
    e $\pi_\gamma$ as projeções canônicas
    \begin{equation*}
      \function{\pi_\gamma}{X}{X_\gamma}.
    \end{equation*}
    Então, escolhendo $x = (x_\lambda) \in X$, para cada
    $\gamma \in \Lambda$, o conjunto da forma
    \begin{equation*}
      X(\gamma, x)
      =
      \bigcap_{\substack{\lambda \in \Lambda \\ \lambda \neq \gamma}}
      \pi_\lambda^{-1}( x_\lambda )
    \end{equation*}
    é homeomorfo a $X_\gamma$.
  \end{proposition}

  \begin{proof}
    A topologia de $X(\gamma, x)$ é gerada pela família
    \begin{equation*}
      X(\gamma, x) \cap \pi_\lambda^{-1}(A),
    \end{equation*}
    onde $\lambda \in \Lambda$, e $A$ é um aberto de $X_\lambda$.
    Mas exceto quando $\lambda = \gamma$, esses conjuntos ou são vazios,
    ou iguais a $X(\gamma, x)$.
    Assim, a topologia de $X(\gamma, x)$
    é gerada pela família
    \begin{equation*}
      X(\gamma, x) \cap \pi_\gamma^{-1}(A),
    \end{equation*}
    onde $A$ é um aberto de $X_\gamma$.
    Essa família é uma topologia.  Portanto, esses são exatamente os
    abertos de $X(\gamma, x)$.  Isso implica que a bijeção contínua
    $\pi_\gamma |_{X(\gamma, x)}$ é também uma aplicação aberta.
    De fato,
    \begin{equation*}
      \pi_\gamma
      (
        X(\gamma, x) \cap \pi_\gamma^{-1}(A)
      )
      =
      A.
    \end{equation*}
    Ou seja, $\pi_\gamma |_{X(\gamma, x)}$ é um homeomorfismo.
  \end{proof}


  \begin{example}[Representação Decimal]
    \label{ex:topologia_produto:representacao_decimal}
    \label{ex:topologia_produto:representacao_binaria}
    Considere o conjunto $D = \set{0,1,\dotsc,9}$
    dos dígitos de $0$ a $9$.
    O espaço $X = D^\naturals$
    pode ser utilizado para representar números
    reais no intervalo $[0,1]$.
    Enxergamos um elemento $(a_0, a_1, \dotsc) \in X$
    como sendo o número real cuja representação decimal
    é $0,a_0 a_1 a_2 \dotsb$.
    Formalmente, a representação é feita pela função
    \begin{equation*}
      \functionarray{f}{X}{[0,1]}
                       {(a_n)_{n \in \naturals}}{\sum_{n \in \naturals} a_n 10^{-n-1}}.
    \end{equation*}
    Note, no entanto, que a aplicação $f$ não é uma bijeção.
    Apesar de $f$ ser sobrejetiva,
    existem números que possuem duas representações distintas.
    Por exemplo,
    \begin{equation*}
      0,100000\dotsb
      =
      0,099999\dotsb.
    \end{equation*}

    Na topologia produto,
    uma sequência
    $a^n = (a_j^n)_{j \in \naturals}$
    converge para
    $a = (a_j)_{j \in \naturals}$,
    quando para todo $j \in \naturals$,
    $a_j^n \rightarrow a_j$.
    No entanto, como $D$ é discreto, isso significa que
    a partir de um certo $N = N_j$,
    \begin{equation*}
      n \geq N
      \Rightarrow
      a_j^n = a_j.
    \end{equation*}
    Em outras palavras,
    para todo $J$, existe $N$ tal que
    \begin{equation*}
      n \geq N
      \Rightarrow
      \forall j \leq J,\,
      a_j^n = a_j.
    \end{equation*}
    Assim, na topologia produto,
    $a^n$ converge para $a$ quando para todo $J$,
    a partir de um certo $n$,
    os $J$ primeiros termos de $a^n$
    coincidem com os $J$ primeiros termos de $a$.
    Em particular,
    a aplicação $f$ é contínua,
    pois se $a_n \in D^\naturals$ converge para $a \in D^\naturals$,
    então, para todo $M$, existe $N$
    tal que os primeiros $M$ termos de $a_n$ coincidem
    com os de $a$, para todo $n \geq N$.
    E isso implica que $f(a_n) \rightarrow f(a)$.

    É comum utilizarmos o espaço $\set{0,1}^\naturals$,
    ao invés de $D^\naturals$.
    Neste caso, trabalhamos com a
    \emph{representação binária} dos elementos de $[0,1]$.
  \end{example}


  \begin{example}[Espaço de Funções: convergência pontual]
    \label{ex:topologia_produto:convergencia_pontual}
    Sejam $X$ um conjunto qualquer,
    e $Y$ um espaço topológico.
    Podemos identificar as funções
    $\function{f}{X}{Y}$
    com os elementos do conjunto
    \begin{equation*}
      Y^X
      =
      \prod_{x \in X}
      Y_x,
    \end{equation*}
    onde $Y_x$ é uma cópia do espaço $Y$.
    Se dotarmos $Y^X$ da topologia produto,
    temos uma noção de convergência no
    espaço das funções de $X$ em $Y$.
    Nesta topologia,
    uma vizinhança $V$ de $\function{f}{X}{Y}$
    são as funções $\function{g}{X}{Y}$ que
    para um certo número finito de pontos (coordenadas)
    $x_1, \dotsc, x_n \in X$,
    $g(x_j)$ e $f(x_j)$ diferem ``pouco''.
    Para que $g$ pertença a esta vizinhança $V$,
    não faz diferença que valores $g$ assume em
    pontos diferentes de $x_1, \dotsc, x_n$.
    Para ser preciso,
    uma vizinhança $V$ é um conjunto que contém
    $\pi_{x_1}^{-1}(U_1) \cap \dotsb \cap \pi_{x_n}^{-1}(U_n)$
    para determinados $x_1, \dotsc, x_n$,
    e determinadas vizinhanças
    $U_j \in \neighbourhoods(x_j)$.

    Pela Proposição \ref{prop:convergencia_com_sub_bases},
    uma sequência $\function{f_n}{X}{Y}$ converge para
    $\function{f}{X}{Y}$ nesta topologia, exatamente quando,
    para todo $x \in X$,
    $f_n(x) \rightarrow f(x)$.
    Por isso, esta topologia no espaço das funções
    de $X$ a $Y$ é chamada de
    \emph{topologia da convergência pontual}, ou
    \emph{topologia da convergência ponto a ponto}.
  \end{example}
