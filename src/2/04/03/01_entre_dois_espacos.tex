\subsection{Entre dois Espaços}

  Inspirados pelo fato de que na métrica do máximo
  (Exemplo \ref{ex:metrica_do_maximo}) as bolas são na verdade
  quadrados, vamos definir o produto de dois espaços topológicos como
  sendo o espaço onde a base da topologia serão os ``retângulos''.

  \begin{definition}[Topologia do Produto de dois Espaços]
    \label{def:topologia_produto_de_dois}

    Sejam $\topologicalspace{X}$ e $\topologicalspace{Y}$ dois espaços
    topológicos.
    Definimos o \emph{espaço produto} como sendo o espaço topológico
    $\topologicalspace{X \times Y}$, onde $\topology{X \times Y}$ é
    gerada pelos conjuntos da forma $U \times V$,
    onde $U \in \topology{X}$ e $V \in \topology{Y}$.

    A topologia $\topology{X \times Y}$ é chamada de
    \emph{topologia produto}.
    Por um abuso de notação, escrevemos
    $\topology{X} \times \topology{Y}$ para designar a topologia
    produto.  Quando queremos ser menos ambíguos, escrevemos
    $\topologicalspace{X} \times \topologicalspace{Y}$.

% TODO: Figura.
  \end{definition}


  As \emph{projeções canônicas em $X$ e $Y$}
  \begin{equation*}
    \functionarray{\pi_X}{X \times Y}{X}{(x,y)}{x}
  \end{equation*}
  e
  \begin{equation*}
    \functionarray{\pi_Y}{X \times Y}{Y}{(x,y)}{y}
  \end{equation*}
  exercem papel fundamental no estudo das topologias produto.


  \begin{obs}
    \label{obs:topologia_produto_de_dois:sub-base}

    Na Definição \ref{def:topologia_produto_de_dois}, poderíamos ter
    dito que a topologia produto é gerada pelos conjuntos da forma $U
    \times Y$ e $X \times V$, onde $U \in \topology{X}$ e $V \in
    \topology{Y}$. No entanto, os conjuntos da forma $U \times V$,
    além de geradores são também uma base da topologia.
    Isso está de acordo com a analogia com a métrica do
    máximo, onde as bolas --- que são uma base para a topologia --- são
    os ``quadrados.'' No caso da topologia produto, não temos
    ``quadrados,'' temos ``retângulos.''
  \end{obs}

  \begin{proposition}
    \label{prop:equivalencias_topologia_produto_de_dois}

    Sejam $\topologicalspace{X}$ e $\topologicalspace{Y}$ dois espaços
    topológicos e $\topology{X \times Y}$ uma topologia
    qualquer no conjunto $X \times Y$. As seguintes afirmações são
    equivalentes.
    \begin{enumerate}
      \item
        \label{it:prop:equivalencias_topologia_produto_de_dois:definicao}
        A topologia $\topology{X \times Y}$ é a topologia produto. Ou
        seja,
        \begin{equation*}
          \topology{X \times Y} = \producttopology{X}{Y}.
        \end{equation*}

      \item
        \label{it:prop:equivalencias_topologia_produto_de_dois:base}
        Os conjuntos da forma $A \times B$,
        onde $A \in \topology{X}$ e $B \in \topology{Y}$ formam
        uma base de $\topology{X \times Y}$.

      \item
        \label{it:prop:equivalencias_topologia_produto_de_dois:sub-base}
        Os conjuntos da forma $A \times Y$ e $X \times B$,
        onde $A \in \topology{X}$ e $B \in \topology{Y}$ formam
        uma sub-base de $\topology{X \times Y}$.

      \item
        \label{it:prop:equivalencias_topologia_produto_de_dois:projecoes}
        A topologia $\topology{X \times Y}$ é a menor topologia em $X
        \times Y$ tal que as projeções canônicas são contínuas. Ou
        seja, é a topologia inicial induzida pelas projeções.

      \item
        \label{it:prop:equivalencias_topologia_produto_de_dois:composicao_com_projecao}
        Toda aplicação
        $\function{f}{\topologicalspace{Z}}{\topologicalspace{X \times Y}}$
        com domínio em um espaço topológico $Z$ qualquer é
        contínua se, e somente se, $\pi_X \circ f$ e $\pi_Y \circ f$
        forem contínuas.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      $\refitem{it:prop:equivalencias_topologia_produto_de_dois:definicao}
      \Leftrightarrow
      \refitem{it:prop:equivalencias_topologia_produto_de_dois:base}
      \Leftrightarrow
      \refitem{it:prop:equivalencias_topologia_produto_de_dois:sub-base}$
    }

    Imediato da definição de topologia produto e da Observação
    \ref{obs:base_gerada_por_sub-base}. Basta notar que
    $\left(X \times W\right) \cap \left(V \times Y\right) = V \times W$.


    \proofitem
    {
      $\refitem{it:prop:equivalencias_topologia_produto_de_dois:sub-base}
      \Leftrightarrow
      \refitem{it:prop:equivalencias_topologia_produto_de_dois:projecoes}$
    }

    As projeções são contínuas se, e somente se, para todo
    $U \in \topology{X}$ e $V \in \topology{Y}$,
    $U \times Y = \pi_X^{-1}(U) \in \topology{X \times Y}$
    e
    $X \times V = \pi_X^{-1}(V) \in \topology{X \times Y}$.
    Assim, a menor topologia em $X \times Y$ que torna as projeções
    contínuas é a topologia gerada pelos conjuntos da forma
    $U \times Y$ e $X \times V$, para
    $U \in \topology{X}$ e $V \in \topology{Y}$.


    \proofitem
    {
      $\refitem{it:prop:equivalencias_topologia_produto_de_dois:projecoes}
      \Leftrightarrow
      \refitem{it:prop:equivalencias_topologia_produto_de_dois:composicao_com_projecao}$
    }

    É um caso particular da Proposição
    \ref{prop:topologia_inicial_com_diagramas:generalizacao}.
  \end{proof}


  \begin{obs}
    Se para $\function{f}{Z}{X \times Y}$ escrevermos
    \begin{equation*}
      f(z) = (f_x(z), f_y(z)),
    \end{equation*}
    então $f_x = \pi_X \circ f$ e $f_y = \pi_Y \circ f$. O item
    \refitem{it:prop:equivalencias_topologia_produto_de_dois:composicao_com_projecao}
    da proposição diz que na topologia produto, $f$ é contínua se, e
    somente se, $f_x$ e $f_y$ são contínuas.
  \end{obs}


  \begin{obs}
    Seja $\function{f}{X \times Y}{Z}$. O item
    \refitem{it:prop:equivalencias_topologia_produto_de_dois:composicao_com_projecao}
    da Proposição
    \ref{prop:equivalencias_topologia_produto_de_dois} pode sugerir
    que a continuidade de $f$ no ponto $(a,b) \in X \times Y$ seja
    equivalente à continuidade de $f(a, \cdot)$ e $f(\cdot, b)$.
    No entanto, a continuidade dessas duas \emph{seções} de $f$ é uma
    condição mais fraca que a continuidade de $f$.

    Se $f(a, \cdot)$ é contínua em $b$, isso significa que se ``nos
    aproximarmos'' de $(a,b)$ na ``vertical'', o valor de $f$ se
    aproxima de $f(a,b)$. A continuidade de $f(\cdot, b)$ em $a$
    corresponde à continuidade de $f$ na ``horizontal''. No entanto, isso
    não garante nada sobre o comportamento de $f$ quando ``nos
    aproximamos'' de $(a,b)$ pela ``diagonal'',
    ou mesmo por um caminho em ``espiral''.
    Um exemplo concreto é a aplicação $\function{f}{\reals^2}{\reals}$,
    dada por
    \begin{equation*}
      f(x,y) =
      \left\{
        \begin{array}{lll}
          0                   &,& (x,y) = (0,0) \\
          \frac{xy}{x^2 + y^2}&,& (x,y) \neq (0,0)
        \end{array}
      \right..
    \end{equation*}
    Neste caso, $f(0,y) = f(x,0) = 0$. No entanto,
    $f\left(\frac{1}{n}, \frac{1}{n}\right) = \frac{1}{2}$.
% TODO: Figura.
  \end{obs}


  \begin{obs}
    Para a topologia produto, também vale que
    \begin{equation}
      \label{obs:eq:topologia_produto_com_sequencias}
      (x_n, y_n) \rightarrow (x,y)
      \Leftrightarrow
      x_n \rightarrow x,\,
      y_n \rightarrow y.
    \end{equation}
    No entanto, como as sequências convergentes não determinam a
    topologia, não se pode afirmar que a condição acima determina a
    topologia produto.

    Se ao invés de sequências, utilizássemos o conceito de
    \emph{redes} --- desenvolvido no Capítulo
    \ref{ch:topologia_com_redes} ---, a relação da equação
    \ref{obs:eq:topologia_produto_com_sequencias} caracterizaria
    totalmente a topologia. Com redes no lugar de sequências,
    $f$ será contínua se, e somente se,
    para toda rede $z_\lambda \rightarrow z$, tivermos
    \begin{equation*}
      \begin{array}{lcr}
        f_x(z_\lambda) \rightarrow f_x(z)
          &\text{ e }&
        f_y(z_\lambda) \rightarrow f_y(z).
      \end{array}
    \end{equation*}
  \end{obs}
