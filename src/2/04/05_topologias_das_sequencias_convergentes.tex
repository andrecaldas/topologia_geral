\section{Topologias das Sequências Convergentes}

  Esta seção pode (e deve!) ser omitida.
  É apenas uma divagação sobre convergência de sequências.
  Ao fazer analogia com os espaços métricos,
  o estudante frequentemente se pergunta porque é que nem sempre
  se pode usar sequências para determinar as propriedades topológicas
  de um espaço.

  Se temos um conjunto $X$ e uma topologia $\topology{X}$ sobre $X$,
  sabemos exatamente quais são e quais não são as sequências
  convergentes. No entanto, conhecer as sequências convergentes não
  garante que conheçamos a topologia. De fato, duas topologias
  distintas podem ter exatamente as mesmas sequências convergentes,
  convergindo para os mesmos limites.

  \begin{example}[Topologia Coenumerável]
    \label{example:topologia_das_sequencias:coenumeravel}
    Seja $X$ um conjunto não enumerável, e 
    \begin{equation*}
      \indexedtopology{X}{1} = \powerset{X}.
    \end{equation*}
    As sequências convergentes em $\indexedtopology{X}{1}$ são aquelas
    que a partir de um certo índice se tornam constantes. Ou seja, as
    sequências constantes a menos de um número finito de termos.

    Considere agora $\indexedtopology{X}{2}$ dada por
    \begin{equation*}
      \indexedtopology{X}{2} =
        \set{\emptyset}
        \cup
        \setsuchthat{A \subset X}{\complementset{A} \text{ é enumerável}}.
    \end{equation*}
    Fica como exercício mostrar que $\indexedtopology{X}{2}$ é de fato
    uma topologia.
    Evidentemente que as sequências constantes a menos de um número
    finito de termos convergem nesta e em qualquer outra
    topologia. Considere então a sequência $x_1, x_2, \cdots$. Suponha
    que
    \begin{equation*}
      x_n \xrightarrow{\indexedtopology{X}{2}} x.
    \end{equation*}
    O conjunto
    \begin{equation*}
      V = \complementset{\setsuchthat{x_n}{x_n \neq x}}
    \end{equation*}
    é vizinhança aberta de $x$ em $\indexedtopology{X}{2}$. A
    convergência de $x_n$ implica que para um certo $N$,
    \begin{equation*}
      n > N \Rightarrow x_n \in V.
    \end{equation*}
    Mas $x_n$ só está em $V$ se $x_n = x$. Ou seja, $x_n$ é constante
    a menos, possivelmente, de $x_1, \cdots, x_N$.

    Pergunta: porque sabemos que
    $\indexedtopology{X}{1} \neq \indexedtopology{X}{2}$?
  \end{example}

  Dado um espaço topológico $\topologicalspace{X}$, podemos indagar
  se existe uma topologia $\indexedtopology{X}{m}$ que é a menor onde
  as sequências convergentes são as mesmas que de
  $\topology{X}$. Também podemos nos perguntar se não existe a maior
  topologia $\indexedtopology{X}{M}$ com esta mesma propriedade.
  Se existir, $\indexedtopology{X}{m}$ será a
  interseção da família de todas as topologias
  $\indexedtopology{X}{\lambda}$ ($\lambda \in \Lambda$)
  tais que para todo $x \in X$,
  \begin{equation*}
    x_n \xrightarrow{\indexedtopology{X}{\lambda}} x
    \Leftrightarrow
    x_n \xrightarrow{\topology{X}} x.
  \end{equation*}
  Vamos definir $\indexedtopology{X}{m}$ como sendo
  \begin{equation*}
    \indexedtopology{X}{m}
    =
    \bigcap_{\lambda \in \Lambda}
    \indexedtopology{X}{\lambda}.
  \end{equation*}
  Evidentemente que como
  $\indexedtopology{X}{m} \subset \topology{X}$, então toda vizinhança
  de $x$ em $\indexedtopology{X}{m}$ também é uma vizinhança em
  $\topology{X}$. Portanto,
  \begin{equation*}
    x_n \xrightarrow{\topology{X}} x
    \Rightarrow
    x_n \xrightarrow{\indexedtopology{X}{m}} x.
  \end{equation*}
  No entanto, a implicação contrária pode não ser verdadeira.
  Ou seja, é possível que, para a topologia
  $\indexedtopology{X}{m}$,
  não tenhamos
  \begin{equation*}
    x_n \xrightarrow{\indexedtopology{X}{m}} x
    \Rightarrow
    x_n \xrightarrow{\topology{X}} x.
  \end{equation*}
  Para um exemplo, veja:\\
  {\tiny\url{http://math.stackexchange.com/questions/395980/topology-for-convergent-sequences}}
  Para compreender o exemplo é necessário conhecer
  um conceito mais avançado chamado \emph{``ultrafiltro''}.
  Para nossos propósitos, basta dizer que o exemplo
  se trata de uma família de topologias $\topology{\beta}$
  no conjunto $\naturals \cup \set{\infty}$ tais que
  $x_n \xrightarrow{\topology{\beta}} \infty$
  equivale à existência de $N$ tal que
  \begin{equation*}
    n \geq N
    \Rightarrow
    x_n = \infty.
  \end{equation*}
  No entanto,
  em $\tau = \bigcap \topology{\beta}$,
  $x_n \xrightarrow{\tau} \infty$
  equivale à existência de $N$ tal que
  \begin{equation*}
    m > n \geq N
    \Rightarrow
    x_m \neq x_n
    \text{ ou }
    x_m = x_n = \infty.
  \end{equation*}
  Ou seja,
  $x_n \xrightarrow{\tau} \infty$
  sempre que $x_n$ ``sai'' de qualquer
  subconjunto finito de $\naturals$.

  Por outro lado, vamos definir a topologia
  \begin{equation*}
    \indexedtopology{X}{M} =
      \setsuchthat{V \subset X}
                  {x_n \xrightarrow{\topology{X}} x \in V
                   \Rightarrow
                   \exists N, \forall n \geq N, x_n \in V}.
  \end{equation*}
  É evidente que $\indexedtopology{X}{M}$ é uma topologia e é mais
  forte que $\topology{X}$.
  O leitor é convidado a demonstrar essa afirmação.

  Como $\topology{X} \subset \indexedtopology{X}{M}$, sabemos que
  \begin{equation*}
    x_n \xrightarrow{\indexedtopology{X}{M}} x
    \Rightarrow
    x_n \xrightarrow{\topology{X}} x.
  \end{equation*}
  Por outro lado, pela definição de $\indexedtopology{X}{M}$, sabemos
  que
  \begin{equation}
    \label{equation:topologia_da_intersecao}
    x_n \xrightarrow{\topology{X}} x
    \Rightarrow
    x_n \xrightarrow{\indexedtopology{X}{M}} x.
  \end{equation}
  Ou seja, sempre existe a topologia mais forte determinada pela
  família de sequências convergentes de uma toplogia $\topology{X}$
  dada.
  Em outras palavras,
  denotando por $\family{F}$ a família de topologias
  \begin{equation*}
    \family{F} = \setsuchthat{\indexedtopology{X}{\lambda}}
                             {\lambda \in \Lambda},
  \end{equation*}
  temos que
  $\indexedtopology{X}{M} = \bigvee \family{F}$
  é tal que
  $\indexedtopology{X}{M} \in \family{F}$,
  mas pode ocorrer que
  $\indexedtopology{X}{m} = \bigwedge \family{F}$
  não pertença a $\family{F}$.

% TODO: exercício -- descreva
% \neighbourhoods{x}_{\indexedtopology{X}{M}}. Mostre que esta família
% satisfaz os itens de \ref{prop:propriedades_vizinhanca}.

% TODO: exercício -- que propriedades as famílias de sequências x_n ->
% x devem satisfazer para que $\indexedtopology{X}{M}$ seja uma
% topologia?

  {\input{2/04/05/exercicios}}
