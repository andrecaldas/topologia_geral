\subsection{Relação de Equivalência}

  Estamos preocupados em ``particionar'' um conjunto $X$ e pensar no
  conjunto $\tilde{X}$ formado pelos elementos da partição escolhida.
  Uma maneira muito comum de se escolher uma partição de
  $X$ é através de uma \emph{relação de equivalência}.
  Não vamos entrar em detalhes quanto às propriedades das relações de
  equivalência, mas de fato,
  definir uma \emph{relação de equivalência} no conjunto $X$
  equivale a particioná-lo.

  \begin{definition}
    Uma \emph{relação} $\sim$ em um conjunto $X$ é simplesmente um
    subconjunto de $X^2$. Usualmente escrevemos
    \begin{equation*}
      a \sim b
    \end{equation*}
    ao invés de dizer que $(a,b)$ pertence à relação $\sim$.
  \end{definition}

  \begin{definition}
    Uma relação (binária) $\sim$ definida em um conjunto $X$ é uma
    \emph{relação de equivalência} se satisfizer,
    para todo $a,b,c \in X$,
    \begin{enumerate}
      \item
        $a \sim a$.

      \item
        $a \sim b \Rightarrow b \sim a$.

      \item
        $a \sim b \text{ e } b \sim c \Rightarrow a \sim c$.
    \end{enumerate}
  \end{definition}

  Definir uma relação de equivalência em $X$ é equivalente a
  particioná-lo, pois dada uma relação de equivalência podemos
  particionar $X$ em \emph{classes de equivalência}, ou seja, nos
  conjuntos
  \begin{equation*}
    [a] = \setsuchthat{x \in X}{x \sim a}.
  \end{equation*}
  Do mesmo modo, dada uma partição $A_\lambda$ de $X$, podemos definir
  a relação de equivalência
  \begin{equation*}
    a \sim b \Leftrightarrow \exists A_\lambda \text{ tal que } a,b \in A_\lambda.
  \end{equation*}

  \begin{notation}
    Dada uma relação de equivalência $\sim$ em $X$, denotamos por
    $X/{\sim}$ o conjunto das classes de equivalência de $\sim$. A
    projeção natural de $X$ em $X/{\sim}$ é a aplicação dada por
    \begin{equation*}
      \functionarray{\pi}{X}{X/{\sim}}{x}{[x]}.
    \end{equation*}
  \end{notation}

  Ao identificarmos, por exemplo, os pontos $0$ e $1$ do intervalo
  $[0,1]$ para formar o círculo, chamando esse ponto identificado de $p$,
  a topologia que esperamos deve ser tal que,
  uma sequência converge para o ponto $p$ sempre que se aproximar
  do conjunto $\set{0,1}$.
  Dessa forma,
  uma sequência que se aproxima de $p = \set{0,1}$ é uma sequência
  que se aproxima de $0$,
  ou de $1$,
  ou que ``oscila'' entre esses dois pontos.
  O que queremos, é que cada vizinhança de $p$
  seja união de uma vizinhança de $0$ e uma vizinhança de $1$.
  Ou seja, queremos que a projeção canônica,
  $\function{\pi}{[0,1]}{(0,1)\cup \set{p}}$
  seja contínua.
  Mas também, não esperamos que uma sequência que nem mesmo se
  aproxima de $0$ ou de $1$ seja considerada uma sequência que
  se aproxima de $p$.
  Queremos que a topologia seja a mais forte possível com esta
  propriedade.

  \begin{definition}[Topologia Quociente]
    Quando $X$ é um espaço topológico e $\sim$ uma relação de
    equivalência definida sobre $X$, a \emph{topologia quociente} em
    $X/{\sim}$ é a topologia final induzida pela projeção natural.
  \end{definition}

  Seja $X$ um conjunto e $\sim$ uma relação de equivalência em
  $X$. Suponha que a aplicação
  \begin{equation*}
    \function{f}{X}{Y}
  \end{equation*}
  seja tal que $a \sim b \Rightarrow f(a) = f(b)$. Neste caso, podemos
  definir
  \begin{equation*}
    \functionarray{\tilde{f}}{X/{\sim}}{Y}{[x]}{f(x)}.
  \end{equation*}
  Note que o seguinte diagrama
  \begin{equation*}
    \xymatrix{
      X        \ar[d]_{\pi} \ar[dr]^f   \\
      X/{\sim} \ar[r]_{\tilde{f}}       &Y
    }
  \end{equation*}
  é comutativo. Assim, pela Proposição
  \ref{prop:topologia_final_com_diagramas}, sabemos que a topologia
  quociente fará com que uma eventual continuidade da aplicação $f$
  seja equivalente à continuidade de $\tilde{f}$.

  Construções com ``quocientes'' são muito comuns, por exemplo, em
  álgebra, onde quocientamos grupos por subgrupos, anéis por
  ideais, espaços vetoriais por subespaços vetoriais e assim por
  diante. Em muitos casos, essas estruturas algébricas são também
  dotadas de topologia. Mais a diante, no Capítulo
  \ref{ch:grupos_topologicos}, por exemplo, veremos como o estudo da
  topologia pode facilitar a compreensão desses espaços.
