\subsection{Diagramas Comutativos}

  Quando temos uma família de aplicações cada uma com seu domínio e
  seu contra-domínio, podemos representá-las em um diagrama.
  Por exemplo,
  \begin{equation*}
    \xymatrix{
      X           \ar[d]_{\pi} \ar[r]^f    &Y         \ar[r]^h    &Z \\
      \tilde{X}   \ar[r]_{\tilde{f}}       &\tilde{Y} \ar[u]^g \ar[ur]_{\tilde{h}}
    }.
  \end{equation*}
  Dizemos que o diagrama \emph{comuta} quando ``diferentes caminhos''
  correspondem à mesma aplicação. Se, por exemplo, o diagrama anterior
  comuta, então sabemos que $g \circ \tilde{f} \circ \pi = f$; ou
  então, $\tilde{h} \circ \tilde{f} \circ \pi = h \circ f$.

  Vamos caracterizar as topologias inicial e final utilizando
  diagramas comutativos.

  \begin{proposition}
    \label{prop:topologia_inicial_com_diagramas}

    Dada a aplicação $\function{f}{X}{\topologicalspace{Y}}$, a
    topologia inicial $\initialtopology{f}$ é a \emph{única} topologia
    $\topology{X}$ que torna $f$ contínua e é tal que para todo
    diagrama comutativo 
    \begin{equation*}
      \xymatrix{
        \topologicalspace{Z}   \ar[r]^{g} \ar[dr]_{\tilde{g}}   &\topologicalspace{X} \ar[d]^f \\
                                                                &\topologicalspace{Y}
      },
    \end{equation*}
    a continuidade de $g$ é equivalente à continuidade de
    $\tilde{g}$.
  \end{proposition}

  \begin{proof}
    Primeiro vamos mostrar que para $\topology{X} =
    \initialtopology{f}$, a continuidade de $g$ é equivalente à de
    $\tilde{g}$. Primeiramente, por definição, $\initialtopology{f}$
    torna $f$ contínua. Se $g$ for contínua, então $\tilde{g}$ é
    contínua por ser a composição de duas aplicações contínuas. Por
    outro lado, supondo que $\tilde{g}$ é contínua, dado
    $A \in \initialtopology{f}$, $A$ é da forma $f^{-1}(U)$, com
    $U \in \topology{Y}$. Assim, 
    \begin{equation*}
      g^{-1}(A) = g^{-1}\left(f^{-1}(U)\right) = \tilde{g}^{-1}(U).
    \end{equation*}
    Pela continuidade de $\tilde{g}$, este conjunto é aberto de
    $Z$. Ou seja, a imagem inversa de um aberto de $X$ é aberto de
    $Z$. Portanto, $g$ é contínua.

    Falta mostrar que só existe uma topologia que satisfaz a condição
    da proposição. Suponha que $\topology{X}$ e $\topology{X}'$ ambas
    satisfaçam a condição do enunciado.
    Considere a seguinte o seguinte diagrama comutativo.
    \begin{equation*}
      \xymatrix{
        \basespaceandtopology{X}{\topology{X}'}
        \ar[r]^{\identity} \ar[dr]_{f}
        &\basespaceandtopology{X}{\topology{X}} \ar[d]^{f}
        \\
        &\topologicalspace{Y}
      }.
    \end{equation*}
    Neste caso, como $\topology{X}$ satisfaz as condições do enunciado
    e $f$ é contínua na topologia $\topology{X}'$, temos que
    $\function{\identity}{\basespaceandtopology{X}{\topology{X}'}}{\basespaceandtopology{X}{\topology{X}}}$
    é contínua.  Mas a continuidade da identidade é equivalente a
    \begin{equation*}
      \topology{X}
      \subset
      \topology{X}'.
    \end{equation*}
    Invertendo os papeis de $\topology{X}$ e $\topology{X}'$, obtemos
    a unicidade:
    \begin{equation*}
      \topology{X}' = \topology{X}.
    \end{equation*}
  \end{proof}


  \begin{obs}
    Muitos autores começariam a demonstração anterior pela unicidade.
    Na demonstração da unicidade,
    não foi preciso utilizar a existência!
    Não foi preciso saber como é a ``cara'' da topologia $\initialtopology{f}$.
    Optamos por considerar duas topologias quaisquer que satisfazem
    as condições impostas e demonstrar que são necessariamente iguais.
    Concluindo que \emph{se existe uma}, então é única.
    Daqui por diante, quando possível,
    começaremos esse tipo de demonstração pela unicidade.
  \end{obs}

% TODO: exercício -- generalizar a proposição
% \ref{prop:topologia_inicial_com_diagramas}.


  A Proposição \ref{prop:topologia_inicial_com_diagramas} admite a
  seguinte generalização.

  \begin{proposition}
    \label{prop:topologia_inicial_com_diagramas:generalizacao}

    Considere a família de aplicações
    $\function{f_\lambda}{X}{\topologicalspace{Y_\lambda}}$.
    A topologia inicial em $X$ dada pela família $f_\lambda$ é a única
    topologia $\topology{X}$ onde todas as $f_\lambda$ são contínuas,
    e para toda aplicação
    \begin{equation*}
      \function{g}{\topologicalspace{Z}}{\topologicalspace{X}}
    \end{equation*}
    vale que
    \begin{equation*}
      \text{$g$ é contínua}
      \Leftrightarrow
      \forall \lambda \in \Lambda,\,
      \text{$f_\lambda \circ g$ é contínua}.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      Unicidade.
    }
    {
      Seja $\topology{X}$ uma topologia que satisfaz as condições do
      enunciado, e $\topology{X}'$ uma topologia onde as $f_\lambda$
      são contínuas.
      Considere a seguinte família de diagramas comutativos indexada
      por $\lambda \in \Lambda$.
      \begin{equation*}
        \xymatrix{
          \basespaceandtopology{X}{\topology{X}'}
          \ar[r]^{\identity} \ar[dr]_{f_\lambda}
          &\basespaceandtopology{X}{\topology{X}} \ar[d]^{f_\lambda}
          \\
          &\topologicalspace{Y}
        }.
      \end{equation*}
      Como $\topology{X}$ satisfaz as condições da proposição e
      $f_\lambda$ é contínua na topologia $\topology{X}'$, então
      $\function{\identity}{\basespaceandtopology{X}{\topology{X}'}}{\basespaceandtopology{X}{\topology{X}}}$
      é contínua.  Ou seja,
      \begin{equation*}
        \topology{X}
        \subset
        \topology{X}'.
      \end{equation*}
      Portanto, para uma topologia $\topology{X}'$ que também satisfaz
      as condições da proposição, se invertermos os papeis de
      $\topology{X}$ e $\topology{X}'$, chegaremos à igualdade.
    }


    \proofitem
    {
      A topologia inicial possui as propriedades enunciadas.
    }
    {
      Suponha que $\topology{X}$ seja a topologia inicial.
      Ou seja, a topologia gerada pela família
      \begin{equation*}
        \family{F}
        =
        \setsuchthat{f_\lambda^{-1}(U)}{\lambda \in \Lambda,\, U \in \topology{Y_\lambda}}.
      \end{equation*}
      Pela Proposição
      \ref{prop:continuidade_com_sub-base},
      \begin{align*}
        \text{$g$ é contínua}
        &\Leftrightarrow
        g^{-1}\left( \family{F} \right)
        \subset
        \topology{Z}
        \\&\Leftrightarrow
        \forall \lambda \in \Lambda,\,
        g^{-1}\left( f_\lambda^{-1}(\topology{Y_\lambda}) \right)
        \subset
        \topology{Z}
        \\&\Leftrightarrow
        \forall \lambda \in \Lambda,\,
        \text{$f_\lambda \circ g$ é contínua}.
      \end{align*}
    }
  \end{proof}

  A topologia final tem forma semelhante à inicial quando utilizamos
  diagramas comutativos.

  \begin{proposition}
    \label{prop:topologia_final_com_diagramas}

    Dada a aplicação $\function{f}{\topologicalspace{X}}{Y}$, a
    topologia final $\finaltopology{f}$ é a \emph{única} topologia,
    $\topology{Y}$ que torna $f$ contínua e é tal que para todo
    diagrama comutativo 
    \begin{equation*}
      \xymatrix{
        \topologicalspace{X}   \ar[d]_{f} \ar[dr]^{\tilde{g}} \\
        \topologicalspace{Y}   \ar[r]^{g}                     &\topologicalspace{Z}
      },
    \end{equation*}
    a continuidade de $g$ é equivalente à continuidade de
    $\tilde{g}$.
  \end{proposition}

  \begin{proof}
    Para mostrar a unicidade, considere as topologias $\topology{Y}$ e
    $\topology{Y}'$, e suponha que ambas possuem as propriedades do
    enunciado.  Então, o diagrama
    \begin{equation*}
      \xymatrix{
        \topologicalspace{X}   \ar[d]_{f}         \ar[dr]^{f} \\
        \topologicalspace{Y}   \ar[r]^{\identity}             &\topologicalspace{Z}
      }
    \end{equation*}
    comuta, e o fato de $f$ ser contínua em ambas as topologias
    implica que $\identity$ é um homeomorfismo.
    Ou seja,
    \begin{equation*}
      \topology{Y}
      =
      \topology{Y}'.
    \end{equation*}

    Vamos então mostrar que
    $\topology{Y} = \finaltopology{f}$
    satisfaz as condições da proposição.
    A parte não trivial é mostrar que a
    continuidade de $\tilde{g}$ implica na continuidade de $g$.
    Seja $A \in \topology{Z}$,
    então
    \begin{equation*}
      \tilde{g}^{-1}(A) = (g \circ f)^{-1}(A) = f^{-1}\left(g^{-1}(A)\right)
    \end{equation*}
    é aberto de $X$.
    Pela definição de topologia final, temos que
    $g^{-1}(A)$ é aberto de $\finaltopology{f}$.
    Ou seja, $g$ é contínua.
  \end{proof}


% TODO: exercício -- argumente de diferentes formas que se
% considerarmos a topologia inicial em $X$ induzida por
% $\function{f}{X}{\topologicalspace{Y}}$, ou considerarmos a
% topologia inicial induzida por
% $\function{f}{X}{\inducedtopologicalspace{Y}{f(X)}}$, obteremos a
% mesma topologia.
