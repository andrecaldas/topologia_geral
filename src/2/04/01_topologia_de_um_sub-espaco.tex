\section{Topologia de um Sub-Espaço}
  \label{sec:topologia_induzida}

  Se temos um espaço topológico $\topologicalspace{X}$ e um subconjunto $Z
  \subset X$, então parece natural pensarmos na restrição da topologia
  $\topology{X}$ ao subconjunto $Z$. Mas isso é realmente natural?
  Vamos fazer algumas considerações.

  Imagine que $x_n \in Z$ é uma sequência
  (o ideal seria falar de ``redes''
  ---
  veja o Capítulo \ref{ch:redes})
  que na topologia $\topology{X}$ converge para $x \in Z$.
  Neste caso, se fôssemos ``induzir'' em $Z$
  uma topologia $\topology{Z}$ a partir de $\topology{X}$, sua topologia deveria ser
  tal que para $x_n, x \in Z$,
  \begin{equation*}
    x_n \xrightarrow{\topology{X}} x \Leftrightarrow x_n \xrightarrow{\topology{Z}} x.
  \end{equation*}
  Pensando em termos da operação de fecho, para um
  conjunto $B \subset Z$, o conjunto dos pontos de $Z$ que estão
  ``próximos'' --- ou seja, no fecho --- de $B$ são, intuitivamente, os
  pontos de $Z$ que estão em $\closureop[\topology{X}]{B}$. Ou seja, deveríamos
  ter que
  \begin{equation*}
    \closureop[\topology{Z}]{B} = Z \cap \closureop[\topology{X}]{B}.
  \end{equation*}
  Vendo do ponto de vista da continuidade, se
  $\function{f}{\topologicalspace{X}}{\topologicalspace{Y}}$ é uma aplicação qualquer, e
  $W \subset Y$ é tal que $f(X) \subset W$, então podemos pensar na
  aplicação
  \begin{equation*}
    \functionarray{\bar{f}}{X}{W}{x}{f(x)},
  \end{equation*}
  e esperar que possamos induzir em $W$ uma topologia tal que $f$ é
  contínua se, e somente se, $\bar{f}$ o for. Poderíamos também,
  dado $Z \subset X$, pensar na continuidade de $f|_Z$.
  Claro que esperaríamos que se $f$ é contínua em $z \in Z$,
  então, na topologia induzida,
  $f|_Z$ deve ser contínua em $z$.
  Ou seja,
  se $V$ é vizinhança de $z$ em $\topology{X}$,
  $Z \cap V$ deve ser vizinhança de $z$ em $\topology{Z}$.
  Dentre essas considerações, o menos natural é pensar
  em termos de abertos. E é por isso que este livro é ``de vários
  ângulos''. :-)

  Entretanto, como nossa definição de espaço topológico é em termos de
  abertos, com as ferramentas que temos até o momento, será mais fácil
  definir a topologia de um subconjunto em termos de abertos.
  Felizmente, a definição com abertos é extremamente simples.

  \begin{definition}[Topologia Induzida em um Subconjunto]
    \label{def:topologia_de_um_subespaco}

    Seja $\topologicalspace{X}$ um espaço topológico e $Z \subset X$ um
    subconjunto de $X$ qualquer. Então, o conjunto
    \begin{equation*}
      Z \cap \topology{X} = \setsuchthat{Z \cap A}{A \in \topology{X}}
    \end{equation*}
    é a \emph{topologia induzida por $\topology{X}$ em $Z$}.
  \end{definition}

  \begin{notation}
    Na Definição \ref{def:topologia_de_um_subespaco}, a notação
    $Z \cap \topology{X}$ não é a interseção de $\topology{X}$ e $Z$, mas a família
    formada pela interseção dos elementos de $\topology{X}$ com o conjunto
    $Z$. Este abuso de notação, em geral, não deve causar problemas
    de entendimento e será usado sem ressalvas.
  \end{notation}


  Vamos então verificar que a definição de topologia induzida em um
  subconjunto satisfaz as propriedades discutidas no início do
  capítulo.

  \begin{proposition}
    \label{prop:propriedades_da_topologia_induzida}

    Seja $\topologicalspace{X}$ um espaço topológico e $Z \subset X$ um
    subconjunto qualquer de $X$. Então a topologia induzida em $Z$,
    $\topology{Z} = Z \cap \topology{X}$, satisfaz:
    \begin{enumerate}
      \item
        \label{it:prop:propriedades_da_topologia_induzida:forma_do_aberto}
        Todo aberto $A \in \topology{Z}$ da topologia induzida é da forma
        $A = Z \cap A'$ para algum aberto $A' \in \topology{X}$ da topologia
        de $X$.

      \item
        \label{it:prop:propriedades_da_topologia_induzida:forma_do_fechado}
        Todo fechado $F \in \topology{Z}$ da topologia induzida é da forma
        $F = Z \cap F'$ para algum fechado $F' \in \topology{X}$ da topologia
        de $X$.

      \item
        \label{it:prop:propriedades_da_topologia_induzida:forma_da_vizinhanca}
        Se $x \in Z$,
        então
        \begin{equation*}
          \neighbourhoods[\topology{Z}]{x}
          =
          Z
          \cap
          \neighbourhoods[\topology{X}]{x}.
        \end{equation*}

      \item
        \label{it:prop:propriedades_da_topologia_induzida:fecho}
        Se $B \subset Z$, então
        \begin{equation*}
          \closureop[\topology{Z}]{B} = Z \cap \closureop[\topology{X}]{B}.
        \end{equation*}

      \item
        \label{it:prop:propriedades_da_topologia_induzida:convergencia}
        Para $x_n, x \in Z$, então
        \begin{equation*}
          x_n \xrightarrow{\topology{X}} x
          \Leftrightarrow
          x_n \xrightarrow{\topology{Z}} x.
        \end{equation*}

      \item
        \label{it:prop:propriedades_da_topologia_induzida:continuidade_no_contradominio}
        Se $\topologicalspace{Y}$ é um espaço topológico qualquer e
        $\function{f}{\topologicalspace{Y}}{\topologicalspace{X}}$ é uma aplicação tal que
        $f(Y) \subset Z$, então
        \begin{equation*}
          \functionarray{\bar{f}}{\topologicalspace{Y}}{\topologicalspace{Z}}{y}{f(y)}
        \end{equation*}
        é contínua se, e somente se, $f$ é contínua.
        (Note que a diferença entre as aplicações $f$ e
        $\bar{f}$ é apenas o contra-domínio das aplicações)

      \item
        \label{it:prop:propriedades_da_topologia_induzida:continuidade_no_dominio}
        Se $\topologicalspace{Y}$ é um espaço topológico qualquer e
        $\function{g}{\topologicalspace{X}}{\topologicalspace{Y}}$ é uma aplicação
        contínua, então
        \begin{equation*}
          \functionarray{g|_Z}{\topologicalspace{Z}}{\topologicalspace{Y}}{z}{g(z)}
        \end{equation*}
        é contínua.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      Itens
      \refitem{it:prop:propriedades_da_topologia_induzida:forma_do_aberto}
      e
      \refitem{it:prop:propriedades_da_topologia_induzida:forma_do_fechado}.
    }

    Imediato da definição de $\topology{Z}$.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_da_topologia_induzida:forma_da_vizinhanca}.
    }

    Imediato do item
    \refitem{it:prop:propriedades_da_topologia_induzida:forma_do_aberto}.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_da_topologia_induzida:fecho}.
    }

    Este fato pode ser demonstrado de várias formas --- de vários
    ângulos ;-). Vamos utilizar a Proposição
    \ref{prop:fecho_com_fechados}, mas o leitor é motivado a
    demonstrar diretamente da definição de fecho
    (Definição \ref{def:fecho}).

    Pela Proposição \ref{prop:fecho_com_fechados}
    e pelo item \refitem{it:prop:propriedades_da_topologia_induzida:forma_do_fechado},
    \begin{align*}
      \closureop[\topology{Z}]{B}
      &=
      \bigcap_{\substack{\text{$F$: fechado de $\topology{Z}$}\\B \subset F}} F
      \\
      &=
      \bigcap_{\substack{\text{$F$: fechado de $\topology{X}$}\\B \subset Z \cap F}} (Z \cap F)
      \\
      &=
      Z \cap \left(\bigcap_{\substack{\text{$F$: fechado de $X$}\\B \subset F}} F\right)
      \\
      &=
      Z \cap \closureop[\topology{X}]{B}.
    \end{align*}
    (em que lugar da equação foi utilizado que $B \subset Z$?)


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_da_topologia_induzida:convergencia}.
    }

    Exercício.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_da_topologia_induzida:continuidade_no_contradominio}
    }

    Basta notar que $f^{-1}(Z \cap A) = f^{-1}(A)$.


    \proofitem
    {
      Item
      \refitem{it:prop:propriedades_da_topologia_induzida:continuidade_no_dominio}
    }

    Exercício.
  \end{proof}

  \begin{obs}
    Para uma aplicação $\function{f}{\topologicalspace{X}}{\topologicalspace{Y}}$ e um
    subconjunto $Z \subset X$, sempre que falarmos de propriedades
    topológicas de $f|_Z$ estaremos nos referindo à topologia
    $Z \cap \topology{X}$. De modo mais geral, a menos que se diga o
    contrário, consideraremos $Z \subset X$
    dotado da topologia $Z \cap \topology{X}$.
  \end{obs}

  \begin{obs}
    Note que se $Z$ é um aberto, então
    \begin{equation*}
      Z \cap \topology{X} = \setsuchthat{A \in \topology{X}}{A \subset Z}.
    \end{equation*}
    Em particular, os abertos da topologia induzida são também abertos
    na topologia original. Isso não vale em geral.

    Da mesma forma, se $Z$ for fechado, os fechados da topologia
    induzida serão exatamente os fechados da topologia original que
    estejam contidos em $Z$. (demonstre!)
  \end{obs}



  \begin{example}[Topologia Induzida: $[0,1) \subset \reals$]
    Considere a topologia no intervalo $[0,1)$ induzida
    pela topologia usual dos números reais. Então, por exemplo, a
    família
    \begin{equation*}
      \family{B}_0 = \setsuchthat{[0,\frac{1}{n})}{n \in \naturals}
    \end{equation*}
    é uma base de vizinhanças abertas para o ponto $0$.
  \end{example}

  \begin{example}[Espaço Métrico]
    Em um espaço métrico $(X, d)$, temos a topologia $\topology{d}$, em $X$,
    induzida pela métrica $d$. Se $Z \subset X$ é um subconjunto
    qualquer de $X$, então, a princípio, temos duas maneiras canônicas
    de induzir uma topologia em $Z$. Temos $Z \cap \topology{d}$, e temos
    também a topologia $\topology{d_Z}$ induzida pela restrição da métrica
    $d$ ao conjunto $Z$:
    \begin{equation*}
      \functionarray{d_Z}{Z \times Z}{\nonnegativereals}{(z_1, z_2)}{d(z_1, z2)}.
    \end{equation*}
    Essas duas topologias coincidem. (por quê? dica: o que são as
    bolas na métrica induzida?)
  \end{example}



  \subsection{União Disjunta}

    Sejam $\topologicalspace{X}$ e $\topologicalspace{Y}$ espaços
    topológicos disjuntos.
    O leitor não deverá ter problemas para se convencer que é natural
    definir a topologia
    \begin{equation*}
      \topology{X \cup Y}
      =
      \setsuchthat{U \cup V}{U \in \topology{X},\, V \in \topology{Y}}
    \end{equation*}
    em $X \cup Y$.
    Note que
    $\topology{X \cup Y} = \generatedtopology{\topology{X} \cup \topology{Y}}$.
    Essa topologia é caracterizada pela propriedade
    \begin{equation*}
      \topology{X} = X \cap \topology{X \cup Y}
      \quad \text{ e } \quad
      \topology{Y} = Y \cap \topology{X \cup Y}.
    \end{equation*}

    No Capítulo \ref{ch:conexidade}, estudaremos a existência de
    conjuntos que são fechados e abertos ao mesmo tempo. Se em um
    espaço topológico $\topologicalspace{W}$ existe um subconjunto próprio não
    vazio, $X \subset W$, que é aberto e fechado ao mesmo tempo, então seu
    complemento, $Y = \complementset{X}$ também é aberto e fechado.
    Neste caso, os abertos de $W$ são da forma $U \cup V$, onde
    $U \in \topology{X}$ e $V \in \topology{Y}$.
    Dizemos que $\topologicalspace{W}$ é desconexo
    (Definição \ref{def:conexidade}).

    {\input{2/04/01/exercicios}}
