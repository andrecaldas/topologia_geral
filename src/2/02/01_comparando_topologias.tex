\section{Comparando Topologias}

  Em um mesmo conjunto $X$, podemos ter definidas duas topologias
  $\topology{1}$ e $\topology{2}$. Pode acontecer que $\topology{1} \subset \topology{2}$, por
  exemplo. Neste caso, sempre que $\function{f}{\topologicalspace{X}}{(Y,
  \topology{Y})}$ for contínua, teremos que $\function{f}{(X,
  \topology{2})}{\topologicalspace{Y}}$ também será contínua. Também podemos
  concluir que
  \begin{equation*}
    x_n \xrightarrow{\topology{2}} x \Rightarrow x_n \xrightarrow{\topology{1}} x.
  \end{equation*}
  Pode ser que não tenhamos nem $\topology{1} \subset \topology{2}$, nem $\topology{2}
  \subset \topology{1}$. Duas topologias nem sempre são comparáveis.

  \begin{definition}
    \label{def:topologia_mais_fina}

    Se $\topologicalspace{X}$ e $\topologicalspace{X}$ são duas topologias em um mesmo
    conjunto $X$ e $\topology{1} \subset \topology{2}$, então dizemos que $\topology{2}$
    é \emph{mais forte} ou \emph{mais fina} que $\topology{1}$. Também
    dizemos que $\topology{1}$ é \emph{mais fraca} que $\topology{2}$.
    Podemos também dizer que $\topology{1}$ é \emph{menor} que $\topology{2}$, ou que
    $\topology{2}$ é \emph{maior} que $\topology{1}$.
    Veja a Observação \ref{obs:ordem_parcial_nas_topologias}.
  \end{definition}

  A topologia determina quais são as sequências que convergem e quais
  não convergem. Se imaginarmos a topologia como uma ``peneira'' que
  deixa passar apenas as sequências convergentes, quanto mais fina for
  a topologia, menos sequências passarão como convergentes.
  Veja a Figura \ref{fig:topologia_mais_fina}.

  \begin{figure}[!htp]
    \begin{center}
      \includegraphics{figs/finer_topology}
    \end{center}
    \caption{
      Quanto \emph{mais fina} é a topologia, menos sequências
      ``passam'' como convergentes.
    }

    \label{fig:topologia_mais_fina}
  \end{figure}



  \begin{proposition}
    \label{prop:intersecao_de_topologias}

    Seja $X$ um conjunto qualquer, e $\topology{\lambda}$ $(\lambda \in
    \Lambda)$ uma família de topologias em $X$. Então $\topology{X} =
    \bigcap_{\lambda \in \Lambda} \topology{\lambda}$ é uma topologia em
    $X$.
  \end{proposition}

  \begin{proof}
    Basta verificar que $\topology{X}$ satisfaz os axiomas listados na
    Definição \ref{def:topologia}. Por exemplo,
    \begin{align*}
      A, B \in \topology{X}
      &\Rightarrow
      \forall \lambda \in \Lambda,\, A \cap B \in \topology{\lambda}
      \\
      &\Rightarrow
      A \cap B \in \bigcap_{\lambda \in \Lambda} \topology{\lambda} = \topology{X}.
    \end{align*}
  \end{proof}



  \begin{obs}
    \label{obs:ordem_parcial_nas_topologias}

    A relação \emph{``mais forte que''} define uma \emph{ordem parcial} na
    família
    \begin{equation*}
      \family{T}(X)
      =
      \setsuchthat{\topology{X} \subset \parts{X}}
                  {\topology{X} \text{ é topologia}},
    \end{equation*}
    das topologias de um conjunto $X$.
    Esta ordem é simplesmente a restrição da relação de inclusão
    definida em $\parts{X}$ à família $\family{T}(X)$.

    Existe um elemento \emph{máximo} dentre todas as topologias de
    $X$. É o conjunto das partes de $X$, $\parts{X}$,
    que é a topologia mais forte que pode ser definida em $X$.
    Por outro lado, $\{\emptyset, X\}$ é a topologia mais fraca em
    $\family{T}(X)$.

    A Proposição \ref{prop:intersecao_de_topologias} mostra que dada
    uma família de topolgias $\topology{\lambda\,} (\lambda \in \Lambda)$,
    existe a \emph{maior topologia que é menor que todas as $\topology{\lambda}$}.
    Essa topologia $\topology{\delta}$ é o \emph{ínfimo} das $\topology{\lambda}$.
    Escrevemos
    \begin{equation*}
      \topology{\delta}
      =
      \bigwedge \setsuchthat{\topology{\lambda}}{\lambda \in \Lambda}.
    \end{equation*}
    ou
    \begin{equation*}
      \topology{\delta}
      =
      \bigwedge_{\lambda \in \Lambda} \topology{\lambda}.
    \end{equation*}
    Por outro lado, a união de topologias não é necessariamente uma
    topologia. No entanto, se considerarmos a família
    \begin{equation*}
      \family{F} = \setsuchthat{\topology{X} \in \family{T}(X)}
                               {\bigcup_{\lambda \in \Lambda} \topology{\lambda} \subset \topology{X}}
    \end{equation*}
    de todas as topologias que são maiores que todas as $\topology{\lambda}$,
    sabemos que a família $\family{F}$ não é vazia,
    pois $\parts{X} \in \family{F}$.
    Seja então $\topology{\sigma}$ o ínfimo de $\family{F}$:
    \begin{equation*}
      \topology{\sigma}
      =
      \bigwedge \family{F}.
    \end{equation*}
    A topologia $\topology{\sigma}$ é a menor topologia
    que é maior que todas as $\topology{\lambda}$.
    Essa topologia é o supremo de $\topology{\lambda}$,
    e é denotada por
    \begin{equation*}
      \topology{\sigma}
      =
      \bigvee_{\lambda \in \Lambda} \topology{\lambda}.
    \end{equation*}
  \end{obs}

%TODO: Exercícios sobre reticulados.
%TODO: Exercício - a família de todas as topologias é um reticulado,
%      mas não é um sub-reticulado de \parts{X}.
%TODO: Exercício -- Pode ser que \sigma, \delta \not \in \family{F}.


  \subsection{Comparação de Topologias e Continuidade}

    Quando $X$ é um espaço topológico dotado de duas topologias
    $\topology{1}$ e $\topology{2}$, o que podemos dizer da relação entre essas
    topologias se soubermos que a aplicação identidade
    \begin{equation*}
      \functionarray{\identity}{(X,\topology{1})}{\topologicalspace{X}}{x}{x}
    \end{equation*}
    é contínua? A resposta é simples:
    \begin{equation*}
        \identity \text{ é contínua} \Leftrightarrow \topology{2} \subset \topology{1}.
    \end{equation*}
    Vamos generalizar isso para uma aplicação qualquer
    \begin{equation*}
      \function{f}{(X,\topology{1})}{\topologicalspace{X}}.
    \end{equation*}

    \begin{proposition}
      \label{prop:imagem_inversa_de_topologia}

      Seja $X$ um conjunto qualquer e $\topologicalspace{Y}$ um espaço
      topológico. Dada uma aplicação qualquer $\function{f}{X}{Y}$,
      $\topology{f} = f^{-1}(\topology{Y})$ define uma topologia em $X$.
    \end{proposition}

    \begin{proof}
      Basta notar que $\emptyset, X \in \topology{f}$, se $A, B \in \topology{f}$,
      então $A = f^{-1}(A')$ e $B = f^{-1}(B')$ com $A', B' \in
      \topology{Y}$. Como $\topology{Y}$ é uma topologia, $A' \cap B' \in
      \topology{Y}$. Assim, $A \cap B = f^{-1}(A' \cap B') \in
      f^{-1}(\topology{Y}) = \topology{f}$. Podemos fazer analogamente para a
      união arbitrária de elementos de $\topology{f}$. Basta observar que
      $\function{f^{-1}}{\parts{Y}}{\parts{X}}$ \emph{comuta com} as
      operações de união e interseção.
    \end{proof}


    Pela Proposição \ref{prop:imagem_inversa_de_topologia}, dizer que
    $\function{f}{\topologicalspace{X}}{\topologicalspace{Y}}$ é contínua é o mesmo que
    dizer que a topologia $\topology{f}$ é mais fraca que $\topology{X}$. De fato,
    é fácil verificar que $\topology{f}$ é a menor topologia que faz com que
    $\function{f}{X}{\topologicalspace{Y}}$ seja contínua.
%TODO: exercício -- verifique! :-)
