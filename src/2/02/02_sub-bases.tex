\section{Sub-Base}

  A construção feita na Observação
  \ref{obs:ordem_parcial_nas_topologias} é muito comum. É essa
  construção que em álgebra, por exemplo, nos permite definir
  para um espaço vetorial $V$
  e um subconjunto qualquer $S \subset V$,
  o menor subespaço de $V$ que contém $S$.
  Este é o \emph{subespaço vetorial gerado por $S$}.
  Do mesmo modo,
  para um grupo $G$ e um subconjunto qualquer $S \subset G$,
  pode-se definir o que seria o \emph{grupo gerado por $S$}.
  Esse seria o menor subgrupo de $G$ que contém $S$.
  Existem exemplos também em teoria da medida.
  Assim, como na Proposição
  \ref{prop:intersecao_de_topologias}, a interseção de uma família de
  subgrupos é um subgrupo, a interseção de uma família de
  subespaços vetoriais é um subespaço vetorial.

  \begin{definition}
    \label{def:topologia_gerada}

    Seja $X$ um conjunto, e $\family{C} \subset \parts{X}$ uma
    família qualquer de subconjuntos de $X$. Então a topologia
    \begin{equation*}
      \generatedtopology{\family{C}} = \bigvee_{\family{C} \subset \topology{X}} \topology{X}
    \end{equation*}
    é a topologia gerada por $\family{C}$. Essa é a menor topologia de
    $X$ que contém a família $\family{C}$.
  \end{definition}
% TODO: exercício -- mostrar que \family{C} \subset
% \generatedtopology{\family{C}}.

  \begin{definition}
    \label{def:base_gerada}

    Seja $X$ um conjunto qualquer, e $\family{C} \subset \parts{X}$ uma
    família qualquer de subconjuntos de $X$.
    Mesmo sem definir o que venha a ser uma base para a topologia
    (Definição \ref{def:base_da_topologia}),
    vamos definir o conjunto
    \begin{equation*}
      \generatedtopologybase{\family{C}}
      =
      \setsuchthat{\bigcap_{A \in \family{C}'} A}
                  {
                    \family{C}' \subset \family{C},\,
                    \cardinality{\family{C}'} < \infty
                  },
    \end{equation*}
    e chamá-lo de \emph{base induzida pela família $\family{C}$}.
    Aqui, usamos a convenção de que
    $\bigcap_{A \in \emptyset} A = X$.
  \end{definition}

  \begin{obs}
    \label{obs:intersecao_e_uniao_vazias}
    Na
    Definição \ref{def:base_gerada},
    utilizamos a seguinte convenção:
    \begin{align*}
      \bigcup_{A \in \emptyset} A &= \emptyset \\
      \bigcap_{A \in \emptyset} A &= X.
    \end{align*}
    Esta convenção se torna mais natural se, considerando a relação
    de ordem $\subset$ em $\parts{X}$, interpretarmos $\cup$ e
    $\cap$ como sendo operadores de supremo e ínfimo, assim como fizemos
    na Observação \ref{obs:ordem_parcial_nas_topologias}.
    Desta forma, dado
    $\family{F} \subset \parts{X}$,
    \begin{equation*}
      \bigcup_{A \in \family{F}} A
    \end{equation*}
    é o menor subconjunto de $X$ que é maior que todos os conjuntos
    em $\family{F}$.
    Se $\family{F}$ é vazio, então o menor
    subconjunto seria justamente $\emptyset$.
    Da mesma forma,
    \begin{equation*}
      \bigcap_{A \in \family{F}} A
    \end{equation*}
    é o maior subconjunto de $X$ que é menor que todos os conjuntos
    em $\family{F}$. Se $\family{F} = \emptyset$, este conjunto é
    simplesmente o maior subconjunto de $X$, que é o próprio $X$.
  \end{obs}

  Por um abuso de notação, quando $\family{C} =
  \setsuchthat{A_\lambda}{\lambda \in \Lambda}$, por vezes escrevemos
  $\generatedtopology{A_\lambda,\, \lambda \in \Lambda}$ no lugar de
  $\generatedtopology{\family{C}}$. E quando
  $\family{C_\lambda},\, (\lambda \in \Lambda)$ é uma coleção de
  famílias de subconjuntos de $X$, escrevemos
  $\generatedtopology{\family{C}_\lambda,\, \lambda \in \Lambda}$ ao
  invés de $\generatedtopology{\bigcup_{\lambda \in \Lambda}
  \family{C}_\lambda}$. As seguintes propriedades da topologia
  gerada por uma família são consequência direta da definição. O
  leitor deve ficar atento para a diferença entre
  \begin{equation*}
    \generatedtopology{\family{C}_\lambda,\, \lambda \in \Lambda}
  \end{equation*}
  e
  \begin{equation*}
    \generatedtopology{\family{C}_\lambda},\, \lambda \in \Lambda.
  \end{equation*}

  \begin{proposition}
    \label{prop:propriedades_topologia_gerada}

    Sejam $\family{C}$ e $\family{D}$ famílias de subconjuntos de $X$,
    e $\topology{X}$ uma topologia em $X$.
    Então,
    valem as seguintes propriedades:
    \begin{enumerate}
      \item
        Todos os conjuntos em $\family{C}$ são abertos na topologia
        gerada: $\family{C} \subset \generatedtopology{\family{C}}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:inclusao}
        Se $\family{C} \subset \family{D}$, então
        $\generatedtopology{\family{C}} \subset
        \generatedtopology{\family{D}}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:inclusao_em_uma_topologia}
        Temos que $\family{C} \subset \topology{X}$ se, e somente se,
        $\generatedtopology{\family{C}} \subset \topology{X}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:idempotencia}
        Se $\topology{X}$ é uma topologia, então $\topology{X} =
        \generatedtopology{\topology{X}}$. Em particular, vale que
        \begin{equation*}
          \generatedtopology{\generatedtopology{\family{C}}}
          = \generatedtopology{\family{C}}.
        \end{equation*}

      \item
        \label{it:prop:propriedades_topologia_gerada:sub-base}
        Se $\family{C} \subset \topology{X} \subset
        \generatedtopology{\family{C}}$, então $\topology{X} =
        \generatedtopology{\family{C}}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:refinamento_de_sub-base}
        Se $\family{C} \subset \family{D} \subset
        \generatedtopology{\family{C}}$, então
        $\generatedtopology{\family{C}} =
        \generatedtopology{\family{D}}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:pela_uniao_finita}
        Se $\family{E} = \generatedtopology{\family{C}} \cup
        \generatedtopology{\family{D}}$, então
        $\generatedtopology{\family{E}} =
        \generatedtopology{\family{C} \cup \family{D}}$.

      \item
        \label{it:prop:propriedades_topologia_gerada:pela_uniao}
        Seja $\family{C}_\lambda,\, (\lambda \in \Lambda)$ uma coleção
        de famílias de subconjuntos de $X$. Então,
        \begin{equation*}
          \generatedtopology{\generatedtopology{\family{C}_\lambda},\, \lambda \in \Lambda}
          =
          \generatedtopology{\family{C}_\lambda,\, \lambda \in \Lambda}.
        \end{equation*}
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    Vamos mostrar apenas o item
    \refitem{it:prop:propriedades_topologia_gerada:pela_uniao}, que é
    mais difícil. O restante fica como exercício. :-)

    É evidente, pelo item
    \refitem{it:prop:propriedades_topologia_gerada:inclusao}, que
    \begin{equation*}
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \family{C}_\lambda}
      \subset
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \generatedtopology{\family{C}_\lambda}}.
    \end{equation*}
    No entanto, novamente pelo item
    \refitem{it:prop:propriedades_topologia_gerada:inclusao}, sabemos
    que para todo $\gamma \in \Lambda$,
    \begin{equation*}
      \generatedtopology{\family{C}_\gamma}
      \subset
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \family{C}_\lambda}.
    \end{equation*}
    E portanto,
    \begin{equation*}
      \bigcup_{\lambda \in \Lambda}
      \generatedtopology{\family{C}_\lambda}
      \subset
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \family{C}_\lambda}.
    \end{equation*}
    Agora, pelo item
    \refitem{it:prop:propriedades_topologia_gerada:sub-base},
    \begin{equation*}
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \generatedtopology{\family{C}_\lambda}}
      =
      \generatedtopology{\bigcup_{\lambda \in \Lambda} \family{C}_\lambda}.
    \end{equation*}
  \end{proof}


  \input{2/02/02/01_topologia_gerada}
  \input{2/02/02/02_sub-base_e_continuidade}
  \input{2/02/02/exercicios}
