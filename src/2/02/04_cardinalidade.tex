\section{Cardinalidade das Bases e Sub-Bases}

  Principalmente quando trabalhamos com sequências de elementos,
  ou mesmo sequências de conjuntos, a existência ou não de
  bases ou bases de vizinhanças que sejam enumeráveis
  torna-se uma questão importante.

  \begin{definition}
    \label{def:base:cardinalidade}
    Dizemos que um espaço topológico $X$ é \emph{segundo-enumerável}
    quando possuir uma base enumerável.
    Se todo $x \in X$ possui uma base enumerável de vizinhanças,
    dizemos que $X$ é \emph{primeiro-enumerável}.
  \end{definition}

  \begin{obs}
    A nomenclatura da Definição \ref{def:base:cardinalidade}
    é uma tradução direta da língua inglesa
    ---
    \emph{first countable} e \emph{second countable}
    ---,
    e é muito ruim.
    Se um espaço topológico $X$ possui uma base enumerável,
    não seria melhor dizer que $X$ \emph{possui base enumerável}?
    Da mesma forma, neste livro, vamos dizer que
    $x \in X$ \emph{possui base enumerável de vizinhanças}.
    Ao invés de dizer \emph{primeiro-enumerável},
    vamos simplesmente dizer que todo ponto de $X$
    \emph{possui base enumerável de vizinhanças}.
  \end{obs}

  \begin{example}[Espaço Métrico]
    \label{ex:espaco_metrico:base_enumeravel_de_vizinhancas}
    Em um espaço métrico $X$,
    um ponto qualquer possui uma base enumerável de vizinhanças.
    De fato, dado $x$, as bolas de raio
    $\frac{1}{n}$ centradas em $x$ formam uma base de vizinhanças.
    Essa base de vizinhanças tem a propriedade de
    poder ser ordenada de forma decrescente.
    Ou seja,
    \begin{equation*}
      \ball{\frac{1}{n}}{x}
      \supset
      \ball{\frac{1}{n+1}}{x}.
    \end{equation*}
    Esta é a propriedade que nos permitiu estabelecer
    a equivalência entre continuidade com bolas
    e continuidade com sequências para aplicações entre
    espaços métricos.
    Veja a
    Proposição \ref{prop:espaco_metrico:continuidade:bolas}.
  \end{example}

  Na Definição
  \ref{def:base:cardinalidade},
  não mencionamos nada sobre a cardinalidade das sub-bases.
  A seguinte proposição explica porque.

  \begin{proposition}
    \label{prop:cardinalidade_de_base_e_sub-base}
    Em um espaço topológico $X$ com infinitos abertos,
    existe uma base com cardinalidade $\kappa$ se, e somente se,
    existe uma sub-base com a mesma cardinalidade.
    Em particular, o espaço possui base enumerável se, e somente se
    possuir uma sub-base enumerável.
  \end{proposition}

  \begin{proof}
    Como toda base é uma sub-base, basta mostrar que
    dada uma sub-base $\family{S}$, existe uma base
    $\family{B}$ com a mesma cardinalidade que $\family{S}$.
    Primeiramente, é preciso notar que a cardinalidade de $\family{S}$
    não pode ser finita.
    Caso contrário, não existiriam infinitos abertos na topologia.

    Note que a família
    \begin{equation*}
      \family{B}_n
      =
      \setsuchthat{U_1 \cap \dotsb \cap U_n}{U_1, \dotsc, U_n \in \family{S}}
    \end{equation*}
    tem a mesma cardinalidade que $\family{S}$ (por quê?).
    Note também, que
    \begin{equation*}
      \family{B}
      =
      \set{X}
      \cup
      \bigcup_{n=1}^\infty
      \family{B}_n,
    \end{equation*}
    e portanto, $\family{B}$ também tem
    a mesma cardinalidade que $\family{S}$ (por quê?).
  \end{proof}

  % TODO: Exercício -- onde foi usado o fato de que $\family{S}$ é infinito?
  % TODO: Exercício -- por que precisamos de \set{X} \cup ...?

  Os espaços tais que todo ponto possui uma
  base enumerável de vizinhanças são semelhantes aos
  espaços métricos.
  Onde usaríamos sequências de bolas de raio $\frac{1}{n}$,
  usamos a proposição abaixo.
  Um exemplo é a
  Proposição \ref{prop:base_de_vizinhancas_enumeravel:sequencias}.

  \begin{proposition}
    \label{prop:base_encaixante}
    Seja $X$ um espaço topológico e $x \in X$ um elemento qualquer.
    Se $x$ possui uma base enumerável infinita de vizinhanças,
    $\family{B}_x$,
    então $x$ possui uma base de vizinhanças formada por conjuntos
    $B_1, B_2, \dotsc$ satisfazendo
    \begin{equation*}
      B_1
      \supset
      B_2
      \supset
      B_3
      \supset
      \dotsb
    \end{equation*}
    Dizemos que $\family{B}_x$ é uma base de vizinhanças \emph{encaixantes}.
  \end{proposition}

  \begin{proof}
    Seja $B_1', B_2', \dotsc$ uma enumeração dos elementos
    de $\family{B}_x$.
    Faça
    \begin{equation*}
      B_n
      =
      \bigcap_{j=1}^n
      B_j'.
    \end{equation*}
    Por ser interseção finita de vizinhanças de $x$,
    cada $B_n$ é uma vizinhança de $x$.
    E dada uma vizinhança qualquer de $x$, $V$,
    existe $n$ tal que $B_n' \subset V$.
    Ou seja,
    \begin{equation*}
      B_n
      \subset
      B_n'
      \subset
      V.
    \end{equation*}
    E portanto, os conjuntos $B_n$ formam uma base
    de vizinhanças de $x$.
  \end{proof}


  É claro que se $x$ possuir uma base finita de vizinhanças,
  então $x$ possui uma base formada por apenas um elemento:
  a interseção de todos os elementos da base finita.

  Em um espaço onde todo ponto possui base enumerável de vizinhanças,
  a topologia pode ser inteiramente descrita através de sequências
  e seus limites.

  \begin{proposition}
    \label{prop:base_de_vizinhancas_enumeravel:sequencias}
    Seja $X$ um espaço topológico, $x \in X$,
    e $\family{B}_x \subset \neighbourhoods{x}$ uma
    base enumerável de vizinhanças de $x$.
    Então, a família $\neighbourhoods{x}$
    pode ser inteiramente determinada se soubermos quais são
    as sequências que convergem para $x$.
  \end{proposition}

  \begin{proof}
    Pela Proposição \ref{prop:base_encaixante},
    podemos assumir que
    $\family{B}_x = \set{B_1, B_2, \dotsc}$,
    com $B_n \supset B_{n+1}$.

    Vamos mostrar que $V$ é vizinhança de $x$ se, e somente se,
    para toda sequência convergente $x_n \rightarrow x$,
    tivermos um $N$ tal que
    $n \geq N \Rightarrow x_n \in V$.
    Se $V$ é vizinhança de $x$, então,
    é evidente que para toda sequência $x_n \rightarrow x$
    existe um tal $N$.
    Por outro lado, se $V$ não é uma vizinhança de $x$,
    então, para cada $n$, existe $B_n$ tal que
    $B_n \setminus V \neq \emptyset$.
    Tome $x_n \in B_n \setminus V$.
    A sequência $x_n$ converge para $x$ (por quê?).
    No entanto,
    para a sequência $x_n$, não existe o referido $N$.
  \end{proof}


  Um exemplo de aplicação da enumerabilidade de uma base
  da topologia, é a demonstração da
  Proposição \ref{prop:espaco_metrico:compacidade_com_sequencias}.
  Uma propriedade que está bastante relacionada é a
  separabilidade do espaço topológico.

  \begin{definition}[Espaço Separável]
    Um espaço topológico $X$ é \emph{separável}
    quando houver um subconjunto enumerável denso.
    Ou seja, quando existir $S \subset X$ enumerável
    tal que $\closure{S} = X$.
  \end{definition}


  Uma das utilidades de se demonstrar que um
  espaço possui base enumerável, é poder concluir
  que este espaço é separável.

  \begin{proposition}
    \label{prop:base_enumeravel_eh_separavel}
    Um espaço topológico com base enumerável é separável.
  \end{proposition}

  \begin{proof}
    Seja $B_1, B_2, \dotsc$ uma base enumerável.
    Agora, para cada $B_n$, escolha $x_n \in B_n$.
    Então, o conjunto
    $S = \set{x_1, x_2, \dotsc}$
    é denso.
  \end{proof}

  % TODO: Exercício -- S é denso.


  Por outro lado, uma das utilidades de se demonstrar que
  um espaço é separável, é, em alguns casos,
  poder concluir que este espaço possui base enumerável.
  É o que faremos na demonstração da
  Proposição \ref{prop:espaco_metrico:compacidade_com_sequencias}.

  \begin{proposition}
    \label{prop:espaco_metrico:separavel_sse_tem_base_enumeravel}
    Um espaço métrico $(X,d)$ tem base enumerável se, e somente se,
    é separável.
  \end{proposition}

  \begin{proof}
    Pela Proposição \ref{prop:base_enumeravel_eh_separavel},
    basta mostrar que se $X$ for separável, então
    $X$ tem base enumerável.
    Seja $S \subset X$ um subconjunto enumerável denso.
    Basta, então, mostrar que
    \begin{equation*}
      \family{B}
      =
      \setsuchthat{\ball{\frac{1}{n}}{x}}{x \in S,\, n = 1,2,\dotsc}
    \end{equation*}
    é uma base para a topologia de $X$.

    Como $\family{B}$ é uma família de abertos, basta mostrar que
    para todo $a \in X$,
    dado $m \in \notnullnaturals$,
    existem $x \in S$ e $n \in \notnullnaturals$ tais que
    \begin{equation*}
      a
      \in
      \ball{\frac{1}{n}}{x}
      \subset
      \ball{\frac{1}{m}}{a}.
    \end{equation*}
    Pela densidade de $X$, podemos escolher $x \in S$
    tal que $d(x,a) < \frac{1}{2m}$.
    Assim, basta tomar $n = 2m$, pois além de termos
    $a \in \ball{\frac{1}{n}}{x}$,
    também temos que para todo $y \in \ball{\frac{1}{n}}{x}$,
    \begin{align*}
      d(y,a)
      &\leq
      d(y,x) + d(x,a)
      \\
      &<
      \frac{1}{2m}
      +
      \frac{1}{2m}
      =
      \frac{1}{m}.
    \end{align*}
    Ou seja,
    $
      \ball{\frac{1}{n}}{x}
      \subset
      \ball{\frac{1}{m}}{a}
    $.
  \end{proof}

  % TODO: Exercício -- por que basta mostrar que dado n...?
  % TODO: Exercício -- Na prop. usamos que x \in ball(a) <==> a \in ball(x).


  \input{2/02/04/exercicios}
