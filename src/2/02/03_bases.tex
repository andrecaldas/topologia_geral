\section{Bases}

  Dada uma família $\family{C}$ de subconjuntos de um conjunto $X$, a
  base induzida $\family{B} = \generatedtopologybase{\family{C}}$
  tinha uma propriedade interessante:
  \begin{quote}
    Todo aberto de $\generatedtopology{\family{B}}$ é uma união de
    elementos de $\family{B}$.
  \end{quote}
  Cada família com essa propriedade é uma de \emph{base da
  topologia}. Quando construímos a topologia dos espaços métricos, as
  \emph{bolas} formavam uma base para a topologia
  (Proposição \ref{prop:espaco_metrico:abeto_eh_uniao_de_bolas}).

  \begin{definition}
    \label{def:base_da_topologia}

    Seja $\topologicalspace{X}$ um espaço topológico. Uma família $\family{B}
    \subset \topology{X}$ é uma \emph{base para a topologia $\topology{X}$} quando
    todo conjunto $A \in \topology{X}$ puder ser escrito como uma união de
    elementos de $\family{B}$.
    Aqui, seguimos a convenção de que
    $\bigcup_{A \in \emptyset} A = \emptyset$, 
  \end{definition}

  Como de costume, vamos ver outras maneiras de caracterizar o que vem
  a ser uma base para uma topologia $\topology{X}$. Note que uma das condições
  da Definição \ref{def:base_da_topologia} é que
  $\family{B} \subset \topology{X}$.

  \begin{proposition}
    \label{prop:base_da_topologia:caracterizacoes}

    Seja $\topologicalspace{X}$ um espaço topológico e
    $\family{B} \subset \parts{X}$
    uma família de subconjuntos de $X$. 
    Então as seguintes afirmações são equivalentes:
    \begin{enumerate}
      \item
        \label{it:prop:base_da_topologia:caracterizacoes:apenas_unioes}
        A família
        \begin{equation*}
          \rho = \setsuchthat{\bigcup_{A \in \family{B}'} A}
                             {\family{B'} \subset \family{B}}
        \end{equation*}
        é uma topologia de $X$.
        E além disso,
        $\topology{X} = \rho$.


      \item
        \label{it:prop:base_da_topologia:caracterizacoes:definicao}
        A família $\family{B}$ é uma base para $\topology{X}$.

      \item
        \label{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas_implicita}
        Temos que $\family{B} \subset \topology{X}$,
        e para todo $x \in X$ e toda vizinhança $V$ de $x$,
        existe $B \in \family{B}$ tal que $x \in B \subset V$.

      \item
        \label{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas}
        Para todo $x \in X$, o conjunto
        \begin{equation*}
          \family{B}_x = \setsuchthat{A \in \family{B}}{x \in A}
        \end{equation*}
        é uma base de vizinhanças de $x$
        (veja a Definição \ref{def:base_de_vizinhancas}).

      \item
        \label{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
        Para todo $A, B \in \family{B}$ e $x \in A \cap B$ existe
        $C \in \family{B}$ tal que $x \in C \subset A \cap B$. E além
        disso, $\topology{X} = \generatedtopology{\family{B}}$ e
        $X = \bigcup_{A \in \family{B}} A$.
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    \proofitem
    {
      \refitem{it:prop:base_da_topologia:caracterizacoes:apenas_unioes}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:definicao}
    }
    {
      Pela definição de base, $\family{B}$ é uma base para $\rho$. Como
      \begin{equation*}
        \family{B}
        \subset
        \rho
        \subset
        \generatedtopology{\family{B}},
      \end{equation*}
      temos que $\rho = \generatedtopology{\family{B}} = \topology{X}$.
    }


    \proofitem
    {
      \refitem{it:prop:base_da_topologia:caracterizacoes:definicao}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas_implicita}
    }
    {
      Se $\family{B}$ é base para $\topology{X}$,
      então $\family{B} \subset \topology{X}$.
      Seja $V \in \neighbourhoods{x}$.
      Então,
      $A = \interior{V}$ é aberto e $x \in A$.
      Como o conjunto $A$ é da forma
      \begin{equation*}
        A
        =
        \bigcup_{B \in \family{B}'} B
      \end{equation*}
      para alguma sub-família $\family{B}' \subset \family{B}$, basta
      escolher $B \in \family{B}'$ tal que $x \in B$,
      para que
      \begin{equation*}
        x
        \in
        B
        \subset
        A
        \subset
        V.
      \end{equation*}
    }


    \proofitem
    {
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas_implicita}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas}
    }
    {
      Evidentemente que $\family{B}_x \subset \neighbourhoods{x}$, já
      que a família é formada por conjuntos abertos que contém $x$.
      Basta mostrar que para toda vizinhança $V$ de $x$,
      existe $B \in \family{B}_x$ tal que $B \subset V$.
      Mas a existência de tal $B$ é justamente a hipótese do item
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas_implicita}.
    }


    \proofitem
    {
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
    }
    {
      Se $A, B \in \family{B}$ e $x \in A \cap B$, então, por hipótese,
      $A$ e $B$ são vizinhanças de $x$. Portanto, $A \cap B$ também é
      vizinhança de $x$. Como $\family{B}_x$ é uma base de vizinhanças
      de $x$, então existe $C \in \family{B}_x$ tal que
      \begin{equation*}
        x
        \in
        C
        \subset
        A \cap B.
      \end{equation*}

      Precisamos então verificar que $\family{B}$ gera $\topology{X}$.
      Primeiramente, note que todo conjunto $U \in \family{B}$ é aberto,
      pois se $x \in U$, então $U \in \family{B}_x \subset
      \neighbourhoods{x}$.
      Ou seja, $U$ é vizinhança de todos os seus pontos.
      Assim,
      \begin{equation*}
        \generatedtopology{\family{B}} \subset \topology{X}.
      \end{equation*}
      Por outro lado, todo aberto de $\topology{X}$ pode ser escrito como uma
      união de elementos de $\family{B}$, pois dado $V \in \topology{X}$, para
      cada $x \in V$ existe $V_x \in \family{B}_x \subset \family{B}$
      tal que $x \in V_x \subset V$.
      Ou seja,
      \begin{equation*}
        V = \bigcup_{x \in V} V_x.
      \end{equation*}
      E portanto,
      \begin{equation*}
        \topology{X} \subset \generatedtopology{\family{B}}.
      \end{equation*}
      É evidente que $X = \bigcup_{A \in \family{B}} A$, pois nenhum dos
      $\family{B}_x$ é vazio.
      (Note que acabamos provando que
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_base_de_vizinhancas}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:definicao}.)
    }

    
    \proofitem
    {
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
      $\Rightarrow$
      \refitem{it:prop:base_da_topologia:caracterizacoes:apenas_unioes}
    }
    {
      Já sabemos que $\emptyset \in \rho$. Pela hipótese de $X$ ser a
      união de todos os conjuntos em $\family{B}$, $X \in \rho$. Por
      definição, $\rho$ é evidentemente fechada por união
      arbitrária. Resta mostrar que é também fechada por interseção
      finita. Para isso, basta notar que $A$ e $B$ são da forma
      \begin{align*}
        A &= \bigcup_{U \in \family{B}_A} U \\
        B &= \bigcup_{V \in \family{B}_B} V
      \end{align*}
      para $\family{B}_A ,\family{B}_B \in \family{B}$ adequados.
      Assim,
      \begin{equation*}
        A \cap B
        =
        \bigcup_{(U, V) \in \family{B}_A \times \family{B}_B} U \cap V,
      \end{equation*}
      sendo que, é fácil verificar que as hipóteses do item
      \refitem{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
      implicam que cada $U \cap V$ é uma união de
      conjuntos em $\family{B}$.
      Portanto, $A \cap B \in \rho$.
    }
  \end{proof}

  \begin{obs}
    \label{obs:base_gerada_por_sub-base}

    Dada uma família qualquer $\family{C} \subset \parts{X}$, a
    Proposição \ref{prop:formato_dos_abertos_na_topologia_gerada} mostra
    que $\generatedtopologybase{\family{C}}$ é uma base para
    $\generatedtopology{\family{C}}$. Em particular, toda família
    $\family{B}$ fechada por interseção finita é uma base para
    $\generatedtopology{\family{B}}$. Esse fato pode ser verificado
    também pelo item
    \refitem{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
    da Proposição \ref{prop:base_da_topologia:caracterizacoes}. Esta
    condição não é, no entanto, necessária para que $\family{B}$ seja
    uma base para $\generatedtopology{\family{B}}$. As bolas, por
    exemplo, formam uma base para a topologia de um espaço métrico, mas
    não é necessariamente verdade que a interseção de duas bolas será
    uma bola.
  \end{obs}

  \begin{corolary}
    \label{corolary:base_e_intersecao_finita}
    Seja $\family{B}$ uma família de subconjuntos de $X$,
    com $X = \bigcup_{A \in \family{B}} A$.
    Para que
    $\family{B}$ seja uma base de $\generatedtopology{\family{B}}$ é
    necessário e suficiente que para todo $A, B \in \family{B}$,
    $A \cap B$ possa ser escrito como união de elementos de
    $\family{B}$.
  \end{corolary}

  \begin{proof}
    A condição é evidentemente necessária.
    Para ver que é suficiente,
    basta verificar as condições do item
    \refitem{it:prop:base_da_topologia:caracterizacoes:com_intersecao}
    da Proposição
    \ref{prop:base_da_topologia:caracterizacoes}. Evidentemente que
    $\family{B}$ gera $\generatedtopology{\family{B}}$. O restante da
    demonstração fica como exercício.
  \end{proof}


  \input{2/02/03/01_sub-base_e_convergencia}
  \input{2/02/03/exercicios}
