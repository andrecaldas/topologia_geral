#! /bin/bash
#
# Substitute variables and conditionals in latex source code
# according provided variables file.
#
# Args:
# 1. Paths to variables file.
#
# Substitution is done line by line.
# Rules for substitution are as follows.
# 1. Uncomment satisfied @uncomment@. (only if the first char is %)
# 2. Comment out satisfied @comment@ with '% %'. (this is not uncommented by any @uncomment@!)
#
# The words @uncomment@ and @comment@ are substituted for
# @uncommentED@ and @commentED@. This way, the value used for the
# variable is the first one processed.
#

#set -x
set -e


usage ()
{
  echo "Usage:"
  echo "    $0 [<vars_file> ...]"
  echo "Hint: available styles at `dirname "$0"`/../styles/."
}

BIN_DIR="`dirname "$0"`"
. "$BIN_DIR/functions.sh"


#
# The main program.
# Substitutes variables and comment/uncomment out conditionals.
#

SED_ARGS="`get_comment_uncomment_sed_args "$@"`"

# Process the input.
eval sed -r $SED_ARGS
