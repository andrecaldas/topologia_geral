#! /bin/bash
#
# Convert all SVG files to PNG.
#
# Dependencies:
# 1. Inkscape: .
# 
#

#set -x
set -e

DPI=${DPI:-300}

convert_to_png ()
{
  local source="$1"
  local target="`dirname "$source"`/`basename "$source" .svg`.png"
  local target_eps="`dirname "$source"`/`basename "$source" .svg`.eps"

  if [ "$source" -nt "$target" ]
  then
    convert "$source" "$target"
    convert "$source" "$target_eps"
  fi
}

find . -name "*.svg" | while read svg_file;
do
  convert_to_png "$svg_file"
done
