#! /bin/bash
#
# Installs website to http://topologia-geral.ourproject.org.
# 

#set -x
set -e

USER="$1"
if [ -z "$USER" ]
then
  echo "Usage: $0 <username>"
  exit 1
fi

SRC_DIR="`dirname "$0$"`/../website/"

DEST_SRV="html.ourproject.org"
DEST_DIR="/home/groups/topologia-geral/htdocs/"
DEST="${USER}@${DEST_SRV}:${DEST_DIR}"

OLD_PATH="`pwd`"
cd "$SRC_DIR"

#tar -cz --exclude-vcs --exclude-backups * | ssh -l "$USER" "$DEST_SRV" "set -x; cd '$DEST_DIR' && tar -xz -p"
tar -cz --exclude-vcs --exclude '*~' --exclude '.#*' --exclude '#*#' * | ssh -l "$USER" "$DEST_SRV" "set -x; cd '$DEST_DIR' && tar -xz -p"


echo "Also install the book HTML version? Ctrl-C to cancel!"
read

ssh -l "$USER" "$DEST_SRV" "rm -rf '$DEST_DIR/topologia_geral'"

cd "$OLD_PATH/build/html/"

#tar -chz --exclude-vcs --exclude-backups topologia_geral/ | ssh -l "$USER" "$DEST_SRV" "set -x; cd '$DEST_DIR' && tar -xz -p"
tar -chz --exclude-vcs --exclude '*~' --exclude '.#*' --exclude '#*#' topologia_geral/ | ssh -l "$USER" "$DEST_SRV" "set -x; cd '$DEST_DIR' && tar -xz -p"
