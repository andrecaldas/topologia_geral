#! /bin/bash
#
# Generate html for an ALREADY adapted source on the build/ directory.
#
# Args:
# 1. Path to TEX file.
#
# Dependencies:
# 1. "`dirname "$0"`"/adapt_source.sh.
# 2. TeX4ht:
#    https://www.tug.org/applications/tex4ht/mn.html
# 3. Known problems:
#    https://www.tug.org/applications/tex4ht/mn.html

set -x
set -e


usage ()
{
  echo "Usage:"
  echo "    $0 <tex_file>"
}

BIN_DIR="`dirname "$0"`"
. "$BIN_DIR/functions.sh"

CONVERT_ALL_SVG="$BIN_DIR/convert_all_svg.sh"
BUILD_PDF="$BIN_DIR/build.sh"
BUILD_STYLE="$BIN_DIR/../styles/html.conf"
TEX4HT_CONF="$(cd "$BIN_DIR/../tex4ht" && pwd)/html5_mathjax_latex.cfg"
TEX4HT_CSS="$(cd "$BIN_DIR/../tex4ht" && pwd)/layout.css"


SRC_TEX_PATH="$1"
shift || true
if [ -z "$SRC_TEX_PATH" ];
then
  echo "No <tex_file> specified." >&2
  usage >&2
  exit 1
fi


BUILD_DIR="build/`build_subdir styles/html.conf`"
mkdir -p "$BUILD_DIR"
BUILD_FULLDIR="$(cd "$BUILD_DIR" && pwd)"
HTML_FULLDIR="$BUILD_FULLDIR/topologia_geral"

SRC_TEX_DIR="`dirname "$SRC_TEX_PATH"`"
SRC_TEX_FULLDIR="$(cd "`dirname "$SRC_TEX_PATH"`" && pwd)"
GENERATED_TEX_PATH="$BUILD_FULLDIR/`basename "$SRC_TEX_PATH"`"
INDEX_TEX_PATH="$BUILD_FULLDIR/index.tex"

mkdir -p "$HTML_FULLDIR"
rm -f "$BUILD_FULLDIR/figs" "$HTML_FULLDIR/figs"
ln -s "$SRC_TEX_FULLDIR/figs" "$BUILD_FULLDIR"
ln -s "$SRC_TEX_FULLDIR/figs" "$HTML_FULLDIR"
cp "$TEX4HT_CSS" "$HTML_FULLDIR"

"$CONVERT_ALL_SVG"

rm -f "$BUILD_FULLDIR/"*.aux
"$BUILD_PDF" "$SRC_TEX_PATH" "$BUILD_STYLE"
rm -f "$BUILD_FULLDIR/"*.aux
(
  cd "$BUILD_FULLDIR"
  cp "$GENERATED_TEX_PATH" "$INDEX_TEX_PATH"
  T4HINPUTS="$SRC_TEX_FULLDIR" htlatex "$INDEX_TEX_PATH" "$TEX4HT_CONF, 2, charset=utf-8" " -cunihtf -utf8" " -d$HTML_FULLDIR/"
)

#UGLY Hack!!

# Server is substituting <!-- --> for space.
find "$HTML_FULLDIR/" -name "*.html" -print0 | xargs -0 sed -r -e 's,<!--tex4ht:ref: [^>]* -->,,g' -i

# First page has no Crosslinks Menu. Careful with "body" tag closing ">".
sed -r -e 's,<body,<body><div class="crosslinks"><ul><li><a href="/">Site do livro</a></li></ul></div,g' -i "$HTML_FULLDIR/index.html"

# Unicode problem in titles.
for c in `seq 3`;
do
  find "$HTML_FULLDIR/" -name "*.html" -print0 |
   xargs -0 sed -r -e "s;(<title>.*)¸c(.*</title>);\1c\2;g" -i
done
