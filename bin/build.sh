#! /bin/bash
#
# Substitute variables and conditionals in latex source code
# according provided variables file.
#
# Args:
# 1. Path to TEX file.
# 2. Paths to conf files.
#
# Dependencies:
# 1. "`dirname "$0"`"/adapt_source.sh.
# 2. Rubber: http://rubber.sourceforge.net/.
# 

#set -x
set -e

RUBBER=rubber


usage ()
{
  echo "Usage:"
  echo "    $0 <tex_file> [<conf_file>...]"
  echo "Look for available styles at the `dirname "$0"`/../styles/ directory."
}

BIN_DIR="`dirname "$0"`"
. "$BIN_DIR/functions.sh"

SRC_TEX_PATH="$1"
shift || true
if [ -z "$SRC_TEX_PATH" ];
then
  echo "No <tex_file> specified." >&2
  usage >&2
  exit 1
fi


BUILD_DIR="$BIN_DIR/../build/"
mkdir -p "$BUILD_DIR"
BUILD_FULLDIR="$(cd "$BUILD_DIR" && pwd)"


SRC_TEX_DIR="`dirname "$SRC_TEX_PATH"`"
SRC_TEX_FULLDIR="$(cd "`dirname "$SRC_TEX_PATH"`" && pwd)"

set -x
BIBINPUTS="$SRC_TEX_FULLDIR" "$RUBBER" --module xelatex --texpath "$SRC_TEX_FULLDIR" --into "$BUILD_DIR" --warn refs "$SRC_TEX_PATH"
