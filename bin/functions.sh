#! /bin/bash
#
# Functions for variable substituition scripts.
#


# Echoes the prefix for the generated files.
build_subdir ()
{
  local BUILD_SUBDIR=""

  for VARS_PATH in "$@"
  do
    if [ ! -e "$VARS_PATH" ];
    then
      echo "No such file: $VARS_PATH." >&2
      usage >&2
      exit 1
    fi

    STYLE_NAME="`basename "$VARS_PATH" .conf`"
    BUILD_SUBDIR="${STYLE_NAME}_${BUILD_SUBDIR}"
  done

  if [ 'x' = 'x'"`echo "$BUILD_SUBDIR" | sed -e 's,default_,,g' -e 's,_$,,'`" ];
  then
    echo -n 'default'
  fi

  echo "$BUILD_SUBDIR" | sed -e 's,default_,,g' -e 's,_$,,'
}


# Addapts all tex files.
generate_all_tex_files ()
{
  local BUILD_DIR="$1"
  local SRC_TEX_DIR="$2"
  shift 2

  for src_tex_path in "$SRC_TEX_DIR"/*.{tex,bib,sty};
  do
    # It is stupid, but the shell does not expand * to an empty string
    # if there is no match.
    if [ ! -e "$src_tex_path" ];
    then
      continue
    fi

    mkdir -p "$BUILD_DIR/$SRC_TEX_DIR"
    generate_file_if_changed "$src_tex_path" "$BUILD_DIR/$src_tex_path" "$@"
  done

  for next_src_tex_dir in "$SRC_TEX_DIR"/*;
  do
    # It is stupid, but the shell does not expand * to an empty string
    # if there is no match.
    if [ ! -d "$next_src_tex_dir" ];
    then
      continue
    fi

    generate_all_tex_files "$BUILD_DIR" "$next_src_tex_dir" "$@"
  done
}

generate_file_if_changed ()
{
  local SRC_TEX_PATH="$1"
  local GENERATED_TEX_PATH="$2"
  shift 2


  NEW_MD5="`echo_generated_file "$SRC_TEX_PATH" "$@" | md5sum`"
  OLD_MD5="`cat "$GENERATED_TEX_PATH" 2> /dev/null | md5sum || true`"

  # Only regenerate tex file if it would have changed or if it does not exist (deleted).
  if [ "$NEW_MD5" != "$OLD_MD5" ] || [ ! -e "$GENERATED_TEX_PATH" ];
  then
    echo_generated_file "$SRC_TEX_PATH" "$@" > "$GENERATED_TEX_PATH"
  fi
}

echo_generated_file ()
{
  local src_tex_path="$1"
  shift

  #
  # Substitutes variables and comment/uncomment out conditionals.
  #
  SED_ARGS="`get_comment_uncomment_sed_args "$@"`"

  # Process the input.
  cat "$src_tex_path" |
  eval sed -r $SED_ARGS
}


#
# Get sed args for commenting/uncommenting the code.
#
get_comment_uncomment_sed_args ()
{
  normalize_style_conf "$@" | while read var_name var_value
  do
    # Substitute values. (TODO choose separator)
    echo "-e 's;@var:$var_name@;$var_value;'"


    # Uncomment conditional.
    if [ "$var_value" != 'no' ];
    then
      cond_val="$var_name"
    else
      cond_val="NO_$var_name"
    fi

    echo "-e 's,^%*([^%].*)@uncomment@(([A-Za-z0-9_]+@)*$cond_val@.*),\1@uncommentED@\2,'"
    echo "-e 's,(.*)@comment@(([A-Za-z0-9_]+@)*$cond_val@.*),% % COMMENT directives have precedence % % \1@commentED@\2,'"
  done


  # Clean up: wipe out conditions not satisfied.
  echo "-e '/^%.*@comment@/D'"
  echo "-e '/^%.*@uncomment@/D'"
}



#
# Get sed args for the sty template.
#
get_sty_sed_args ()
{
  local var_name="`echo "$1" | sed -e 's,[^0-9a-zA-Z],,g'`"
  local var_value="$2"

  # Substitute values. (TODO choose separator)
  echo "-e 's;@var_name@;$var_name;'"
  echo "-e 's;@var_value@;$var_value;'"
}



#
# Strips comments and empty lines from style config and prepend values in args.
#
normalize_style_conf ()
{
  local vars_file_path=""

  # Sets the variable "processing_vars".
  echo "processing_vars yes"

  for vars_file_path in "$@";
  do
    sed -e 's, *#.*,,' -e '/^ *$/D' "$vars_file_path"
  done
}
