#! /bin/bash
#
# Build many variations for different values of one variable.
#
# Example:
#  $ bin/build_many.sh book.tex default chapterstyle
#
# Args:
# 1. Path to TEX file.
# 2. Path to conf files or conf directories.
#
# Dependencies:
# 1. "`dirname "$0"`"/build.sh
# 

#set -x
set -e

usage ()
{
  echo "Usage:"
  echo "    $0 <tex_file> [<conf_file> ...]"
}

BIN_DIR="`dirname "$0"`"
BUILD_SH="$BIN_DIR/build.sh"
BUILD_MANY_SH="$0"

SRC_TEX_PATH="$1"
shift || true
if [ -z "$SRC_TEX_PATH" ];
then
  echo "No <tex_file> specified." >&2
  usage >&2
  exit 1
fi



declare -a new_args

while [ ! -z "$1" ]
do
  path="$1"
  shift || true

  if [ -d "$path" ]
  then
    for file_inside_dir in "$path"/*.conf
    do
      "$BUILD_MANY_SH" "$SRC_TEX_PATH" "${new_args[@]}" "$file_inside_dir" "$@"
    done
    exit "$?"
  fi

  new_args+=("$path")
done

echo; echo;
echo "*************"
echo "* Compiling $SRC_TEX_PATH ${new_args[@]}"
echo "*************"
"$BUILD_SH" "$SRC_TEX_PATH" "${new_args[@]}"
