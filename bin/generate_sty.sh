#! /bin/bash
#
# Generate config STY file using the variables and conditionals
# according provided variables file.
#
# Args:
# 1. Paths to variables file.
#

#set -x
set -e


usage ()
{
  echo "Usage:"
  echo "    $0 [<vars_file> ...]"
  echo "Look for available styles at the `dirname "$0"`/../styles/ directory."
}

BIN_DIR="`dirname "$0"`"
. "$BIN_DIR/functions.sh"

STY_TEMPLATE_PATH="$BIN_DIR/sty_template.sty"


#
# The main program.
# Substitutes variables and sets the comment/uncomment
# tex commands for the tex include sty file.
#

DELETE_SED_ARG=""
normalize_style_conf "$@" | while read var_name var_value
do
  SED_ARGS="`get_sty_sed_args "$var_name" "$var_value"`"

  # Process the input.
  eval sed -r $DELETE_SED_ARG $SED_ARGS "$STY_TEMPLATE_PATH"

  DELETE_SED_ARG="-e '/@only_once@/D' "
done
